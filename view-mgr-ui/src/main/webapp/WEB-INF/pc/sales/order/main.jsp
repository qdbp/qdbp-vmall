<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="base" uri="http://qdbp.gitee.io/tags/base/"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<table class="x-datagrid main-datalist"
		data-options="toolbar:'.toolbar-box',
		method:'POST',url:'<base:url href='actions/order/form/list.json' />',
		fixedParams:{ordering:'orderState asc, createTime desc', orderOrigin:'INSIDE', orderStates:['PAID','DELIVERED','FINISHED']},
		idField:'id',
		eachCopy:{orderId:'this.id'},
		xextra:[
			{field:'Shop',key:'sysCode',value:'sysName',parent:'parentCode'},
			{field:'Area',key:'areaCode',value:'areaName',parent:'parentCode',fn:xfn.extra.group},
			{field:'User',key:'id',value:'nickName||realName||userCode'}]">
	<thead>
		<tr>
			<th data-options="field:'id',width:100,align:'center',checkbox:true">选择</th>
			<th data-options="field:'operate',width:180,align:'center',xtemplate:'OPERATES'">操作</th>
			<th data-options="field:'orderId',width:100,align:'center'">订单编号</th>
			<th data-options="field:'orderContent',width:300,align:'center'">订单内容</th>
			<th data-options="field:'totalAmount',width:80,align:'center'">金额</th>
			<th data-options="field:'userPhone',width:100,align:'center'">顾客号码</th>
			<th data-options="field:'createTime',width:150,align:'center'">创建时间</th>
			<th data-options="field:'orderState',width:60,align:'center',xextra:{field:'OrderState',cls:{PENDING:'color-weak',CANCELED:'color-weak',PAID:'color-info'}}">状态</th>
		</tr>
	</thead>
</table>

<div class="hide">
	<div class="x-dialog container-fluid" title="详情" data-options="xpopup:'details',width:900,height:500,modal:true,iconCls:'fa fa-files-o'">
		<table class="x-datagrid item-datalist"
				data-options="toolbar:'.toolbar-details',
				pagination:false,idField:'id',
				method:'POST',tmpurl:'<base:url href='actions/order/form/items.json' />',
				fixedParams:{extra:false}">
			<thead>
				<tr>
					<th data-options="field:'id',width:100,align:'center',hidden:true">编号</th>
					<th data-options="field:'goodsId',width:95,align:'center',formatter:format.goodsId">商品唯一编号</th>
					<th data-options="field:'goodsVid',width:95,align:'center'">商品版本编号</th>
					<th data-options="field:'goodsTitle',width:300,align:'center'">标题</th>
					<th data-options="field:'goodsPrice',width:70,align:'center'">单价</th>
					<th data-options="field:'quantity',width:70,align:'center'">数量</th>
					<th data-options="field:'goodsAmount',width:70,align:'center'">金额</th>
					<th data-options="field:'soldQuantity',width:80,align:'center'">待发货库存</th>
					<th data-options="field:'stockQuantity',width:80,align:'center'">可销售库存</th>
				</tr>
			</thead>
		</table>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">关闭</a>
		</div>
	</div>
	<div class="toolbar-details">
		<form class="pure-box form-horizontal">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-2 control-label">顾客号码</div>
						<div class="col-md-4 control-input"><input class="x-textbox" type="text" name="userPhone" data-options="readonly:true"/></div>
						<div class="col-md-2 control-label">订单编号</div>
						<div class="col-md-4 control-input"><input class="x-textbox" type="text" name="id" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-2 control-label">顾客姓名</div>
						<div class="col-md-4 control-input"><input class="x-textbox" type="text" name="userName" data-options="readonly:true"/></div>
						<div class="col-md-2 control-label">状态</div>
						<div class="col-md-4 control-input"><input class="x-textbox" type="text" name="orderState" data-options="readonly:true,xextra:'OrderState'"/></div>
					</div>
				</div>
			</dd>
		</form>
	</div>

	<div class="x-dialog container-fluid" title="删除" data-options="xpopup:'delete-ineffective',width:500,modal:true,iconCls:'fa fa-minus'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/order/form/delete-ineffective.json'/>">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">顾客号码</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="userPhone" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">订单编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="id" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">订单内容</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="orderContent" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">订单金额</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="totalAmount" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">状态</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="orderState" data-options="readonly:true,xextra:'OrderState'"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">删除说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
				<div class="text-center color-warn"> 
					<%-- <p>该商品已经发布上线, 不建议删除!</p> --%>
					<p>一旦删除将无法恢复! 确定要删除该商品?</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'订单删除'">确定</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="删除" data-options="xpopup:'delete-effective',width:500,modal:true,iconCls:'fa fa-minus'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/order/form/delete-effective.json'/>">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">顾客号码</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="userPhone" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">订单编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="id" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">订单内容</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="orderContent" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">订单金额</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="totalAmount" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">状态</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="orderState" data-options="readonly:true,xextra:'OrderState'"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">删除说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
				<div class="text-center color-warn"> 
					<p>此订单已生效, 不建议删除!</p>
					<p>一旦删除将无法恢复! 确定要删除此订单?</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'订单删除'">确定</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="取消" data-options="xpopup:'cancel-single',width:500,modal:true,iconCls:'fa fa-minus'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/order/form/cancel.json'/>">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">顾客号码</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="userPhone" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">订单编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="id" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">订单内容</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="orderContent" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">订单金额</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="totalAmount" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">状态</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="orderState" data-options="readonly:true,xextra:'OrderState'"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">删除说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
				<div class="text-center color-warn"> 
					<p>一旦取消顾客将无法再支付此订单! 确定要取消此订单?</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'订单取消'">确定</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">关闭</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="核销" data-options="xpopup:'deliver-single',width:900,height:500,modal:true,iconCls:'fa fa-hand-o-right'">
		<table class="x-datagrid item-datalist"
				data-options="toolbar:'.toolbar-deliver',
				pagination:false,idField:'id',
				method:'POST',tmpurl:'<base:url href='actions/order/form/items.json' />',
				fixedParams:{extra:false}">
			<thead>
				<tr>
					<th data-options="field:'id',width:100,align:'center',hidden:true">编号</th>
					<th data-options="field:'goodsId',width:95,align:'center',formatter:format.goodsId">商品唯一编号</th>
					<th data-options="field:'goodsVid',width:95,align:'center'">商品版本编号</th>
					<th data-options="field:'goodsTitle',width:300,align:'center'">标题</th>
					<th data-options="field:'goodsPrice',width:70,align:'center'">单价</th>
					<th data-options="field:'quantity',width:70,align:'center'">数量</th>
					<th data-options="field:'goodsAmount',width:70,align:'center'">金额</th>
					<th data-options="field:'soldQuantity',width:80,align:'center'">待发货库存</th>
					<th data-options="field:'stockQuantity',width:80,align:'center'">可销售库存</th>
				</tr>
			</thead>
		</table>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'订单核销'">核销</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">关闭</a>
		</div>
	</div>
	<%--
	<div class="x-dialog container-fluid" title="核销" data-options="xpopup:'deliver-single',width:500,modal:true,iconCls:'fa fa-hand-o-right'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/order/form/deliver-by-taken.json'/>">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">顾客号码</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="userPhone" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">订单编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="id" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">订单内容</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="orderContent" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">订单金额</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="totalAmount" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">备注</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
				</div>
				<div class="text-center"> 
					<p><span class="color-warn">请注意核对顾客信息!</span> 一旦核销, 说明顾客已领取商品! 确定要核销此订单?</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'订单核销'">核销</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	--%>
	<div class="toolbar-deliver">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/order/form/deliver-by-taken.json'/>">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-2 control-label">顾客号码</div>
						<div class="col-md-4 control-input"><input class="x-textbox" type="text" name="userPhone" data-options="readonly:true"/></div>
						<div class="col-md-2 control-label">订单编号</div>
						<div class="col-md-4 control-input"><input class="x-textbox" type="text" name="id" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-2 control-label">顾客姓名</div>
						<div class="col-md-4 control-input"><input class="x-textbox" type="text" name="userName" data-options="readonly:true"/></div>
						<div class="col-md-2 control-label">订单金额</div>
						<div class="col-md-4 control-input"><input class="x-textbox" type="text" name="totalAmount" data-options="readonly:true"/></div>
					</div>
				</div>
				<div class="text-center"> 
					<p><span class="color-warn">请注意核对顾客信息!</span> 一旦核销, 说明顾客已领取商品! 确定要核销此订单?</p>
				</div>
			</dd>
		</form>
	</div>

	<div class="toolbar-box">
		<form class="toolbar-collapse">
			<div class="panel-tool">
				<a href="javascript:void(0)" class="accordion-collapse accordion-expand"></a>
			</div>
			<div class="toolbar-item">
				<input type="hidden" name="deptCodeStarts" />
				<span class="toolbar-text">编号:</span><input name="orderCode" class="x-textbox panel-input"/>
			</div>
			<div class="toolbar-item">
				<span class="toolbar-text">顾客:</span><input name="userPhoneLike" class="x-textbox panel-input"/>
				<span class="toolbar-text">状态:</span><input name="orderState" class="x-combobox panel-input" data-options="editable:false,onChange:xfn.toolbar.search,xextra:{field:'OrderState',prepend:{key:'',value:'默认'}}"/>
			</div>
			<div class="toolbar-item">
				<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'search',iconCls:'fa fa-search'">查询</a>
				<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'reset'<%--,iconCls:'fa fa-circle-thin'--%>">重置</a>
				<div class="clear"></div>
			</div>
			<div class="toolbar-more" style="width:950px;">
				<div class="toolbar-item">
					<span class="toolbar-text">内容:</span><input name="orderContentLike" class="x-textbox panel-input"/>
				</div>
				<div class="toolbar-item">
					<span class="toolbar-text">金额:</span><input name="totalAmountMin" class="x-numberbox panel-input"/>-<input name="totalAmountMax" class="x-numberbox panel-input"/>
				</div>
				<div class="toolbar-item">
					<span class="toolbar-text">日期:</span><input name="createTimeMin" class="x-datebox panel-input" data-options="editable:false,startTime:true,onChange:xfn.toolbar.search"/>-<input name="createTimeMax" class="x-datebox panel-input" data-options="editable:false,endTime:true,onChange:xfn.toolbar.search"/>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="toolbar-item">
				<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'details',selection:'single',iconCls:'fa fa-file-text-o'">详情</a> --%>
				<shiro:hasPermission name="order:form:edit">
				<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xclick:onClickOfCreate,iconCls:'fa fa-plus',fillData:false">创建</a> --%>
				</shiro:hasPermission>
				<shiro:hasPermission name="order:form:delete">
				<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'delete',selection:'multi',iconCls:'fa fa-minus',fillData:{id:'ids',i:'.records'}">删除</a> --%>
				</shiro:hasPermission>
				<shiro:hasPermission name="order:form:import">
				<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'import',iconCls:'fa fa-sign-in'">导入</a> --%>
				</shiro:hasPermission>
				<shiro:hasPermission name="order:form:export">
				<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'export',iconCls:'fa fa-sign-out'">导出</a> --%>
				</shiro:hasPermission>
			</div>
			<div class="clear"></div>
		</form>
	</div>
</div>

<script type="OPERATES">
	<shiro:hasPermission name="order:form:query">
		<a class="click-open-xpopup" data-options="xpopup:'details',onAfterFillData:onDetailAfterFillData">详情</a>
	</shiro:hasPermission>
	<# if (this.orderState == "PAID") { #>
		<shiro:hasPermission name="order:form:deliver">
			<a class="x-linkbutton link-text" data-options="xpopup:'deliver-single',onAfterFillData:onDetailAfterFillData">核销</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="order:form:delete:effective">
			<a class="click-open-xpopup" data-options="xpopup:'delete-effective'">删除</a>
		</shiro:hasPermission>
    <# } else if (this.orderState == "PENDING") { #>
		<%--		
		<shiro:hasPermission name="order:form:cancel">
			<a class="click-open-xpopup" data-options="xpopup:'cancel-single'">取消</a>
		</shiro:hasPermission>
		--%>
		<shiro:hasPermission name="order:form:delete:ineffective">
			<a class="click-open-xpopup" data-options="xpopup:'delete-ineffective'">删除</a>
		</shiro:hasPermission>
	<# } else { #>
		<# if (this.orderState == "CANCELED") { #>
			<shiro:hasPermission name="order:form:delete:ineffective">
				<a class="click-open-xpopup" data-options="xpopup:'delete-ineffective'">删除</a>
			</shiro:hasPermission>
		<# } else { #>
			<shiro:hasPermission name="order:form:delete:effective">
				<a class="click-open-xpopup" data-options="xpopup:'delete-effective'">删除</a>
			</shiro:hasPermission>
		<# } #>
	<# } #>
</script>
<script src="<base:url href='assets/${pv.view}.js'/>"></script>
