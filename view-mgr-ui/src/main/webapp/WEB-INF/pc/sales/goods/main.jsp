<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="base" uri="http://qdbp.gitee.io/tags/base/"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<link href="<base:url href='assets/${pv.view}.css'/>" rel="stylesheet" type="text/css" />

<table class="x-datagrid main-datalist"
		data-options="toolbar:'.toolbar-box',
		method:'POST',url:'<base:url href='actions/goods/manager/list.json' />',
		fixedParams:{queryDuplicate:true,ordering:'uid desc, versionState asc, vid desc',versionStates:['ACTIVATED','DUPLICATE']},
		idField:'vid',
		eachCopy:{versionId:'this.vid'},
		xextra:{map:{LeafShop:'extra.Shop'},fields:[
			{field:'Category',key:'nodeCode',value:'nodeText',parent:'parentCode'},
			{field:'LeafShop',key:'sysCode',value:'sysName',parent:'parentCode',fn:xfn.extra.group},
			{field:'Area',key:'areaCode',value:'areaName',parent:'parentCode',fn:xfn.extra.group},
			{field:'User',key:'id',value:'nickName||realName||userCode'}]}">
	<thead>
		<tr>
			<th data-options="field:'vid',width:100,align:'center',checkbox:true">选择</th>
			<th data-options="field:'operate',width:180,align:'center',xtemplate:'OPERATES'">操作</th>
			<th data-options="field:'categoryCode',width:150,align:'center',xextra:{field:'Category',emptyText:'未分类',notFoundText:'未知'}">分类</th>
			<th data-options="field:'uid',width:100,align:'center',formatter:format.goodsUid">商品唯一编号</th>
			<th data-options="field:'versionId',width:100,align:'center'">商品版本编号</th>
			<th data-options="field:'title',width:340,align:'center'">标题</th>
			<th data-options="field:'priceValue',width:80,align:'center',xtemplate:'change-price'">价格</th>
			<th data-options="field:'salesQuantityTotal',width:60,align:'center',xtemplate:'change-sales-quantity'">销量</th>
			<th data-options="field:'stockQuantity',width:60,align:'center',xtemplate:'change-stock-quantity'">库存</th>
			<th data-options="field:'creatorId',width:80,align:'center',xextra:{field:'User'}">创建人</th>
			<th data-options="field:'goodsState',width:60,align:'center',xextra:{field:'GoodsState',cls:{PENDING:'color-weak',PAUSING:'color-weak',SELLING:'color-info',REJECTED:'color-warn'}}">状态</th>
		</tr>
	</thead>
</table>

<div class="hide">
	<div class="x-dialog preview-box container-fluid" title="预览" data-options="xpopup:'preview',width:400,height:800,modal:true,iconCls:'fa fa-search-plus'">
		<iframe class="preview-iframe" frameborder="0" width="100%" height="100%" scrolling="auto" marginwidth="0" marginheight="0"></iframe>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">关闭</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="选择版本" data-options="xpopup:'choose-duplicate',width:900,height:500,modal:true,iconCls:'fa fa-files-o'">
		<table class="x-datagrid duplicate-datalist"
				data-options="pagination:false,idField:'vid',
				method:'POST',tmpurl:'<base:url href='actions/goods/manager/list.json' />',
				fixedParams:{extra:false,ordering:'versionState asc, sortIndex asc, createTime desc',versionStates:['ACTIVATED','DUPLICATE']}">
			<thead>
				<tr>
					<th data-options="field:'vid',width:100,align:'center',hidden:true">VID</th>
					<th data-options="field:'operate',width:150,align:'center',xtemplate:'DUPLICATE-OPERATES'">操作</th>
					<th data-options="field:'title',width:340,align:'center'">标题</th>
					<th data-options="field:'priceValue',width:80,align:'center'">价格</th>
					<th data-options="field:'creatorId',width:80,align:'center',xextra:{field:'User'}">创建人</th>
					<th data-options="field:'goodsState',width:60,align:'center',xextra:{field:'GoodsState',cls:{PENDING:'color-weak',PAUSING:'color-weak',SELLING:'color-info',REJECTED:'color-warn'}}">状态</th>
				</tr>
			</thead>
		</table>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">关闭</a>
		</div>
	</div>

	<div class="x-dialog container-fluid" title="复制为新版本" data-options="xpopup:'create-duplicate',width:500,modal:true,iconCls:'fa fa-plus'">
		<form class="pure-box form-horizontal" method="POST">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">唯一编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="uid" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">版本编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="vid" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">商品标题</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="title" data-options="readonly:true,disabled:true"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">状态</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="goodsState" data-options="readonly:true,disabled:true,xextra:'GoodsState'"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">操作说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
				<div class="text-center">
					<p>每个商品只有一个发布'上架'的版本, 此版本用户能看到和购买.</p>
					<p>可以有多个正在编辑的版本, 用户看不到.</p>
					<p>一个编辑版本通过审核和发布成为新的'上架'版本.</p>
					<p class="color-warn">确定要为该商品创建一个新版本?</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xclick:onClickOfEnterEdit,enterOptions:{createDuplicate:true},iconCls:'fa fa-check',message:'删除'">确定</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="删除" data-options="xpopup:'delete-newer',width:500,modal:true,iconCls:'fa fa-minus'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/manager/delete-newer.json'/>">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">唯一编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="uid" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">版本编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="vid" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">商品标题</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="title" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">状态</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="goodsState" data-options="readonly:true,disabled:true,xextra:'GoodsState'"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">删除说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
				<div class="text-center color-warn"> 
					<p>一旦删除将无法恢复! 确定要删除该商品?</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'删除'">确定</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="删除" data-options="xpopup:'delete-selled',width:500,modal:true,iconCls:'fa fa-minus'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/manager/delete-selled.json'/>">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">唯一编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="uid" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">版本编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="vid" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">商品标题</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="title" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">状态</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="goodsState" data-options="readonly:true,disabled:true,xextra:'GoodsState'"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">删除说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
				<div class="text-center color-warn"> 
					<%-- <p>该商品已经发布上线, 不建议删除!</p> --%>
					<p>一旦删除将无法恢复! 确定要删除该商品?</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'删除'">确定</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="审批" data-options="xpopup:'approve-single',width:500,modal:true,iconCls:'fa fa-hand-o-right'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/manager/approve.json'/>">
			<dd>
				<input type="hidden" name="pass" />
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">唯一编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="uid" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">版本编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="vid" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">商品标题</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="title" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">审批意见</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',fillForm:{pass:true},iconCls:'fa fa-thumbs-o-up',message:'审批'">同意</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',fillForm:{pass:false},iconCls:'fa fa-thumbs-o-down',message:'审批'">驳回</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="补充库存" data-options="xpopup:'change-stock-quantity',width:500,modal:true,iconCls:'fa fa-hand-o-right'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/manager/change-stock-quantity.json'/>">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">唯一编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="uid" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">版本编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="vid" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">商品标题</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="title" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">补充库存</div>
						<div class="col-md-3 control-input"><input class="x-textbox" type="text" name="stockQuantity" data-options="readonly:true,disabled:true"/></div>
						<div class="col-md-1 control-text text-center">+</div>
						<div class="col-md-5 control-input"><input class="x-numberbox" type="text"name="quantityAdd" data-options="required:true,min:-99999999,max:99999999,prompt:'正数调增,负数调减'"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">操作说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
				<div class="text-center"> 
					<p>补充库存是在现在的库存数量基础上增加或减少库存数量!</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'补充库存'">提交</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="调整销量" data-options="xpopup:'change-sales-quantity',width:500,modal:true,iconCls:'fa fa-hand-o-right'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/manager/change-sales-quantity.json'/>">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">唯一编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="uid" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">版本编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="vid" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">商品标题</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="title" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">调整销量</div>
						<div class="col-md-3 control-input"><input class="x-textbox" type="text" name="salesQuantityTotal" data-options="readonly:true,disabled:true"/></div>
						<div class="col-md-1 control-text text-center">+</div>
						<div class="col-md-5 control-input"><input class="x-numberbox" type="text"name="quantityAdd" data-options="required:true,min:-99999999,max:99999999,prompt:'正数调增,负数调减'"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">操作说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
				<div class="text-center"> 
					<p>调整销量是在现在的销量数量基础上增加或减少销量!</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'调整销量'">提交</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="调价" data-options="xpopup:'change-price',width:500,modal:true,iconCls:'fa fa-hand-o-right'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/manager/change-price.json'/>">
			<dd>
				<input type="hidden" name="editIndex" />
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">唯一编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="uid" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">版本编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="vid" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">商品标题</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="title" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">市场价格</div>
						<div class="col-md-4 control-input"><input class="x-textbox" type="text" name="marketPriceText" data-options="readonly:true,disabled:true"/></div>
						<div class="col-md-1 control-text text-center">--&gt;</div>
						<div class="col-md-4 control-input"><input class="x-numberbox" type="text"name="marketPriceText" data-options="precision:2,min:0,max:99999999.99"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">销售价格</div>
						<div class="col-md-4 control-input"><input class="x-textbox" type="text" name="priceValue" data-options="readonly:true,disabled:true"/></div>
						<div class="col-md-1 control-text text-center">--&gt;</div>
						<div class="col-md-4 control-input"><input class="x-numberbox" type="text"name="priceValue" data-options="precision:2,min:0,max:99999999.99"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">操作说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
				<div class="text-center color-warn"> 
					<p>此商品已发布上架</p>
					<p>调整价格将会生成一个新的版本, 并立即发布上架!</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'调价'">提交</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="调价" data-options="xpopup:'edit-price',width:500,modal:true,iconCls:'fa fa-hand-o-right'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/manager/edit-price.json'/>">
			<dd>
				<input type="hidden" name="editIndex" />
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">唯一编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="uid" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">版本编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="vid" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">商品标题</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="title" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">市场价格</div>
						<div class="col-md-4 control-input"><input class="x-textbox" type="text" name="marketPriceText" data-options="readonly:true,disabled:true"/></div>
						<div class="col-md-1 control-text text-center">--&gt;</div>
						<div class="col-md-4 control-input"><input class="x-numberbox" type="text"name="marketPriceText" data-options="precision:2,min:0,max:99999999.99"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">销售价格</div>
						<div class="col-md-4 control-input"><input class="x-textbox" type="text" name="priceValue" data-options="readonly:true,disabled:true"/></div>
						<div class="col-md-1 control-text text-center">--&gt;</div>
						<div class="col-md-4 control-input"><input class="x-numberbox" type="text"name="priceValue" data-options="precision:2,min:0,max:99999999.99"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">价格描述</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="salesPriceText" data-options="validType:['length[0,20]']"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">操作说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'调价'">提交</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="发布上架" data-options="xpopup:'publish-single',width:500,modal:true,iconCls:'fa fa-hand-o-right'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/manager/publish.json'/>">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">唯一编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="uid" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">版本编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="vid" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">商品标题</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="title" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">补充库存</div>
						<div class="col-md-3 control-input"><input class="x-textbox" type="text" name="stockQuantity" data-options="readonly:true,disabled:true"/></div>
						<div class="col-md-1 control-text text-center">+</div>
						<div class="col-md-5 control-input"><input class="x-numberbox" type="text"name="stockQuantityAdd" data-options="min:-99999999,max:99999999,prompt:'正数调增,负数调减'"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">调整销量</div>
						<div class="col-md-3 control-input"><input class="x-textbox" type="text" name="salesQuantityTotal" data-options="readonly:true,disabled:true"/></div>
						<div class="col-md-1 control-text text-center">+</div>
						<div class="col-md-5 control-input"><input class="x-numberbox" type="text"name="salesQuantityAdd" data-options="min:-99999999,max:99999999,prompt:'正数调增,负数调减'"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">市场价格</div>
						<div class="col-md-3 control-input"><input class="x-textbox" type="text" name="marketPriceText" data-options="readonly:true,disabled:true"/></div>
						<div class="col-md-1 control-text text-center">--&gt;</div>
						<div class="col-md-5 control-input"><input class="x-numberbox" type="text"name="marketPriceText" data-options="precision:2,min:0,max:99999999.99"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">销售价格</div>
						<div class="col-md-3 control-input"><input class="x-textbox" type="text" name="priceValue" data-options="readonly:true,disabled:true"/></div>
						<div class="col-md-1 control-text text-center">--&gt;</div>
						<div class="col-md-5 control-input"><input class="x-numberbox" type="text"name="priceValue" data-options="precision:2,min:0,max:99999999.99"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">操作说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
				<div class="text-center"> 
					<p>只有发布上架的商品用户才能看到和购买.</p>
					<p>每个商品只允许有一个发布上架的版本.</p>
					<p class="color-warn">发布此版本会将该商品的其他版本下架!</p>
					<p>确定要将此版本的商品发布上架?</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'上架'">确定</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="下架停售" data-options="xpopup:'unpublish-single',width:500,modal:true,iconCls:'fa fa-hand-o-right'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/manager/unpublish.json'/>">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">唯一编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="uid" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">版本编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="vid" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">商品标题</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="title" data-options="readonly:true,disabled:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">操作说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
				</div>
				<div class="text-center"> 
					<p class="color-warn">下架后用户将无法再看到和购买该商品!</p>
					<p>确定要将该商品下架停售?</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'下架'">确定</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="发布" data-options="xpopup:'publish',width:320,modal:true,iconCls:'fa fa-minus'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/manager/publish.json'/>">
			<dd>
				<span>确定要删除<abbr title="勾选指的是在第一列打勾，可以同时选中多行记录">勾选</abbr>的 <span class="records"></span> 条记录?</span>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'删除'">确定</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="toolbar-box">
		<form class="toolbar-collapse">
			<div class="panel-tool">
				<a href="javascript:void(0)" class="accordion-collapse accordion-expand"></a>
			</div>
			<div class="toolbar-item">
				<span class="toolbar-text">标题:</span><input name="titleLike" class="x-textbox panel-input"/>
			</div>
			<div class="toolbar-item">
				<span class="toolbar-text">唯一编号:</span><input name="uid" class="x-textbox panel-input"/>
				<span class="toolbar-text">版本编号:</span><input name="vid" class="x-textbox panel-input"/>
				<span class="toolbar-text">分类:</span><input class="x-combotree" type="text" name="categoryCodeStarts" data-options="editable:false,onChange:xfn.toolbar.search,xextra:{field:'Category',prepend:{key:'',value:'全部'},append:{key:'-',value:'未分类'}}"/>
				<span class="toolbar-text">状态:</span><input name="goodsState" class="x-combobox panel-input" data-options="editable:false,onChange:xfn.toolbar.search,xextra:{field:'GoodsState',prepend:{key:'',value:'全部'}}"/>
			</div>
			<div class="toolbar-item">
				<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'search',iconCls:'fa fa-search'">查询</a>
				<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'reset'<%--,iconCls:'fa fa-circle-thin'--%>">重置</a>
				<div class="clear"></div>
			</div>
			<div class="toolbar-more" style="width:850px;">
				<div class="toolbar-item">
					<span class="toolbar-text">销量:</span><input name="salesQuantityTotalMin" class="x-numberbox panel-input"/>-<input name="salesQuantityTotalMax" class="x-numberbox panel-input"/>
					<span class="toolbar-text">库存:</span><input name="stockQuantityMin" class="x-numberbox panel-input"/>-<input name="stockQuantityMax" class="x-numberbox panel-input"/>
				</div>
				<div class="clear"></div>
			</div>
			<div class="toolbar-item">
				<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'details',selection:'single',iconCls:'fa fa-file-text-o'">详情</a> --%>
				<shiro:hasPermission name="goods:manager:edit">
				<a href="javascript:void(0)" class="x-linkbutton" data-options="xclick:onClickOfCreate,iconCls:'fa fa-plus',fillData:false">创建</a>
				</shiro:hasPermission>
				<shiro:hasPermission name="goods:manager:delete">
				<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'delete',selection:'multi',iconCls:'fa fa-minus',fillData:{id:'ids',i:'.records'}">删除</a> --%>
				</shiro:hasPermission>
				<shiro:hasPermission name="goods:manager:import">
				<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'import',iconCls:'fa fa-sign-in'">导入</a> --%>
				</shiro:hasPermission>
				<shiro:hasPermission name="goods:manager:export">
				<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'export',iconCls:'fa fa-sign-out'">导出</a> --%>
				</shiro:hasPermission>
			</div>
			<div class="clear"></div>
		</form>
	</div>
</div>

<script type="change-stock-quantity">
	<# if (this.versionState == "ACTIVATED") { #>
		<shiro:hasPermission name="goods:manager:change:stock-quantity">
			<a class="click-open-xpopup" data-options="xpopup:'change-stock-quantity'"><#=this.stockQuantity#></a>
		</shiro:hasPermission>
		<shiro:lacksPermission name="goods:manager:change:stock-quantity">
			<#=this.stockQuantity#>
		</shiro:lacksPermission>
	<# } #>
</script>
<script type="change-sales-quantity">
	<# if (this.versionState == "ACTIVATED") { #>
		<shiro:hasPermission name="goods:manager:change:sales-quantity">
			<a class="click-open-xpopup" data-options="xpopup:'change-sales-quantity'"><#=this.salesQuantityTotal#></a>
		</shiro:hasPermission>
		<shiro:lacksPermission name="goods:manager:change:sales-quantity">
			<#=this.salesQuantityTotal#>
		</shiro:lacksPermission>
	<# } #>
</script>
<script type="change-price">
	<# if (this.goodsState == "PAUSING") { #><%-- 已下架 --%>
		<#=this.priceValue#>
	<# } else if (this.goodsState == "SELLING") { #><%-- 已上架 --%>
		<shiro:hasPermission name="goods:manager:change:price">
			<a class="click-open-xpopup" data-options="xpopup:'change-price'"><#=this.priceValue#></a>
		</shiro:hasPermission>
		<shiro:lacksPermission name="goods:manager:change:price">
			<#=this.priceValue#>
		</shiro:lacksPermission>
	<# } else { #>
		<shiro:hasPermission name="goods:manager:edit">
			<a class="click-open-xpopup" data-options="xpopup:'edit-price'"><#=this.priceValue#></a>
		</shiro:hasPermission>
		<shiro:lacksPermission name="goods:manager:edit">
			<#=this.priceValue#>
		</shiro:lacksPermission>
	<# } #>
</script>
<script type="OPERATES">
	<# if (this.goodsState == "PENDING" || this.goodsState == "REJECTED") { #>
		<shiro:hasPermission name="goods:manager:edit">
			<a class="x-linkbutton link-text" data-options="xclick:onClickOfEnterEdit,duplicate:true">编辑</a>
		</shiro:hasPermission>
		<shiro:lacksPermission name="goods:manager:edit">
			<a class="x-linkbutton link-text" data-options="xclick:onClickOfEnterEdit,duplicate:true,enterOptions:{readonly:true}">详情</a>
		</shiro:lacksPermission>
		<shiro:hasPermission name="goods:manager:delete:newer">
			<a class="click-open-xpopup" data-options="xpopup:'delete-newer'">删除</a>
		</shiro:hasPermission>
    <# } else if (this.goodsState == "SUBMITTED") { #>
       	<shiro:hasPermission name="goods:manager:approve">
       		<a class="click-open-xpopup" data-options="xpopup:'approve-single'">审批</a>
       	</shiro:hasPermission>
		<shiro:hasPermission name="goods:manager:query">
			<a class="x-linkbutton link-text" data-options="xclick:onClickOfEnterEdit,enterOptions:{readonly:true}">详情</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="goods:manager:delete:newer">
			<a class="click-open-xpopup" data-options="xpopup:'delete-newer'">删除</a>
		</shiro:hasPermission>
	<# } else if (this.goodsState == "ARGEED") { #>
		<shiro:hasPermission name="goods:manager:publish">
			<a class="click-open-xpopup" data-options="xpopup:'publish-single'">上架</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="goods:manager:query">
			<a class="x-linkbutton link-text" data-options="xclick:onClickOfEnterEdit,enterOptions:{readonly:true}">详情</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="goods:manager:delete:newer">
			<a class="click-open-xpopup" data-options="xpopup:'delete-newer'">删除</a>
		</shiro:hasPermission>
	<# } else if (this.goodsState == "SELLING") { #><%-- 已上架 --%>
		<shiro:hasPermission name="goods:manager:unpublish">
			<a class="click-open-xpopup" data-options="xpopup:'unpublish-single'">下架</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="goods:manager:query">
			<a class="x-linkbutton link-text" data-options="xclick:onClickOfEnterEdit,enterOptions:{readonly:true}">详情</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="goods:manager:delete:selled"><%-- 删除已销售过的商品需要更高权限 --%>
		<a class="click-open-xpopup" data-options="xpopup:'delete-selled'">删除</a>
		</shiro:hasPermission>
	<# } else if (this.goodsState == "PAUSING") { #><%-- 已下架 --%>
		<shiro:hasPermission name="goods:manager:publish">
			<a class="click-open-xpopup" data-options="xpopup:'publish-single'">上架</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="goods:manager:query">
			<a class="x-linkbutton link-text" data-options="xclick:onClickOfEnterEdit,enterOptions:{readonly:true}">详情</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="goods:manager:delete:selled"><%-- 删除已销售过的商品需要更高权限 --%>
			<a class="click-open-xpopup" data-options="xpopup:'delete-selled'">删除</a>
		</shiro:hasPermission>
	<# } #>
	<shiro:hasPermission name="goods:manager:edit">
		<a class="click-open-xpopup" data-options="xpopup:'create-duplicate'">复制</a>
	</shiro:hasPermission>
	<shiro:hasPermission name="goods:manager:query">
		<a class="click-open-xpopup" data-options="xpopup:'preview',fillData:false,onAfterFillData:onPreviewAfterFillData">预览</a>
	</shiro:hasPermission>
</script>
<script src="<base:url href='assets/${pv.view}.js'/>"></script>
