<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="base" uri="http://qdbp.gitee.io/tags/base/"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<link href="<base:url href='assets/${pv.view}.css'/>" rel="stylesheet" type="text/css" />

<div class="pure-panel noborder container-fluid goods-edit">
	<div class="pure-prompt color-warn"><div class="text display-none"></div></div>
	<div class="pure-west">
		<form name="input" class="pure-box form-horizontal" method="POST" data-options="novalidate:true"
				<%-- enctype="multipart/form-data" // 目前的实现不支持JAVA服务器中转, 因为采用ajax提交而不是form提交 --%>
				data-action-save="<base:url href='actions/goods/manager/save-draft.json'/>" 
				data-action-submit="<base:url href='actions/goods/manager/submit.json'/>">
			<dd>
				<input type="hidden" name="vid" />
				<input type="hidden" name="uid" />
				<input type="hidden" name="editIndex" />
				<div class="sub-title"><div class="bg">基本信息</div></div>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label"><i class="asterisk"></i>名称</div>
						<div class="col-md-9 control-input"><input class="x-textbox draft-required" type="text" name="name" data-options="required:true,prompt:'商品名称',validType:['illegalChar','length[0,50]']"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label"><i class="asterisk"></i>标题</div>
						<div class="col-md-9 control-input"><input class="x-textbox draft-required" type="text" name="title" data-options="required:true,prompt:'显示给顾客看的标题',validType:['illegalChar','length[0,100]']"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">市场价格</div>
						<div class="col-md-9 control-input"><input class="x-numberbox" type="text" name="marketPriceText" data-options="precision:2,min:0,max:9999999999.99"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label"><i class="asterisk"></i>销售价格</div>
						<div class="col-md-9 control-input"><input class="x-numberbox" type="text" name="priceValue" data-options="required:true,precision:2,min:0,max:9999999999.99"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">所属分类</div>
						<div class="col-md-9 control-input"><input class="x-combotree draft-required" type="text" name="categoryCode" data-options="required:false,editable:false,xextra:{field:'Category'}"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">下架时间</div>
						<div class="col-md-9 control-input"><input class="x-datebox" type="text" name="expireTime" data-options="required:false,editable:false,endTime:true,prompt:'定时自动下架'"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">摘要</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="introText" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
				<%-- <div class="sub-title"><div class="bg">商品图片</div></div> --%>
				<div class="row">
					<div class="col-md-12">
						<div class="x-image-upload">
							<div class="img"><img /></div>
							<input type="hidden" name="image0"/>
							<input class="x-filebox" style="width:130px" name="files0" data-zupload="image0" data-options="validType:['extension[\'图片\',\'jpg|jpeg|png\']'],prompt:'请选择图片',buttonText:'选择'" />
						</div>
						<div class="x-image-upload">
							<div class="img"><img /></div>
							<input type="hidden" name="image1"/>
							<input class="x-filebox" style="width:130px" name="files1" data-zupload="image1" data-options="validType:['extension[\'图片\',\'jpg|jpeg|png\']'], prompt:'请选择图片',buttonText:'选择'" />
						</div>
						<div class="x-image-upload">
							<div class="img"><img /></div>
							<input type="hidden" name="image2"/>
							<input class="x-filebox" style="width:130px" name="files2" data-zupload="image2" data-options="validType:['extension[\'图片\',\'jpg|jpeg|png\']'], prompt:'请选择图片',buttonText:'选择'" />
						</div>
						<div class="x-image-upload">
							<div class="img"><img /></div>
							<input type="hidden" name="image3"/>
							<input class="x-filebox" style="width:130px" name="files3" data-zupload="image3" data-options="validType:['extension[\'图片\',\'jpg|jpeg|png\']'], prompt:'请选择图片',buttonText:'选择'" />
						</div>
						<div class="x-image-upload">
							<div class="img"><img /></div>
							<input type="hidden" name="image4"/>
							<input class="x-filebox" style="width:130px" name="files4" data-zupload="image4" data-options="validType:['extension[\'图片\',\'jpg|jpeg|png\']'], prompt:'请选择图片',buttonText:'选择'" />
						</div>
						<div class="x-image-upload">
							<div class="img"><img /></div>
							<input type="hidden" name="image5"/>
							<input class="x-filebox" style="width:130px" name="files5" data-zupload="image5" data-options="validType:['extension[\'图片\',\'jpg|jpeg|png\']'], prompt:'请选择图片',buttonText:'选择'" />
						</div>
					</div>
				</div>
			</dd>
		</form>
	</div>
	<div class="pure-center">
		<form name="content" class="pure-box form-horizontal" method="POST" data-options="novalidate:true">
			<dd>
				<div class="sub-title"><div class="bg">商品详情</div></div>
				<script class="editor" type="text/plain" style="height:500px;"></script>
			</dd>
			<dd>
				<div class="buttons">
					<a href="javascript:void(0)" class="x-linkbutton" data-options="iconCls:'fa fa-save',disabled:true,onClick:onClickOfSave,fillForm:{},message:'保存'">存草稿</a>
					<a href="javascript:void(0)" class="x-linkbutton" data-options="iconCls:'fa fa-upload',disabled:true,onClick:onConfirmOfSubmit,fillForm:{}">提交</a>
					<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="iconCls:'fa fa-circle-o',disabled:true">重置</a> --%>
					<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'back',iconCls:'fa fa-reply'">返回</a>
				</div>
			</dd>
		</form>
	</div>
</div>

<div class="hide">
	<div class="x-dialog container-fluid" title="提交" data-options="xpopup:'submit',width:500,modal:true,iconCls:'fa fa-upload'">
		<form class="pure-box form-horizontal" method="POST">
			<dd>
				<input type="hidden" name="vid" />
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">唯一编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="uid" data-options="readonly:true,prompt:'自动创建'"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">版本编号</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="vid" data-options="readonly:true,prompt:'自动创建'"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">标题</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="title" data-options="readonly:true"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">提交说明</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="description" data-options="multiline:true,height:50,validType:['length[0,2000]']"/></div>
					</div>
					--%>
				</div>
				<div class="text-center color-warn"> 
					<p>确定已完成该商品的编辑, 需要提交审核吗?</p>
					<p>提交之后将不允许再作修改, 如发现问题必须先由审核人驳回</p>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="onClick:onClickOfSubmit,iconCls:'fa fa-check',message:'提交'">确定</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
</div>

<script src="<base:url href='assets/libs/ueditor/ueditor.config.js'/>" type="text/javascript" charset="utf-8"></script>
<script src="<base:url href='assets/libs/ueditor/ueditor.all.js'/>" type="text/javascript" charset="utf-8"> </script>
<script src="<base:url href='assets/libs/ueditor/lang/zh-cn/zh-cn.js'/>" type="text/javascript" charset="utf-8"></script>

<script src="<base:url href='assets/${pv.view}.js'/>"></script>
