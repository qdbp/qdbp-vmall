<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="base" uri="http://qdbp.gitee.io/tags/base/"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<table class="x-treegrid"
		data-options="toolbar:'.toolbar-box',fit:true,border:false,
		method:'POST',url:'<base:url href='actions/goods/category/list.json'/>',
		fixedParams:{ordering:'sortIndex asc, nodeText asc'},
		idField:'nodeCode',parentField:'parentCode',treeField:'nodeText',
		xextra:{fields:[{field:'Category',key:'nodeCode',value:'nodeText',parent:'parentCode'}],map:{Category:'body'}}">
	<thead>
		<tr>
			<th data-options="field:'nodeText',width:200,align:'left',halign:'center',xpopup:'update'">名称</th>
			<th data-options="field:'createTime',width:150,align:'center'">创建时间</th>
		</tr>
	</thead>
</table>

<div class="hide">
	<div class="x-dialog container-fluid" title="创建" data-options="xpopup:'create',width:500,modal:true,iconCls:'fa fa-plus'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/category/create.json'/>">
			<dd>
				<div class="spacer-right">
					<div class="form-group">
						<div class="col-md-3 control-label">上级类别:</div>
						<div class="col-md-9 control-input"><input class="x-combotree" type="text" name="parentCode" data-def-value="0" data-options="editable:false,xextra:{field:'Category',prepend:{key:'0',value:'根节点'}}"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">类别名称:</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="nodeText" data-options="required:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">排序:</div>
						<div class="col-md-9 control-input"><input class="x-numberbox" type="text" name="sortIndex" data-options="min:0,validType:'length[0,8]',prompt:'自动排到最后'"/></div>
					</div>
					<%--
					<div class="form-group">
						<div class="col-md-3 control-label">类别名称:</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="nodeText" data-options="required:true,multiline:true,height:150,prompt:'可批量创建, 每个类别占一行, -->缩进, 如:\n手机\n-->智能手机\n---->安卓手机\n---->苹果手机\n-->非智能手机'"/></div>
					</div>
					--%>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-save',closeOnSuccess:false,message:'保存'">保存并继续</a> --%>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-save'">保存</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="商品类别" data-options="xpopup:'update',width:500,modal:true,iconCls:'fa fa-pencil'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/category/update.json'/>">
			<dd>
				<div class="spacer-right">
					<input type="hidden" name="nodeCode" />
					<div class="form-group">
						<div class="col-md-3 control-label">上级类别:</div>
						<div class="col-md-9 control-input"><input class="x-combotree" type="text" name="parentCode" data-options="editable:false,xextra:{field:'Category',prepend:{key:'0',value:'根节点'}}"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">类别名称:</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="nodeText" data-options="required:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">排序:</div>
						<div class="col-md-9 control-input"><input class="x-numberbox" type="text" name="sortIndex" data-options="min:0,validType:'length[0,8]'"/></div>
					</div>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'prev',iconCls:'fa fa-arrow-left'"></a> --%>
			<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'prev',iconCls:'fa fa-arrow-left'"></a> --%>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-save'">保存</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="x-dialog container-fluid" title="删除" data-options="xpopup:'delete',width:500,modal:true,iconCls:'fa fa-minus'">
		<form class="pure-box form-horizontal" method="POST" action="<base:url href='actions/goods/category/delete.json'/>">
			<dd>
				<div class="spacer-right">
					<input type="hidden" name="nodeCode" />
					<div class="form-group">
						<div class="col-md-3 control-label">类别名称:</div>
						<div class="col-md-9 control-input"><input class="x-textbox" type="text" name="nodeText" data-options="readonly:true"/></div>
					</div>
					<div class="form-group">
						<div class="col-md-3 control-label">上级类别:</div>
						<div class="col-md-9 control-input"><input class="x-combotree" type="text" name="parentCode" data-options="readonly:true,xextra:{field:'Category',prepend:{key:'0',value:'根节点'}}"/></div>
					</div>
					<div class="form-group text-center">
						<span class="color-warn">一旦删除关联数据将会失效且无法恢复!</span><br>确定删除该类别?
					</div>
				</div>
			</dd>
		</form>
		<div class="dialog-button">
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'submit',iconCls:'fa fa-check',message:'删除'">确定</a>
			<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'close',iconCls:'fa fa-close'">取消</a>
		</div>
	</div>
	<div class="toolbar-box">
		<form>
			<div class="toolbar-item">
				<span class="toolbar-text">名称:</span><input name="nodeTextLike" class="x-textbox panel-input"/>
				<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'search',iconCls:'fa fa-search'">查询</a>
				<a href="javascript:void(0)" class="x-linkbutton" data-options="xaction:'reset'<%--,iconCls:'fa fa-circle-thin'--%>">重置</a>
				<div class="clear"></div>
			</div>
			<div class="toolbar-item">
				<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'details',selection:'single',iconCls:'fa fa-file-text-o'">详情</a> --%>
				<shiro:hasPermission name="goods:category:create">
				<a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'create',iconCls:'fa fa-plus',fillData:{nodeCode:'parentCode'}">创建</a>
				</shiro:hasPermission>
				<shiro:hasPermission name="goods:category:update">
				<a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'update',selection:'single',iconCls:'fa fa-pencil'">修改</a>
				</shiro:hasPermission>
				<shiro:hasPermission name="goods:category:delete">
				<a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'delete',selection:'single',iconCls:'fa fa-minus',fillData:true">删除</a>
				</shiro:hasPermission>
				<shiro:hasPermission name="goods:category:import">
				<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'import',iconCls:'fa fa-sign-in'">导入</a> --%>
				</shiro:hasPermission>
				<shiro:hasPermission name="goods:category:export">
				<%-- <a href="javascript:void(0)" class="x-linkbutton" data-options="xpopup:'export',iconCls:'fa fa-sign-out'">导出</a> --%>
				</shiro:hasPermission>
			</div>
			<div class="clear"></div>
		</form>
	</div>
</div>

<script src="<base:url href='assets/${pv.view}.js'/>"></script>
