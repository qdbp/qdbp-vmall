$.zhh.events.on("/sales/goods/edit/", function(space, initial, extras) {

	var v = { finalProgress:80 };
	
	if (initial.createDuplicate) {
		Utils.succTips("正在创建新版本...");
	}

	v.onSpaceLoadSuccess = function() {
		if (!initial.selected) { // 新增
			space.find("form[name=content]").find(".buttons .x-linkbutton").each(function() {
				$(this).xcall("enable");
			});
			space.xcall("xprogress", 100);
		} else { // 修改
			// space.find("form[name=input]").form("load", initial.selected);

			var url;
			if (initial.createDuplicate) {
				url = Utils.getBaseUrl("actions/goods/manager/create-duplicate.json");
			} else {
				url = Utils.getBaseUrl("actions/goods/manager/details.json");
			}
			$.zajax(url, { vid:initial.selected.vid }, false, {
				succ: function(json) {
					var bean = json && json.body;
					initial.selected = bean;
					space.find("form[name=input]").form("load", bean);
					var images = bean.imageInfo && bean.imageInfo.details || [];
					space.find(".x-image-upload").each(function(i, v) {
						var image = images[i];
						var me = $(this);
						if (image) {
							me.find(".img img").attr("src", image);
							me.find("input[name=image" + i + "]").val(image);
						}
					});
					ue.ready(function() { 
						if (bean.descDetails) { ue.setContent(bean.descDetails); }
					});
					if (initial.readonly) { // 详情
						space.find(".pure-prompt .text").text("只读模式").addClass("active");
					} else {
						if (initial.createDuplicate) {
							Utils.succTips("创建新版本成功");
						} else {
							if (bean.goodsState == "REJECTED") {
								var text = "已被审批驳回, 原因: " + (bean.options && bean.options.approveDesc || "无");
								space.find(".pure-prompt .text").text(text).addClass("active");
							}
						}
						space.find("form[name=content]").find(".buttons .x-linkbutton").each(function() {
							$(this).xcall("enable");
						});
					}

					space.xcall("xprogress", 100);
				},
				fail: function(json) {
					this.failTips = false;
					var msg = json && json.message || "商品详情加载失败!";
					if (json && json.code == "RECORD_NOT_EXIST") { msg = "商品信息不存在, 可能已被其他人删除"; }
					space.find(".pure-prompt .text").text(msg).addClass("active");
					space.xcall("xprogress", "close");
				}
			});
		}
	};

	v.onClickOfSave = function() {
		var form = space.find("form[name=input]");
		var valid = true, first = null;
		form.find(".draft-required").each(function() {
			var me = $(this);
			var temp = me.xcall("enableValidation").xcall("isValid");
			if (!temp) {
				valid = false;
				if (first == null) {
					first = me;
					me.xcall("focus");
				}
			}
		});
		if (!valid) { return; }
		doSubmit.call(this, {
			action: function(e) {
				return e.form.attr("data-action-save");
			},
			succ: function(e) {
				var body = e.json.body;
				e.form.fillForm(true, { vid:body.id, editIndex:body.editIndex });
				var $uid = e.form.find("input[name=uid]");
				if (!$uid.val()) { $uid.val(body.id); }
				$.messager.hint(e.message);
			},
			fail: function(e) {
				e.button.warnning(e.message);
			}
		});
	};
	

	v.onConfirmOfSubmit = function() {
		var btn = $(this);
		var form = space.find("form[name=input]");
		// 验证表单
		var valid = form.form("enableValidation").form("validate");
		if (!valid) {
			return;
		}
		var details = ue.getContent();
		if (!details) { btn.warnning("请输入商品详情"); return; }
		var popup = space.xpopups("submit", "open");
		popup.find("form").form("load", form.serializeJson());
	};
	v.onClickOfSubmit = function() {
		var btn = $(this);
		var popup = btn.xcall("options").xref.popup;
		var input = popup.find("form").find("input[textboxname=description]");
		var description = input.length && input.xcall("getValue") || undefined;
		doSubmit.call(this, {
			fillForm: { description:description },
			action: function(e) {
				return e.form.attr("data-action-submit");
			},
			succ: function(e) {
				$.messager.hint(e.message);
				space.xcall("xclose");
			},
			fail: function(e) {
				e.button.warnning(e.message);
			}
		});
	};

	var doSubmit = function(options) {
		var self = this;
		var btn = $(this);
		var bopts = btn.xcall("options");
		var msg = bopts.message || $.trim(btn.text());
		var form = space.find("form[name=input]");
		var url = options.action.call(self, { form:form });
		var otherData = $.extend({ descDetails: ue.getContent() }, bopts.fillForm, options.fillForm);
		form.zajax(url, otherData, {
			succ: function(json) {
				options.succ.call(self, { form:form, button:btn, json:json, message:msg + "成功" })
			},
			failTips: function(json) {
				var message = json && json.message || (msg + "失败");
				options.fail.call(self, { form:form, button:btn, json:json, message:message });
			}
		});
	};

	space.xui(v);

	var ue = Utils.ueditor.create(space, ".editor", { readonly:initial.readonly });
	
});
