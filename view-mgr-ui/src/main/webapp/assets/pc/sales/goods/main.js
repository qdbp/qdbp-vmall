$.zhh.events.on("/sales/goods/main/", function(space, initial, extras) {

	var v = {};

	v.format = {
		goodsUid: function(value, data, index) {
			if (index == 0) {
				return value;
			} else {
				var table = space.find(".main-datalist");
				var prev = table.xcall("getRows")[index - 1];
				return (prev.uid == data.uid) ? '<span class="color-weak">' + value + '</span>' : value; // "↑"
			}
		}
	};
	// v.cleanDuplicateValue = function(row, field, json, options) {
	// 	return row.versionState == "ACTIVATED" ? row[field] : undefined;
	// };
	v.onClickRowOfCategory = function(row){
		var mainlist = space.find(".main-datalist");
		var form = mainlist.xcall("getPanel").find(".toolbar-box form");
		var deptCode = row.deptCode == "0" ? "" : row.deptCode;
		form.form("load", { deptCodeStarts:deptCode });
		mainlist.xcall("reload");
	};
	v.onChooseDuplicateAfterFillData = function(e) {
		var datalist = e.popup.find(".duplicate-datalist");
		var opts = datalist.xcall("options");
		opts.url = opts.tmpurl;
		opts.fixedParams.uid = e.selected.uid;
		datalist.xcall("reload");
	};
	v.onPreviewAfterFillData = function(e) {
		var iframe = e.popup.find("iframe").get(0);
		iframe.src = "";
		var url = Utils.getBaseUrl("actions/goods/manager/preview-url.json");
		$.zajax(url, {vid:e.selected.vid}, function(json) {
			if (json && json.body) {
				iframe.src = json.body;
			}
		});
	};
	v.onClickOfCreate = function(e) {
		space.xcall("xopen", {
			title:"创建商品",
			hash:"/sales/goods/edit/"
		});
	};
	v.onClickOfEnterEdit = function(e) { // 直接进入编辑
		var btn = $(this);
		// readonly:true
		var enterOptions = btn.xcall("options").enterOptions || {};
		var options = $.extend({}, enterOptions, {
			id: e.selected.vid,
			title: e.selected.name,
			selected: e.selected,
			hash:"/sales/goods/edit/"
		});
		space.xcall("xopen", options);
	};

	space.xui(v);
});
