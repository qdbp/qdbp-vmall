$.zhh.events.on("/sales/order/main/", function(space, initial, extras) {

	var v = {};
	
	v.format = {
		goodsId: function(value, data, index) {
			if (index == 0) {
				return value;
			} else {
				var table = space.find(".main-datalist");
				var prev = table.xcall("getRows")[index - 1];
				return (prev.goodsId == data.goodsId) ? '<span class="color-weak">' + value + '</span>' : value; // "↑"
			}
		}
	};
	
	v.onDetailAfterFillData = function(e) {
		var datalist = e.popup.find(".item-datalist");
		var opts = datalist.xcall("options");
		opts.url = opts.tmpurl;
		opts.fixedParams.id = e.selected.id;
		datalist.xcall("reload");
	};

	space.xui(v);
});
