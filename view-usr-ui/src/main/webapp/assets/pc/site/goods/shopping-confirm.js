+function() {
	$(function() {
		var space = $(document.body);
		var skus = $.getPageParams("skus");

		// 数量增减计算金额
		var calculateAllTotalAmount = function() {
			var amount = 0;
			space.find(".amount .quantity input[name=quantity]").each(function() {
				var row = $(this).closest(".data-row");
				var price = row.find(".price .value").text() * 1;
				if (!price) { amount = -1; return false; }
				var quantity = $(this).val() * 1;
				if (!quantity) { amount = -1; return false; }
				amount += quantity * price;
			});
			if (amount >= 0) {
				space.find(".all-amount .value").text(amount.toFixed(2));
			}
		};
		
		if (skus) {
			space.find(".goods-list").fillContent({
				fixedParams: { skuIds:skus.split(/\s*,\s*/g) },
				succ: function() {
					var me = $(this.element);
					me.find(".amount").quantity("init", calculateAllTotalAmount);
				}
			});
		}
		
		space.find(".block-list .item:not(.create)").click(function() {
			var me = $(this);
			me.closest(".block-list").find(".item.success").removeClass("success");
			me.addClass("success");
		});

		// 提交订单按钮
		space.find(".purchase button.do-submit").click(function() {
			// 1.检查是否选中
			// 2.获取数据
			// 3.提交
			window.location.replace("../shopping-pay/");
		});
	});
}();