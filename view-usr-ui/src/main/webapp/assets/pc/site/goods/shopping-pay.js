+function() {
	$(function() {
		var space = $(document.body);

		space.find(".block-list .item:not(.create)").click(function() {
			var me = $(this);
			me.closest(".block-list").find(".item.success").removeClass("success");
			me.addClass("success");
		});
	});
}();