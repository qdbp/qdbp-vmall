+function() {
	$(function() {
		var space = $(document.body);

		// 图片预览
		space.find(".goods-box .image-box .pvw-image").hover(function() {
			var me = $(this);
			me.addClass("active").siblings(".pvw-image").removeClass("active");
			var img = me.find("img");
			var src = img.attr("data-src") || img.attr("src");
			me.closest(".image-box").find(".main-image img").attr("src", src);
		});

		// 数量增减计算金额
		space.find(".amount").quantity("init");

		if (space.attr("data-goods")) {
			// 初始化
			var data = space.zoptions("goods");
			var init = function(goods) {
				space.find(".goods-box").fillData(goods);
				space.find(".amount").quantity("calculate");
			};
			var printTime = Dates.parse(data.printTime);
			if (printTime && new Date().getTime() - printTime.getTime() < 10 * 60 * 1000) {
				init(data);
			} else {
				var url = Utils.getBaseUrl("actions/goods/details.json");
				$.zajax(url, {uid:data.uid}, {
					succ:function(json) {
						init(json.body);
					}
				});
			}
			// 加入购物车
			space.find(".do-add-shopping-cart").click(function() {
				var quantity = space.find("input[name=quantity]").val();
				var url = Utils.getBaseUrl("actions/goods/shopping-cart/add.json");
				$(this).zajax(url, {goodsVid:data.vid, quantity:quantity}, {
					checkLogin: true,
					failTips: function(json) { // 显示失败消息
						Utils.warnTips(json && json.message || "网络异常，请稍后重试！");
					}
				});
			});
		}
	});
}();