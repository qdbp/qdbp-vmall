+function() {
	$(function() {
		var space = $(document.body);

		// 数量增减计算金额
		var calculateAllTotalAmount = function() {
			var amount = 0;
			space.find(".amount .quantity input[name=quantity]").each(function() {
				var row = $(this).closest(".data-row");
				var checkbox = row.find(".col-checkbox input[type=checkbox]");
				if (checkbox.prop("checked")) {
					var price = row.find(".price .value").text() * 1;
					if (!price) { amount = -1; return false; }
					var quantity = $(this).val() * 1;
					if (!quantity) { amount = -1; return false; }
					amount += quantity * price;
				}
			});
			if (amount >= 0) {
				space.find(".all-amount .value").text(amount.toFixed(2));
			}
		};
		var checkboxChanged = function() {
			var all = space.find(".data-row .col-checkbox input[type=checkbox]:enabled");
			var checked = all.filter(":checked");
			var state = all.length > 0 && all.length == checked.length;
			space.find(".title-row .col-checkbox input[type=checkbox]").prop("checked", state);
			space.find(".checked-rows").text(checked.length);
			calculateAllTotalAmount();
		};
		space.find(".goods-list").fillContent(function() {
			var me = $(this.element);
			var rows = space.find(".data-row .col-checkbox input[type=checkbox]:enabled");
			space.find(".all-rows").text(rows.length);
			// 数量组件初始化
			me.find(".data-row").quantity("init", function(value) {
				var input = $(this);
				var box = input.closest(".data-row");
				box.find(".col-checkbox input[type=checkbox]").prop("checked", true);
				checkboxChanged();
				// 数量更新保存到数据库
				var id = box.attr("data-id");
				var url = Utils.getBaseUrl("actions/goods/shopping-cart/update-quantity.json");
				var data = { shoppingCartId:id, quantity:value };
				var options = { OPTIONS:true, loading:false, succTips:false, failTipsOnButton:false, message:"数量保存" };
				input.zajax(url, data, options);
			});
			// 选择行首的checkbox
			me.find(".data-row .col-checkbox input[type=checkbox]").click(function() {
				checkboxChanged();
				// 状态更新保存到数据库
				var checkbox = $(this);
				var url = Utils.getBaseUrl("actions/goods/shopping-cart/update-checked.json");
				var data = { shoppingCartId:checkbox.val(), checked:checkbox.prop("checked") };
				var options = { OPTIONS:true, loading:false, succTips:false, failTipsOnButton:false, message:"状态保存" };
				$.zajax(url, data, options);
			});
			// 删除操作
			me.find("a.delete").click(function() {
				var box = $(this).closest(".data-row");
				var id = box.attr("data-id");
				var dialog = space.find(".modal.delete-item");
				dialog.find("form").fillForm({ shoppingCartId:id });
				dialog.modal("show");
			});
			checkboxChanged();
		});
		
		// 全选/全不选
		space.find(".title-row .col-checkbox input[type=checkbox]").click(function() {
			var checked = $(this).prop("checked");
			var all = space.find(".data-row .col-checkbox input[type=checkbox]:enabled");
			if (all.length == 0) { return; }
			all.prop("checked", checked);
			space.find(".checked-rows").text(checked ? all.length : 0);
			calculateAllTotalAmount();
			// 状态更新保存到数据库
			var url = Utils.getBaseUrl("actions/goods/shopping-cart/update-checked.json");
			var data = { shoppingCartId:"*", checked:checked };
			var options = { OPTIONS:true, loading:false, succTips:false, failTipsOnButton:false, message:"状态保存" };
			$.zajax(url, data, options);
		});

		// 删除对话框的提交处理
		space.find(".modal.delete-item").find("button.do-submit").click(function() {
			var btn = $(this);
			var dialog = btn.closest(".modal");
			var form = dialog.find("form");
			btn.zajax(form, { 
				message:"删除", 
				succ: function() {
					dialog.modal("hide");
					var table = space.find(".goods-list");
					table.find(".data-row[data-id='"+this.data.shoppingCartId+"']").remove();
					// 更新共有几件商品的提示
					var rows = space.find(".data-row .col-checkbox input[type=checkbox]:enabled");
					space.find(".all-rows").text(rows.length);
					if (rows.length == 0) { // 没有商品时显示暂无数据
						table.parent().find(".data-not-found").removeClass("hide");
					}
					checkboxChanged();
				}
			});
		});
		
		// 去结算按钮
		space.find(".purchase button.do-submit").click(function() {
			var me = $(this);
			var table = space.find(".goods-list");
			// 1.检查是否选中
			var checked = table.find(".data-row .col-checkbox input[type=checkbox]:enabled:checked");
			if (checked.length == 0) {
				me.msger("请勾选需要结算的商品");
				return;
			}
			// 2.获取选中数据
			var skuIds = [];
			checked.each(function() {
				var skuId = $(this).closest(".data-row").attr("data-sku-id");
				skuIds.push(skuId);
			});
			// 3.提交
			window.location.href = "../shopping-confirm/?skus=" + skuIds.join(",");
		});
	});
}();