<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="base" uri="http://qdbp.gitee.io/tags/base/"%>

<header class="ihead">
	<div class="container">
		<div class="info-box pull-left user-profile">
			<div class="login-before">
				<a href="<base:url href='auth/login/' />">登录</a>
				<a href="<base:url href='auth/register/' />">注册</a>
			</div>
			<div class="login-after hide">
				欢迎 <a href="<base:url href='pc/mine/personnel/user-profile/' />" data-fill="field:'nickName||realName||userCode||userName||phone||email',defaultValue:'(匿名用户)'">某某某</a>
				[<a href="<base:url href='auth/logout/'/>">退出</a>]
			</div>
		</div>
		<div class="link-box pull-right">
			<a href="<base:url href='goods/shopping-cart/' />">购物车</a>
			<a href="<base:url href='pc/mine/personnel/user-profile/' />">个人中心</a>
		</div>
	</div>
</header>
<nav class="inav">
	<div class="container">
		<div class="logo-box">
			<img class="logo-icon" alt="${pv.project.name}" src="<base:url href='assets/img/common/logo.sm.png' />" />
		</div>
		<div class="link-box">
			<a href="<base:url href='/' />">首页</a>
			<a href="#">商品中心</a>
			<a href="#">帮助中心</a>
		</div>
		<form class="search-box form-inline" onsubmit="return false">
			<div class="form-group">
				<div class="input-group input-btn-box">
					<input type="text" class="form-control" placeholder="站内搜索" />
					<div class="input-group-btn"> <button class="btn btn-default"><i class="fa fa-search"></i></button> </div>
				</div>
			</div>
		</form>
	</div>
</nav>
