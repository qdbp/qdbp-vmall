<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" uri="http://www.bdp.com/tags/base/"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../_/meta.jsp"%>
	<title>确认订单 - ${pv.project.name}</title>
	<%@ include file="../../_/style.jsp"%>
	<link href="<base:url href='assets/pc/site/goods/shopping.css'/>" rel="stylesheet" type="text/css" />
</head>

<body>
	<%@ include file="../../_/header.jsp"%>
	<header class="breadcrumbs bg-light-grey">
		<div class="container">
			<h3 class="mtb-xs">支付订单</h3>
		</div>
	</header>

	<section class="ibody bg-light-grey pb-lg">
		<div class="container">
			<div class="main-panel">
				<div class="block-box order-info bg-white">
					<div class="title color-def fz-lg">订单信息</div>
					<div class="ptb-sm">
						<ul class="row compact">
							<li class="col-xs-2 text-right">订单编号: </li>
							<li class="col-xs-10">1807085579</li>
						</ul>
						<ul class="row compact">
							<li class="col-xs-2 text-right">应付金额: </li>
							<li class="col-xs-10"><span class="color-main">8888</span>元	</li>
						</ul>
						<ul class="row compact">
							<li class="col-xs-2 text-right">商品信息: </li>
							<li class="col-xs-10">
								<div>小米6X 全网通版 4GB内存 64GB 冰川蓝 1599元 × 2</div>
								<div>小米MIX 2S 全网通版 6GB内存 陶瓷标准版 64GB 白色 64GB 3299元 × 1</div>
								<div>米粉卡日租卡 0元 × 1</div>
							</li>
						</ul>
						<ul class="row compact">
							<li class="col-xs-2 text-right">寄送至: </li>
							<li class="col-xs-10">江苏南京江宁淳化芝兰路18号软通动力2号楼204</li>
						</ul>
						<ul class="row compact">
							<li class="col-xs-2 text-right">收货人: </li>
							<li class="col-xs-10">赵卉华 13913001382</li>
						</ul>
						<ul class="row compact">
							<li class="col-xs-2 text-right">有效期: </li>
							<li class="col-xs-10">请在 <span class="color-warn">2018-07-08 15:00:00</span> 前完成付款, 过期后订单将自动关闭</li>
						</ul>
					</div>
				</div>
				<div class="block-box mt-sm bg-white">
					<div class="title color-def fz-lg">选择支付平台</div>
					<div class="block-list">
						<ul class="row compact">
							<li class="col-xs-6 col-sm-4 col-md-3">
								<div class="item success">
									<div class="bg-white text-center">
										<img alt="支付宝" src="<base:url href='assets/img/trade/alipay.png'/>" />
									</div>
								</div>
							</li>
							<li class="col-xs-6 col-sm-4 col-md-3">
								<div class="item">
									<div class="bg-white text-center">
										<img alt="微信支付" src="<base:url href='assets/img/trade/wechatpay.png'/>" />
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="purchase mt-sm bg-white">
					<ul class="row compact">
						<li class="col-xs-8 col-sm-9 col-md-10 text-right all-amount">
							应付金额:&nbsp;&nbsp;<span class="value color-main fz-xl">8888</span>&nbsp;元
						</li>
						<li class="col-xs-4 col-sm-3 col-md-2 buttons">
							<button class="btn btn-success btn-block radius-clear fz-lg">立即支付</button>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<%@ include file="../../_/footer.jsp"%>

	<%@ include file="../../_/script.jsp"%>
	<script src="<base:url href='assets/base/core/bootstrap/plugins-init.js'/>" type="text/javascript" data-zload-ignore="true"></script>
	<script src="<base:url href='assets/pc/site/goods/quantity.js'/>" type="text/javascript"></script>
	<script src="<base:url href='assets/pc/site/goods/shopping-pay.js'/>" type="text/javascript"></script>
	<%@ include file="../../_/analysis.jsp"%>
</body>
</html>
