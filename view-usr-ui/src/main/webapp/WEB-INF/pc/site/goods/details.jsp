<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" uri="http://qdbp.gitee.io/tags/base/"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../_/meta.jsp"%>
	<title>${pv.goods.title} - ${pv.project.name}</title>
	<%@ include file="../../_/style.jsp"%>
	<link href="<base:url href='assets/libs/zhh.tools/quantity.css'/>" rel="stylesheet" type="text/css" />
	<link href="<base:url href='assets/pc/site/goods/details.css'/>" rel="stylesheet" type="text/css" />
</head>

<body data-goods="{vid:'${pv.goods.vid}',uid:'${pv.goods.uid}',marketPriceText:'${pv.goods.marketPriceText}',salesPriceText:'${pv.goods.salesPriceText}',priceValue:'${pv.goods.priceValue}',goodsState:'${pv.goods.goodsState}',printTime:'${pv.now}'}">
	<%@ include file="../../_/header.jsp"%>
	<section class="ibody">
		<div class="container bg-clear">
			<div class="row pt-md goods-box">
				<div class="col-xs-12 col-sm-6 image-box">
					<div class="border">
						<div class="main-image">
							<core:if test="${not empty pv.goods.imageInfo.details}">
							<img src="${pv.goods.imageInfo.details[0]}" />
							</core:if>
							<core:if test="${empty pv.goods.imageInfo.details}">
							<div class="image-lost"><span class="vertical-middle">暂无图片</span></div>
							</core:if>
						</div>
						<core:if test="${not empty pv.goods.imageInfo.details}">
						<div class="pvw-images">
							<core:forEach items="${pv.goods.imageInfo.details}" var="img" varStatus="s">
							<div class="pvw-image <core:if test='${s.index == 0}'>active</core:if>">
								<img src="${img}" />
							</div>
							</core:forEach>
						</div>
						</core:if>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 info-box">
					<div>
						<h2 class="title color-def">${pv.goods.title}</h2>
						<core:if test="${not empty pv.goods.introText}">
						<div class="intro color-weak pt-xs">${pv.goods.introText}</div>
						</core:if>
						<div class="price pt-sm">
							<div class="hide" data-fill="showWith:function(d){return !!d.marketPriceText}">原价: <span class="del" data-fill="marketPriceText">--</span>元</div>
							<div>售价: <span class="value color-main" data-fill="field:'salesPriceText||priceValue'">--</span>元</div>
						</div>
						<core:if test="${not empty pv.goods.specificKindData}">
						<div class="specifics">
						</div>
						</core:if>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-sm-offset-6 height-auto">
					<div class="pst-relative">
						<div class="purchase bg-light-grey">
							<div class="on-active" data-fill="showWith:{goodsState:'SELLING'}">
								<div class="title color-def">${pv.goods.title}</div>
								<div class="row amount pt-xs">
									<div class="col-xs-4 price text-left">单价: <span class="value color-main" data-fill="priceValue">--</span>元</div>
									<div class="col-xs-4 quantity text-center">
										<div class="quantity-box">
											<button class="minus">-</button>
											<input type="text" name="quantity" value="1" />
											<button class="plus">+</button>
										</div>
									</div>
									<div class="col-xs-4 subtotal text-right">金额: <span class="value color-main">--</span>元</div>
								</div>
								<div class="buttons pt-sm text-right">
									<a class="btn btn-success radius-clear do-add-shopping-cart">加入购物车</a>
								</div>
							</div>
							<div class="on-inactive text-center hide" data-fill="hideWith:{goodsState:'SELLING'}">
								<span class="prompt color-warn fz-lg">此商品已下架</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="ibody mt-lg">
		<div class="container">
			<div class="row ptb-xs">
				<div class="col-xs-12">
					<div class="rich-text-box">
						${pv.goods.descDetails}
					</div>
				</div>
			</div>
		</div>
	</section>
	<%@ include file="../../_/footer.jsp"%>


	<%@ include file="../../_/script.jsp"%>
	<script src="<base:url href='assets/libs/zhh.tools/quantity.js'/>" type="text/javascript"></script>
	<script src="<base:url href='assets/base/core/bootstrap/plugins-init.js'/>" type="text/javascript" data-zload-ignore="true"></script>
	<script src="<base:url href='assets/pc/site/goods/details.js'/>" type="text/javascript"></script>
	<%@ include file="../../_/analysis.jsp"%>
</body>
</html>
