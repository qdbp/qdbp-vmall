<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" uri="http://www.bdp.com/tags/base/"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../_/meta.jsp"%>
	<title>购物车 - ${pv.project.name}</title>
	<%@ include file="../../_/style.jsp"%>
	<link href="<base:url href='assets/libs/bs-check/bs-check.css'/>" rel="stylesheet" type="text/css" />
	<link href="<base:url href='assets/libs/zhh.tools/quantity.css'/>" rel="stylesheet" type="text/css" />
	<link href="<base:url href='assets/pc/site/goods/shopping.css'/>" rel="stylesheet" type="text/css" />
</head>

<body>
	<%@ include file="../../_/header.jsp"%>
	<header class="breadcrumbs bg-light-grey">
		<div class="container">
			<a href="<base:url href='/' />">首页</a> &gt; <a>购物车</a>
		</div>
	</header>
	
	<section class="ibody bg-light-grey pb-lg">
		<div class="container">
			<div class="main-panel">
				<div class="goods-list table-box bg-white"
						data-fill="url:'<base:url href='actions/goods/shopping-cart/list.json'/>',
							fixedParams:{ ordering:'createTime desc', needCount:false, paging:false },
							clear:'.data-row',template:'data-rows',appendTo:'this',
							space:'parent',loadingElement:'.data-loading',notFoundElement:'.data-not-found'">
					<ul class="table-row title-row">
						<li class="table-cell col-checkbox">
							<label class="checkbox"><input class="bs-check" type="checkbox"/><span class="bs-check"></span></label>
						</li>
						<li class="table-cell col-image">&nbsp;</li>
						<li class="table-cell col-info">商品信息</li>
						<li class="table-cell col-price">价格</li>
						<li class="table-cell col-quantity">数量</li>
						<li class="table-cell col-total hidden-xs">小计</li>
						<li class="table-cell col-operate">操作</li>
					</ul>
				</div>
				<div class="data-loading bg-white border-top"></div>
				<div class="data-not-found bg-white border-top hide"> <div class="content">暂无数据</div> </div>
				<div class="purchase mt-sm bg-white">
					<ul class="row compact">
						<li class="col-xs-5 pl-sm pr-clear">
							共&nbsp;<span class="color-main all-rows">0</span>&nbsp;件商品, 已选择&nbsp;<span class="color-main checked-rows">0</span>&nbsp;件
						</li>
						<li class="col-xs-3 col-sm-4 col-md-5 text-right all-amount">
							合计:&nbsp;&nbsp;<span class="value color-main fz-xl">0</span>&nbsp;元
						</li>
						<li class="col-xs-4 col-sm-3 col-md-2 buttons">
							<button class="btn btn-success btn-block radius-clear fz-lg do-submit">去结算</button>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade delete-item" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="关闭"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">确认</h4>
				</div>
				<form class="modal-body" action="<base:url href='actions/goods/shopping-cart/delete.json'/>">
					<input type="hidden" name="shoppingCartId" />
					<div class="color-warn ptb-sm text-center fz-lg">确定要从购物车删除该商品吗&hellip;</div>
				</form>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
					<button type="button" class="btn btn-success do-submit plr-sm">确定</button>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="../../_/footer.jsp"%>


	<%@ include file="../../_/script.jsp"%>
	<script src="<base:url href='assets/libs/zhh.tools/quantity.js'/>" type="text/javascript"></script>
	<script src="<base:url href='assets/base/core/bootstrap/plugins-init.js'/>" type="text/javascript" data-zload-ignore="true"></script>
	<script src="<base:url href='assets/pc/site/goods/shopping-cart.js'/>" type="text/javascript"></script>
	<%@ include file="../../_/analysis.jsp"%>

<script type="data-rows">
<# $.each(this, function() {#>
	<ul class="table-row data-row amount" data-id="<#= this.id #>" data-sku-id="<#= this.skuId #>">
		<li class="table-cell col-checkbox">
			<label class="checkbox">
				<input class="bs-check" type="checkbox" name="id" value="<#=this.id#>" <# if (this.goods.goodsState != "SELLING") { #>disabled<#} else if (this.checked) { #>checked<#}#> />
				<span class="bs-check"></span>
			</label>
		</li>
		<li class="table-cell col-image">
			<# if (this.goods.imageInfo && this.goods.imageInfo.preview) { #>
			<img src="<#=this.goods.imageInfo.preview#>" />
			<# } else { #>
			<div class="image-lost"><span class="vertical-middle">暂无图片</span></div>
			<# } #>
		</li>
		<li class="table-cell col-info">
			<p class="fz-lg"><a class="color-def" href="<#=Utils.getBaseUrl('goods/'+this.goods.uid+'/')#>"><#=this.goods.title#></a></p>
			<# if(this.sku && this.sku.specificText) { #><p class="color-weak"><#=this.sku.specificText#></p><#}#>
			<p class="fz-sm color-weak">加入时间: <#=this.createTime#></p>
			<# if (this.goodsVid != this.goods.vid) { #>
			<p class="fz-sm color-weak">此商品已于<#=this.goods.publishTime#>被重新编辑, <a class="color-weak">查看原版本&gt;&gt;</a></p>
			<# } #>
		</li>
		<li class="table-cell col-price price">
			<p><span class="value"><#= this.sku ? this.sku.priceValue : this.goods.priceValue #></span>元</p>
			<# if (this.priceChange > 0) { #>
			<p class="fz-sm color-weak">已涨价<span class="color-warn"><#=this.priceChange#></span>元</p>
			<# } else if (this.priceChange < 0) { #>
			<p class="fz-sm color-weak">已降价<span class="color-warn"><#=-this.priceChange#></span>元</p>
			<# } #>
		</li>
		<li class="table-cell col-quantity quantity plr-clear">
			<# if (this.goods.goodsState != "SELLING") { #>
				<p class="fz-sm color-weak">商品已下架</p>
			<# } else { #>
			<div class="quantity-box">
				<button class="minus">-</button>
				<input type="text" name="quantity" value="<#=this.quantity#>" />
				<button class="plus">+</button>
			</div>
			<# } #>
		</li>
		<li class="table-cell col-total subtotal hidden-xs">
			<# if (this.goods.goodsState == "SELLING") { #>
			<p><span class="value color-main"><#= ( this.sku ? this.sku.priceValue : this.goods.priceValue ) * this.quantity #></span>元</p>
			<# } #>
		</li>
		<li class="table-cell col-operate">
			<a title="删除" class="delete"><span class="fa fa-trash-o"></span></a>
		</li>
	</ul>
<# }); #>
</script>
</body>
</html>
