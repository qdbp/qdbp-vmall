<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" uri="http://www.bdp.com/tags/base/"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../_/meta.jsp"%>
	<title>确认订单 - ${pv.project.name}</title>
	<%@ include file="../../_/style.jsp"%>
	<link href="<base:url href='assets/libs/zhh.tools/quantity.css'/>" rel="stylesheet" type="text/css" />
	<link href="<base:url href='assets/pc/site/goods/shopping.css'/>" rel="stylesheet" type="text/css" />
</head>

<body>
	<%@ include file="../../_/header.jsp"%>
	<header class="breadcrumbs bg-light-grey">
		<div class="container">
			<h3 class="mtb-xs">确认订单</h3>
		</div>
	</header>

	<section class="ibody bg-light-grey pb-lg">
		<div class="container">
			<div class="main-panel">
				<div class="goods-list table-box bg-white"
						data-fill="url:'<base:url href='actions/goods/shopping-cart/list.json'/>',
							fixedParams:{ ordering:'createTime desc', needCount:false, paging:false },
							clear:'.data-row',template:'data-rows',appendTo:'this',
							space:'parent',loadingElement:'.data-loading',notFoundElement:'.data-not-found'">
					<ul class="table-row title-row">
						<li class="table-cell col-image">&nbsp;</li>
						<li class="table-cell col-info">商品信息</li>
						<li class="table-cell col-price">价格</li>
						<li class="table-cell col-quantity">数量</li>
						<li class="table-cell col-total">小计</li>
					</ul>
				</div>
				<div class="data-loading bg-white border-top"></div>
				<div class="data-not-found bg-white border-top hide"> <div class="content">暂无数据</div> </div>
				<div class="table-box bg-white hide">
					<ul class="table-row info-row">
						<li class="table-cell col-auto border-top pl-sm">共&nbsp;<span class="color-main">3</span>&nbsp;件商品</li>
						<li class="table-cell col-label border-top">合计:</li>
						<li class="table-cell col-total border-top"><span class="value color-main">0</span>&nbsp;元</li>
					</ul>
					<ul class="table-row info-row">
						<li class="table-cell col-auto border-top">&nbsp;</li>
						<li class="table-cell col-label border-top">+运费:</li>
						<li class="table-cell col-total border-top"><span class="value color-main">0</span>&nbsp;元</li>
					</ul>
					<ul class="table-row info-row">
						<li class="table-cell col-auto">&nbsp;</li>
						<li class="table-cell col-label border-top">-活动:</li>
						<li class="table-cell col-total border-top"><span class="value color-main">0</span>&nbsp;元</li>
					</ul>
					<ul class="table-row info-row">
						<li class="table-cell col-auto">&nbsp;</li>
						<li class="table-cell col-label border-top">=实付:</li>
						<li class="table-cell col-total border-top"><span class="value color-main fz-xl">0</span>&nbsp;元</li>
					</ul>
				</div>
				<div class="block-box mt-sm bg-white">
					<div class="title color-def fz-lg">选择收货地址</div>
					<div class="block-list">
						<ul class="row compact">
							<li class="col-xs-6 col-sm-4 col-md-3">
								<div class="item success">
									<div class="bg-white">
										<div class="">江苏南京 (赵卉华 收)</div>
										<div class="">软通动力204 <span class="inline-block">(电话: 13913001382)</span></div>
									</div>
								</div>
							</li>
							<li class="col-xs-6 col-sm-4 col-md-3">
								<div class="item">
									<div class="bg-white">
										<div class="">江苏南京 (赵卉华 收)</div>
										<div class="">软通动力2号楼204 <span class="inline-block">(电话: 13913001382)</span></div>
									</div>
								</div>
							</li>
							<li class="col-xs-6 col-sm-4 col-md-3">
								<div class="item">
									<div class="bg-white">
										<div class="">江苏南京 (赵卉华 收)</div>
										<div class="">江宁淳化芝兰路18号软通动力2号楼204 <span class="inline-block">(电话: 13913001382)</span></div>
									</div>
								</div>
							</li>
							<li class="col-xs-6 col-sm-4 col-md-3">
								<div class="item create">
									<div class="bg-white">
										<div><i class="fa fa-plus-circle fa-2x"></i></div>
										<div>创建新地址</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="purchase mt-sm bg-white">
					<ul class="row compact">
						<li class="col-xs-12">
							<div class="description">
								留言: <input name="description" type="text" class="form-control inline"/>
							</div>
						</li>
					</ul>
					<ul class="row compact">
						<li class="col-xs-12">
							<div class="address">
								<div>寄送至: 江苏南京江宁淳化芝兰路18号软通动力2号楼204</div>
								<div>收货人：赵卉华 13913001382</div>
							</div>
						</li>
					</ul>
					<ul class="row compact">
						<li class="col-xs-5 pl-sm pr-clear"> &nbsp; </li>
						<li class="col-xs-3 col-sm-4 col-md-5 text-right all-amount">
							实付:&nbsp;&nbsp;<span class="value color-main fz-xl">0</span>&nbsp;元
						</li>
						<li class="col-xs-4 col-sm-3 col-md-2 buttons">
							<button class="btn btn-success btn-block radius-clear fz-lg do-submit">提交订单</button>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<%@ include file="../../_/footer.jsp"%>


	<%@ include file="../../_/script.jsp"%>
	<script src="<base:url href='assets/libs/zhh.tools/quantity.js'/>" type="text/javascript"></script>
	<script src="<base:url href='assets/base/core/bootstrap/plugins-init.js'/>" type="text/javascript" data-zload-ignore="true"></script>
	<script src="<base:url href='assets/pc/site/goods/shopping-confirm.js'/>" type="text/javascript"></script>
	<%@ include file="../../_/analysis.jsp"%>

<script type="data-rows">
<# $.each(this, function() {#>
	<ul class="table-row data-row amount" data-id="<#= this.id #>">
		<li class="table-cell col-image">
			<# if (this.goods.imageInfo && this.goods.imageInfo.preview) { #>
			<img src="<#=this.goods.imageInfo.preview#>" />
			<# } else { #>
			<div class="image-lost"><span class="vertical-middle">暂无图片</span></div>
			<# } #>
		</li>
		<li class="table-cell col-info">
			<p class="fz-lg"><a class="color-def" href="<#=Utils.getBaseUrl('goods/'+this.goods.uid+'/')#>"><#=this.goods.title#></a></p>
			<# if(this.sku && this.sku.specificText) { #><p class="color-weak"><#=this.sku.specificText#></p><#}#>
		</li>
		<li class="table-cell col-price price">
			<p><span class="value"><#= this.sku ? this.sku.priceValue : this.goods.priceValue #></span>元</p>
		</li>
		<li class="table-cell col-quantity quantity plr-clear">
			<# if (this.goods.goodsState != "SELLING") { #>
				<p class="fz-sm color-weak">商品已下架</p>
			<# } else { #>
			<div class="quantity-box">
				<button class="minus">-</button>
				<input type="text" name="quantity" value="<#=this.quantity#>" />
				<button class="plus">+</button>
			</div>
			<# } #>
		</li>
		<li class="table-cell col-total subtotal hidden-xs">
			<# if (this.goods.goodsState == "SELLING") { #>
			<p><span class="value color-main"><#= ( this.sku ? this.sku.priceValue : this.goods.priceValue ) * this.quantity #></span>元</p>
			<# } #>
		</li>
	</ul>
<# }); #>
</script>
</body>
</html>
