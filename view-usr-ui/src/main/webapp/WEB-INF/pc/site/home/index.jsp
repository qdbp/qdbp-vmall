<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="base" uri="http://qdbp.gitee.io/tags/base/"%>

<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../_/meta.jsp"%>
	<title>首页 - ${pv.project.name}</title>
	<%@ include file="../../_/style.jsp"%>
	<link href="<base:url href='assets/pc/site/home/index.css'/>" rel="stylesheet" type="text/css" />
</head>

<body>
	<%@ include file="../../_/header.jsp"%>
	<section class="ibody bg-light-grey pb-lg">
		<div class="container bg-clear">
			<div class="row ptb-xs">
				<div class="col-xs-12"><div class="bg-white">&nbsp;</div></div>
			</div>
			<div class="row compact hover-pop-box">
				<core:forEach items="${pv.goods.selling.list}" var="goods" varStatus="s">
				<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2_4 row-padding">
					<dl class="item-block bg-white">
						<dt>
							<a href="<base:url href='goods/${goods.uid}/'/>" class="img"><img src="${goods.imageInfo.preview}" /></a>
						</dt>
						<dd>
							<a href="<base:url href='goods/${goods.uid}/'/>" class="title color-def ellipsis">${goods.title}</a>
							<a href="<base:url href='goods/${goods.uid}/'/>" class="intro color-weak ellipsis">
								<core:if test="${empty goods.introText}">&nbsp;</core:if>
								<core:if test="${not empty goods.introText}">${goods.introText}</core:if>
							</a>
							<a href="<base:url href='goods/${goods.uid}/'/>" class="price">
								<span class="value color-main"><core:if test="${not empty goods.salesPriceText}">${goods.salesPriceText}</core:if><core:if test="${empty goods.salesPriceText and not empty goods.priceValue}">${goods.priceValue}</core:if>元</span>
								<core:if test="${not empty goods.marketPriceText}"><span class="del color-weak">${goods.marketPriceText}元</span></core:if>
							</a>
						</dd>
					</dl>
				</div>
				</core:forEach>
			</div>
		</div>
	</section>
	<%@ include file="../../_/footer.jsp"%>


	<%@ include file="../../_/script.jsp"%>
	<script src="<base:url href='assets/base/core/bootstrap/plugins-init.js'/>" type="text/javascript" data-zload-ignore="true"></script>
	<script src="<base:url href='assets/pc/site/home/index.js'/>" type="text/javascript"></script>
	<%@ include file="../../_/analysis.jsp"%>
</body>
</html>
