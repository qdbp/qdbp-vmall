package com.gitee.qdbp.vmall.web.mgr.views;

import java.util.Properties;
import javax.annotation.PostConstruct;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.gitee.qdbp.base.annotation.OperateRecord;
import com.gitee.qdbp.base.utils.SessionTools;
import com.gitee.qdbp.general.system.api.permission.model.ResourceCascadeBean;
import com.gitee.qdbp.general.system.web.shiro.simple.ModuleOptions;
import com.gitee.qdbp.general.system.web.views.EasyuiViewsTools;
import com.gitee.qdbp.able.beans.DepthMap;
import com.gitee.qdbp.able.exception.ServiceException;

/**
 * 页面跳转控制器
 *
 * @author zhaohuihua
 * @version 150325
 */
@Controller
@RequestMapping()
@OperateRecord(enable = false)
public class ViewsController {

    /** 页面是否使用TAB页签 **/
    protected boolean tabs = true;
    protected EasyuiViewsTools viewTools;
    @Autowired
    protected Properties setting;

    @PostConstruct
    protected void init() {
        viewTools = new EasyuiViewsTools(setting);
    }

    /** 首页 **/
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView toDefault() throws ServiceException {
        return toIndex();
    }

    /** 首页 **/
    @RequestMapping(value = "index", method = RequestMethod.GET)
    public ModelAndView toIndex() throws ServiceException {
        // 要有这一行Subject.isPermitted("*")
        // 才会调用AuthorizingRealm.doGetAuthorizationInfo()
        // 才会加载用户权限资源
        SecurityUtils.getSubject().isPermitted("*");

        ResourceCascadeBean root = (ResourceCascadeBean) SessionTools.getUserResources();
        ResourceCascadeBean first = root.getFirst();
        if (first != null) {
            if (this.tabs || !(first.getOptions() instanceof ModuleOptions)) {
                return new ModelAndView("redirect:" + first.getUrl());
            } else { // 粮库项目要求直接打开模块的第一个菜单(而不是模块首页)
                ModuleOptions options = (ModuleOptions) first.getOptions();
                return new ModelAndView("redirect:" + options.getSublink());
            }
        } else {
            // throw new ServiceException(ResultCode.FORBIDDEN);
            return viewTools.toEasyuiModuleHome(tabs, null);
        }
    }

    /** 登录/注册/找回密码等页面 **/
    @RequestMapping(value = "auth/{page}/", method = RequestMethod.GET)
    public ModelAndView toAuthPage(@PathVariable("page") String page) throws ServiceException {
        String dt = "pc"; // viewTools.getDeviceType();
        String view = dt + "/auth/" + page;
        DepthMap dpm = viewTools.getPageVariables();
        return new ModelAndView(view, "pv", dpm.map());
    }

    /** 模块首页 **/
    @RequestMapping(value = "/{module}/", method = RequestMethod.GET)
    public ModelAndView toModuleHome(@PathVariable("module") String module) {
        return viewTools.toEasyuiModuleHome(tabs, module);
    }

    /** 模块子页 **/
    @RequestMapping(value = "/{module}/{channel}/{group}/{page}/", method = RequestMethod.GET)
    public ModelAndView toModulePage(@PathVariable("module") String module, @PathVariable("channel") String channel,
            @PathVariable("group") String group, @PathVariable("page") String page) {
        return viewTools.toEasyuiModulePage(tabs, module, channel, group, page);
    }

}
