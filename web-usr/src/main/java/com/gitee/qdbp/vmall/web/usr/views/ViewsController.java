package com.gitee.qdbp.vmall.web.usr.views;

import java.util.Properties;
import javax.annotation.PostConstruct;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.gitee.qdbp.base.annotation.OperateRecord;
import com.gitee.qdbp.general.system.web.views.SimpleViewsTools;
import com.gitee.qdbp.able.beans.DepthMap;
import com.gitee.qdbp.able.exception.ServiceException;

/**
 * 页面跳转控制器
 *
 * @author zhaohuihua
 * @version 150325
 */
@Controller
@RequestMapping()
@OperateRecord(enable = false)
public class ViewsController {

    protected SimpleViewsTools viewTools;
    @Autowired
    protected Properties setting;

    @PostConstruct
    protected void init() {
        viewTools = new SimpleViewsTools(setting);
    }

    /** 登录/注册/找回密码等页面 **/
    @RequestMapping(value = "auth/{page}/", method = RequestMethod.GET)
    public ModelAndView toAuthPage(@PathVariable("page") String page) throws ServiceException {
        String dt = viewTools.getDeviceType();
        String view = dt + "/auth/" + page;
        DepthMap dpm = viewTools.getPageVariables();
        return new ModelAndView(view, "pv", dpm.map());
    }

    /** PC页面 **/
    @RequestMapping(value = "/pc/{channel}/{group}/{page}/", method = RequestMethod.GET)
    public ModelAndView toPcPage(@PathVariable("channel") String channel, @PathVariable("group") String group,
            @PathVariable("page") String page) {
        SecurityUtils.getSubject().isPermitted("*");

        String view = "pc/" + channel + "/" + group + "/" + page;
        DepthMap dpm = viewTools.getPageVariables();
        return new ModelAndView(view, "pv", dpm.map());
    }

    /** H5页面 **/
    @RequestMapping(value = "/h5/{channel}/{group}/{page}/", method = RequestMethod.GET)
    public ModelAndView toH5Page(@PathVariable("channel") String channel, @PathVariable("group") String group,
            @PathVariable("page") String page) {
        SecurityUtils.getSubject().isPermitted("*");

        String view = "h5/" + channel + "/" + group + "/" + page;
        DepthMap dpm = viewTools.getPageVariables();
        return new ModelAndView(view, "pv", dpm.map());
    }

    /** 页面跳转 **/
    @RequestMapping(value = "/site/{group}/{page}/", method = RequestMethod.GET)
    public ModelAndView toSite(@PathVariable("group") String group, @PathVariable("page") String page) {
        SecurityUtils.getSubject().isPermitted("*");

        String dt = "h5"; // viewTools.getDeviceType();
        String view = dt + "/site/" + group + "/" + page;
        DepthMap dpm = viewTools.getPageVariables();
        return new ModelAndView(view, "pv", dpm.map());
    }

    //    /** 模块首页 **/
    //    @RequestMapping(value = "/{module}/", method = RequestMethod.GET)
    //    public ModelAndView toModuleHome(@PathVariable("module") String module) {
    //    }
    //
    //    /** 模块子页 **/
    //    @RequestMapping(value = "/{module}/{channel}/{group}/{page}/", method = RequestMethod.GET)
    //    public ModelAndView toModulePage(@PathVariable("module") String module, @PathVariable("channel") String channel,
    //            @PathVariable("group") String group, @PathVariable("page") String page) {
    //    }

}
