package com.gitee.qdbp.vmall.sales.biz.goods.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.general.common.enums.VersionState;
import com.gitee.qdbp.tools.utils.VerifyTools;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionBean;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionWhere;
import com.gitee.qdbp.vmall.sales.api.goods.main.service.IGoodsVersionQueryer;
import com.gitee.qdbp.vmall.sales.biz.goods.main.basic.GoodsVersionBasic;

/**
 * 商品版本信息业务处理类
 *
 * @author zhh
 * @version 170701
 */
@Service
@Transactional(readOnly = true)
public class GoodsVersionQueryer implements IGoodsVersionQueryer {

    /** 商品版本信息DAO **/
    @Autowired
    private GoodsVersionBasic goodsVersionBasic;

    @Override
    public GoodsVersionBean findByVid(String vid) throws ServiceException {
        return goodsVersionBasic.findByVid(vid);
    }

    @Override
    public GoodsVersionBean findByUid(String uid) throws ServiceException {
        GoodsVersionWhere where = new GoodsVersionWhere();
        where.setUid(uid);
        where.setVersionState(VersionState.ACTIVATED);
        return goodsVersionBasic.find(where);
    }

    @Override
    public GoodsVersionBean find(GoodsVersionWhere where) throws ServiceException {
        return goodsVersionBasic.find(where);
    }

    @Override
    public PageList<GoodsVersionBean> list(GoodsVersionWhere where, OrderPaging paging) throws ServiceException {
        PageList<GoodsVersionBean> paged = goodsVersionBasic.list(where, paging);
        if (VerifyTools.isNotBlank(paged)) {
            for (GoodsVersionBean bean : paged) {
                bean.setDescDetails(null); // 清空富文本内容
            }
        }
        return paged;
    }

}
