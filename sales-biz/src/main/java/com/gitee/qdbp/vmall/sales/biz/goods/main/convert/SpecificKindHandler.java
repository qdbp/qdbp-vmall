package com.gitee.qdbp.vmall.sales.biz.goods.main.convert;

import com.gitee.qdbp.base.orm.mybatis.converter.JsonBeanListHandler;
import com.gitee.qdbp.vmall.sales.api.specific.model.SpecificKind;

/**
 * 规格数据列表类型转换类
 *
 * @author zhh
 * @version 180627
 */
public class SpecificKindHandler extends JsonBeanListHandler<SpecificKind> {

    public SpecificKindHandler() {
        this.addIgnoreField("extra");
    }

    @Override
    public Class<SpecificKind> getBeanType() {
        return SpecificKind.class;
    }

}