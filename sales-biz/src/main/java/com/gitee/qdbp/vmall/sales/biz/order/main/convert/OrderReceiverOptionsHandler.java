package com.gitee.qdbp.vmall.sales.biz.order.main.convert;

import com.gitee.qdbp.base.orm.mybatis.converter.JsonBeanTypeHandler;
import com.gitee.qdbp.vmall.sales.api.order.main.model.OrderReceiverOptions;

/**
 * 选项类型转换类
 *
 * @author zhh
 * @version 180624
 */
public class OrderReceiverOptionsHandler extends JsonBeanTypeHandler<OrderReceiverOptions> {

    public OrderReceiverOptionsHandler() {
        this.addIgnoreField("extra");
    }

    @Override
    public Class<OrderReceiverOptions> getBeanType() {
        return OrderReceiverOptions.class;
    }

}