package com.gitee.qdbp.vmall.sales.biz.goods.manage.dao;

import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.Paging;
import com.gitee.qdbp.base.annotation.OperateTraces;
import com.gitee.qdbp.base.model.condition.OrderWhere;
import com.gitee.qdbp.base.model.condition.Where;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.InventoryRealtimeBean;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.InventoryRealtimeUpdate;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.InventoryRealtimeWhere;

/**
 * 实时库存DAO
 *
 * @author zhh
 * @version 170812
 */
@OperateTraces(enable = true)
public interface IInventoryRealtimeBasicDao {

    /**
     * 按条件查询单个对象,一般是根据ID或CODE查询
     *
     * @param where 查询条件
     * @return 实时库存
     */
    @OperateTraces(operate = "实时库存-查询")
    InventoryRealtimeBean find(Where<InventoryRealtimeWhere> where);

    /**
     * 按条件查询
     *
     * @param where 查询条件
     * @param paging 分页条件
     * @return 实时库存列表
     */
    @OperateTraces(operate = "实时库存-查询")
    List<InventoryRealtimeBean> list(OrderWhere<InventoryRealtimeWhere> where, Paging paging);

    /**
     * 创建实时库存记录
     *
     * @param model 实时库存信息
     * @return 影响行数
     */
    @OperateTraces(operate = "实时库存-创建")
    int insert(InventoryRealtimeBean model);

    /**
     * 批量创建实时库存记录
     *
     * @param model 实时库存信息
     * @return 影响行数
     */
    @OperateTraces(operate = "实时库存-批量创建")
    int inserts(List<InventoryRealtimeBean> models);

    /**
     * 按条件修改实时库存
     *
     * @param model 实时库存信息
     * @return 影响行数
     */
    @OperateTraces(operate = "实时库存-更新")
    int update(InventoryRealtimeUpdate model);

    /**
     * 按条件删除实时库存
     *
     * @param condition 条件
     * @return 影响行数
     */
    @OperateTraces(operate = "实时库存-删除")
    int delete(Where<InventoryRealtimeWhere> condition);
}
