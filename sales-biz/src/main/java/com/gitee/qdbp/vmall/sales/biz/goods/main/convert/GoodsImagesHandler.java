package com.gitee.qdbp.vmall.sales.biz.goods.main.convert;

import com.gitee.qdbp.base.orm.mybatis.converter.JsonBeanTypeHandler;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsImages;

/**
 * 图片信息类型转换类
 *
 * @author zhh
 * @version 180626
 */
public class GoodsImagesHandler extends JsonBeanTypeHandler<GoodsImages> {

    public GoodsImagesHandler() {
        this.addIgnoreField("extra");
    }

    @Override
    public Class<GoodsImages> getBeanType() {
        return GoodsImages.class;
    }

}