package com.gitee.qdbp.vmall.sales.biz.goods.main.dao;

import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.Paging;
import com.gitee.qdbp.base.annotation.OperateTraces;
import com.gitee.qdbp.base.model.condition.OrderWhere;
import com.gitee.qdbp.base.model.condition.Where;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsSkuRefBean;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsSkuRefUpdate;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsSkuRefWhere;

/**
 * 商品单品关联DAO
 *
 * @author zhh
 * @version 180627
 */
@OperateTraces(enable = true)
public interface IGoodsSkuRefBasicDao {

    /**
     * 按条件查询单个对象,一般是根据ID或CODE查询
     *
     * @param where 查询条件
     * @return 商品单品关联
     */
    @OperateTraces(operate = "商品单品关联-查询")
    GoodsSkuRefBean find(Where<GoodsSkuRefWhere> where);

    /**
     * 按条件查询
     *
     * @param where 查询条件
     * @param paging 分页条件
     * @return 商品单品关联列表
     */
    @OperateTraces(operate = "商品单品关联-查询")
    List<GoodsSkuRefBean> list(OrderWhere<GoodsSkuRefWhere> where, Paging paging);

    /**
     * 创建商品单品关联记录
     *
     * @param model 商品单品关联信息
     * @return 影响行数
     */
    @OperateTraces(operate = "商品单品关联-创建")
    int insert(GoodsSkuRefBean model);

    /**
     * 批量创建商品单品关联记录
     *
     * @param model 商品单品关联信息
     * @return 影响行数
     */
    @OperateTraces(operate = "商品单品关联-批量创建")
    int inserts(List<GoodsSkuRefBean> models);

    /**
     * 按条件修改商品单品关联
     *
     * @param model 商品单品关联信息
     * @return 影响行数
     */
    @OperateTraces(operate = "商品单品关联-更新")
    int update(GoodsSkuRefUpdate model);

    /**
     * 按条件删除商品单品关联
     *
     * @param condition 条件
     * @return 影响行数
     */
    @OperateTraces(operate = "商品单品关联-删除")
    int delete(Where<GoodsSkuRefWhere> condition);
}
