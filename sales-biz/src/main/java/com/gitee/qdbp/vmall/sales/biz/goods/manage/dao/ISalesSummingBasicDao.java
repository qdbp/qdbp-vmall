package com.gitee.qdbp.vmall.sales.biz.goods.manage.dao;

import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.Paging;
import com.gitee.qdbp.base.annotation.OperateTraces;
import com.gitee.qdbp.base.model.condition.OrderWhere;
import com.gitee.qdbp.base.model.condition.Where;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.SalesSummingBean;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.SalesSummingUpdate;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.SalesSummingWhere;

/**
 * 销量汇总DAO
 *
 * @author zhh
 * @version 180626
 */
@OperateTraces(enable = true)
public interface ISalesSummingBasicDao {

    /**
     * 按条件查询单个对象,一般是根据ID或CODE查询
     *
     * @param where 查询条件
     * @return 销量汇总
     */
    @OperateTraces(operate = "销量汇总-查询")
    SalesSummingBean find(Where<SalesSummingWhere> where);

    /**
     * 按条件查询
     *
     * @param where 查询条件
     * @param paging 分页条件
     * @return 销量汇总列表
     */
    @OperateTraces(operate = "销量汇总-查询")
    List<SalesSummingBean> list(OrderWhere<SalesSummingWhere> where, Paging paging);

    /**
     * 创建销量汇总记录
     *
     * @param model 销量汇总信息
     * @return 影响行数
     */
    @OperateTraces(operate = "销量汇总-创建")
    int insert(SalesSummingBean model);

    /**
     * 批量创建销量汇总记录
     *
     * @param model 销量汇总信息
     * @return 影响行数
     */
    @OperateTraces(operate = "销量汇总-批量创建")
    int inserts(List<SalesSummingBean> models);

    /**
     * 按条件修改销量汇总
     *
     * @param model 销量汇总信息
     * @return 影响行数
     */
    @OperateTraces(operate = "销量汇总-更新")
    int update(SalesSummingUpdate model);

    /**
     * 按条件删除销量汇总
     *
     * @param condition 条件
     * @return 影响行数
     */
    @OperateTraces(operate = "销量汇总-删除")
    int delete(Where<SalesSummingWhere> condition);
}
