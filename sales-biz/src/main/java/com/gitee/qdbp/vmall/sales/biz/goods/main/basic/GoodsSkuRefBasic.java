package com.gitee.qdbp.vmall.sales.biz.goods.main.basic;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.able.result.ResultCode;
import com.gitee.qdbp.base.model.condition.OrderWhere;
import com.gitee.qdbp.base.model.condition.Where;
import com.gitee.qdbp.general.common.api.sequence.service.ILocalSequenceGenerator;
import com.gitee.qdbp.tools.utils.JsonTools;
import com.gitee.qdbp.tools.utils.VerifyTools;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsSkuRefBean;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsSkuRefUpdate;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsSkuRefWhere;
import com.gitee.qdbp.vmall.sales.biz.goods.main.dao.IGoodsSkuRefBasicDao;

/**
 * 商品单品关联基础业务类
 *
 * @author zhh
 * @version 180627
 */
@Service
public class GoodsSkuRefBasic {

    /** 日志对象 **/
    private static final Logger log = LoggerFactory.getLogger(GoodsSkuRefBasic.class);

    /** 序列号生成(ID) **/
    @Autowired
    private ILocalSequenceGenerator sequenceGenerator;

    /** 商品单品关联DAO **/
    @Autowired
    private IGoodsSkuRefBasicDao goodsSkuRefBasicDao;

    /**
     * 商品单品关联查询
     *
     * @param where 查询条件
     * @return 商品单品关联
     * @throws ServiceException 查询失败
     */
    public GoodsSkuRefBean find(GoodsSkuRefWhere where) throws ServiceException {
        String msg = "Failed to query GoodsSkuRef. ";

        if (where == null) {
            log.error(msg + "params is null: where");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        doHandleDate(where);

        GoodsSkuRefBean bean;
        try {
            bean = goodsSkuRefBasicDao.find(Where.of(where));
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + "\n\t" + JsonTools.toJsonString(where), e);
            throw new ServiceException(ResultCode.DB_SELECT_ERROR, e);
        }

        if (bean == null) {
            log.trace("GoodsSkuRef not found. {}", JsonTools.toJsonString(where));
        }
        return bean;
    }

    /**
     * 根据主键查询商品单品关联
     *
     * @param id 主键
     * @return 商品单品关联
     * @throws ServiceException 查询失败
     */
    public GoodsSkuRefBean findById(String id) throws ServiceException {
        String msg = "Failed to query GoodsSkuRef by id. ";

        if (VerifyTools.isBlank(id)) {
            log.error(msg + "params is null: id");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        GoodsSkuRefWhere where = new GoodsSkuRefWhere();
        where.setId(id);

        return find(where);
    }

    /**
     * 商品单品关联查询
     *
     * @param paging 排序分页条件
     * @return 商品单品关联列表
     * @throws ServiceException 查询失败
     */
    public PageList<GoodsSkuRefBean> list(OrderPaging paging) throws ServiceException {
        String msg = "Failed to query GoodsSkuRef. ";

        if (paging == null) {
            log.error(msg + "params is null: paging");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }
        return list(new GoodsSkuRefWhere(), paging);
    }

    /**
     * 商品单品关联查询
     *
     * @param where 查询条件
     * @param paging 排序分页条件
     * @return 商品单品关联列表
     * @throws ServiceException 查询失败
     */
    public PageList<GoodsSkuRefBean> list(GoodsSkuRefWhere where, OrderPaging paging) throws ServiceException {
        String msg = "Failed to query GoodsSkuRef. ";

        if (paging == null) {
            log.error(msg + "params is null: paging");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        doHandleDate(where);

        try {
            OrderWhere<GoodsSkuRefWhere> ow = OrderWhere.of(where, paging.getOrderings());
            return PageList.of(goodsSkuRefBasicDao.list(ow, paging));
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + "\n\t" + JsonTools.toJsonString(where), e);
            throw new ServiceException(ResultCode.DB_SELECT_ERROR, e);
        }
    }

    /**
     * 根据主键查询商品单品关联列表
     *
     * @param ids 主键列表
     * @return 商品单品关联列表
     * @throws ServiceException 查询失败
     */
    public PageList<GoodsSkuRefBean> listByIds(List<String> ids) throws ServiceException {
        String msg = "Failed to query GoodsSkuRef by id list. ";

        if (VerifyTools.isBlank(ids)) {
            log.error(msg + "params is null: ids");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        GoodsSkuRefWhere where = new GoodsSkuRefWhere();
        where.setIds(ids);

        return list(where, OrderPaging.NONE);
    }

    /**
     * 创建商品单品关联
     *
     * @param model 待新增的商品单品关联
     * @return 受影响行数
     * @throws ServiceException 创建失败
     */
    public int create(GoodsSkuRefBean model) throws ServiceException {
        String msg = "Failed to create GoodsSkuRef. ";

        if (VerifyTools.isBlank(model)) {
            log.error(msg + "params is null: model");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        if (VerifyTools.isBlank(model.getId())) {
            model.setId(sequenceGenerator.generate(GoodsSkuRefBean.TABLE));
        }

        doHandleDate(model);

        // 向vml_goods_sku_ref表插入记录
        int rows;
        try {
            rows = goodsSkuRefBasicDao.insert(model);
        } catch (DuplicateKeyException e) {
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(model), e);
            throw new ServiceException(ResultCode.DB_DUPLICATE_KEY, e);
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(model), e);
            throw new ServiceException(ResultCode.DB_INSERT_ERROR, e);
        }

        if (rows == 0) {
            log.error(msg + "affected rows is 0. \n\t" + JsonTools.toJsonString(model));
            throw new ServiceException(ResultCode.DB_INSERT_ERROR);
        }
        return rows;
    }

    /**
     * 批量创建商品单品关联
     *
     * @param models 待新增的商品单品关联
     * @return 受影响行数
     * @throws ServiceException 创建失败
     */
    public int create(List<GoodsSkuRefBean> models) throws ServiceException {
        String msg = "Failed to batch create GoodsSkuRef. ";

        if (VerifyTools.isBlank(models)) {
            log.error(msg + "params is null: models");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        for (GoodsSkuRefBean model : models) {
            if (VerifyTools.isBlank(model.getId())) {
                model.setId(sequenceGenerator.generate(GoodsSkuRefBean.TABLE));
            }

            doHandleDate(model);
        }

        // 向vml_goods_sku_ref表插入记录
        int rows;
        try {
            rows = goodsSkuRefBasicDao.inserts(models);
        } catch (DuplicateKeyException e) {
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(models), e);
            throw new ServiceException(ResultCode.DB_DUPLICATE_KEY, e);
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(models), e);
            throw new ServiceException(ResultCode.DB_INSERT_ERROR, e);
        }

        int size = models.size();
        if (rows != size) {
            String desc = size > 0 ? "affected rows " + rows + "!=" + size + ". " : "affected rows is 0. ";
            log.error(msg + desc + "\n\t" + JsonTools.toJsonString(models));
            throw new ServiceException(ResultCode.DB_INSERT_ERROR);
        }
        return rows;
    }

    /**
     * 修改商品单品关联
     *
     * @param model 待修改的内容
     * @param errorOnUnaffected 受影响行数为0时是否抛异常
     * @return 受影响行数
     * @throws ServiceException 修改失败
     */
    public int update(GoodsSkuRefUpdate model, boolean errorOnUnaffected) throws ServiceException {
        String msg = "Failed to update GoodsSkuRef. ";

        if (model == null) {
            log.error(msg + "params is null: model");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        if (VerifyTools.isNotBlank(model.getId())) {
            if (model.getWhere() == null || VerifyTools.isBlank(model.getWhere().getId())) {
                model.getWhere(true).setId(model.getId());
            }
        }
        return doUpdate(model, 0, errorOnUnaffected);
    }

    /**
     * 根据主键删除商品单品关联
     *
     * @param ids 待删除的商品单品关联的主键
     * @param errorOnRowsNotMatch 删除行数不匹配时是否抛异常
     * @return 删除行数
     * @throws ServiceException 删除失败
     */
    public int deleteByIds(List<String> ids, boolean errorOnRowsNotMatch) throws ServiceException {
        String msg = "Failed to delete GoodsSkuRef. ";

        if (VerifyTools.isBlank(ids)) {
            log.error(msg + "params is null: ids");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        GoodsSkuRefWhere where = new GoodsSkuRefWhere();
        where.setIds(ids);

        return doDelete(where, ids.size(), errorOnRowsNotMatch);
    }

    /**
     * 根据条件删除商品单品关联
     *
     * @param where 条件
     * @param errorOnUnaffected 删除行数为0时是否抛异常
     * @return 删除行数
     * @throws ServiceException 删除失败
     */
    public int delete(GoodsSkuRefWhere where, boolean errorOnUnaffected) throws ServiceException {
        String msg = "Failed to delete GoodsSkuRef. ";

        if (where == null) {
            log.error(msg + "params is null: where");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }
        return doDelete(where, 0, errorOnUnaffected);
    }

    /**
     * 根据条件删除商品单品关联
     *
     * @param where 条件
     * @param size 应删除条数, 0表示未知
     * @param errorOnRowsNotMatch 删除行数不匹配时是否抛异常
     * @param isId 是不是唯一主键
     * @return 删除行数
     * @throws ServiceException 删除失败
     */
    private int doDelete(GoodsSkuRefWhere where, int size, boolean errorOnRowsNotMatch) throws ServiceException {
        String msg = "Failed to delete GoodsSkuRef. ";

        doHandleDate(where);

        int rows;
        try {
            rows = goodsSkuRefBasicDao.delete(Where.of(where));
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + JsonTools.toJsonString(where), e);
            throw new ServiceException(ResultCode.DB_DELETE_ERROR, e);
        }

        if (size > 0 && rows != size || rows == 0) {
            String desc = size > 0 ? "affected rows != " + size + ". " : "affected rows is 0. ";
            if (errorOnRowsNotMatch) {
                log.error(msg + desc + "\n\t" + JsonTools.toJsonString(where));
                throw new ServiceException(ResultCode.DB_DELETE_ERROR);
            } else {
                log.debug(msg + desc + "\n\t" + JsonTools.toJsonString(where));
            }
        }
        return rows;
    }

    /**
     * 修改商品单品关联
     *
     * @param model 待修改的内容
     * @param size 应删除条数, 0表示未知
     * @param errorOnRowsNotMatch 删除行数不匹配时是否抛异常
     * @return 受影响行数
     * @throws ServiceException 修改失败
     */
    private int doUpdate(GoodsSkuRefUpdate model, int size, boolean errorOnRowsNotMatch) throws ServiceException {
        String msg = "Failed to update GoodsSkuRef. ";

        if (model == null) {
            log.error(msg + "params is null: model");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        if (model.getWhere() == null) {
            log.error(msg + "params is null: model.getWhere()");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        doHandleDate(model);
        doHandleDate(model.getWhere());

        int rows;
        try {
            rows = goodsSkuRefBasicDao.update(model);
        } catch (DuplicateKeyException e) {
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(model), e);
            throw new ServiceException(ResultCode.DB_DUPLICATE_KEY, e);
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(model), e);
            throw new ServiceException(ResultCode.DB_UPDATE_ERROR, e);
        }

        if (size > 0 && rows != size || rows == 0) {
            String desc = size > 0 ? "affected rows != " + size + ". " : "affected rows is 0. ";
            if (errorOnRowsNotMatch) {
                log.error(msg + desc + "\n\t" + JsonTools.toJsonString(model));
                throw new ServiceException(ResultCode.DB_UPDATE_ERROR);
            } else {
                log.debug(msg + desc + "\n\t" + JsonTools.toJsonString(model));
            }
        }
        return rows;
    }

    // 由于数据库不支持毫秒, 超过500毫秒会因四舍五入导致变成下一天0点, 因此将毫秒清掉
    protected static void doHandleDate(GoodsSkuRefBean model) {
        if (model == null) {
            return;
        }

    }

    // 由于数据库不支持毫秒, 超过500毫秒会因四舍五入导致变成下一天0点, 因此将毫秒清掉
    // 处理XxxTime, XxxTimeMin和XxxTimeMax, XxxTimeMinWithDay和XxxTimeMaxWithDay
    protected static void doHandleDate(GoodsSkuRefWhere model) {
        if (model == null) {
            return;
        }


    }
}