package com.gitee.qdbp.vmall.sales.biz.goods.main.dao;

import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.Paging;
import com.gitee.qdbp.base.annotation.OperateTraces;
import com.gitee.qdbp.base.model.condition.OrderWhere;
import com.gitee.qdbp.base.model.condition.Where;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionBean;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionUpdate;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionWhere;

/**
 * 商品版本信息DAO
 *
 * @author zhh
 * @version 170706
 */
@OperateTraces(enable = true)
public interface IGoodsVersionBasicDao {

    /**
     * 按条件查询单个对象,一般是根据ID或CODE查询
     *
     * @param where 查询条件
     * @return 商品版本信息
     */
    @OperateTraces(operate = "商品版本信息-查询")
    GoodsVersionBean find(Where<GoodsVersionWhere> where);

    /**
     * 按条件查询
     *
     * @param where 查询条件
     * @param paging 分页条件
     * @return 商品版本信息列表
     */
    @OperateTraces(operate = "商品版本信息-查询")
    List<GoodsVersionBean> list(OrderWhere<GoodsVersionWhere> where, Paging paging);

    /**
     * 创建商品版本信息记录
     *
     * @param model 商品版本信息信息
     * @return 影响行数
     */
    @OperateTraces(operate = "商品版本信息-创建")
    int insert(GoodsVersionBean model);

    /**
     * 批量创建商品版本信息记录
     *
     * @param model 商品版本信息信息
     * @return 影响行数
     */
    @OperateTraces(operate = "商品版本信息-批量创建")
    int inserts(List<GoodsVersionBean> models);

    /**
     * 按条件修改商品版本信息
     *
     * @param model 商品版本信息信息
     * @return 影响行数
     */
    @OperateTraces(operate = "商品版本信息-更新")
    int update(GoodsVersionUpdate model);

    /**
     * 按条件删除商品版本信息
     *
     * @param condition 条件
     * @return 影响行数
     */
    @OperateTraces(operate = "商品版本信息-删除")
    int delete(Where<GoodsVersionWhere> condition);
}
