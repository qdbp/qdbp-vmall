package com.gitee.qdbp.vmall.sales.biz.goods.manage.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.SalesSummingBean;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.SalesSummingWhere;
import com.gitee.qdbp.vmall.sales.api.goods.manage.service.ISalesSummingQueryer;
import com.gitee.qdbp.vmall.sales.biz.goods.manage.basic.SalesSummingBasic;

/**
 * 销量汇总业务处理类
 *
 * @author zhh
 * @version 170812
 */
@Service
@Transactional(readOnly = true)
public class SalesSummingQueryer implements ISalesSummingQueryer {

    /** 销量汇总DAO **/
    @Autowired
    private SalesSummingBasic salesSummingBasic;

    @Override
    public SalesSummingBean find(String id) throws ServiceException {
        return salesSummingBasic.findById(id);
    }

    @Override
    public SalesSummingBean find(SalesSummingWhere where) throws ServiceException {
        return salesSummingBasic.find(where);
    }

    @Override
    public PageList<SalesSummingBean> list(SalesSummingWhere where, OrderPaging paging) throws ServiceException {
        return salesSummingBasic.list(where, paging);
    }

}