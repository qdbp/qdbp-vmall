package com.gitee.qdbp.vmall.sales.biz.order.manage.dao;

import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.Paging;
import com.gitee.qdbp.base.annotation.OperateTraces;
import com.gitee.qdbp.base.model.condition.OrderWhere;
import com.gitee.qdbp.base.model.condition.Where;
import com.gitee.qdbp.vmall.sales.api.order.manage.model.OrderDeliveryBean;
import com.gitee.qdbp.vmall.sales.api.order.manage.model.OrderDeliveryUpdate;
import com.gitee.qdbp.vmall.sales.api.order.manage.model.OrderDeliveryWhere;

/**
 * 商品订单发货单DAO
 *
 * @author zhh
 * @version 180624
 */
@OperateTraces(enable = true)
public interface IOrderDeliveryBasicDao {

    /**
     * 按条件查询单个对象,一般是根据ID或CODE查询
     *
     * @param where 查询条件
     * @return 商品订单发货单
     */
    @OperateTraces(operate = "商品订单发货单-查询")
    OrderDeliveryBean find(Where<OrderDeliveryWhere> where);

    /**
     * 按条件查询
     *
     * @param where 查询条件
     * @param paging 分页条件
     * @return 商品订单发货单列表
     */
    @OperateTraces(operate = "商品订单发货单-查询")
    List<OrderDeliveryBean> list(OrderWhere<OrderDeliveryWhere> where, Paging paging);

    /**
     * 创建商品订单发货单记录
     *
     * @param model 商品订单发货单信息
     * @return 影响行数
     */
    @OperateTraces(operate = "商品订单发货单-创建")
    int insert(OrderDeliveryBean model);

    /**
     * 批量创建商品订单发货单记录
     *
     * @param model 商品订单发货单信息
     * @return 影响行数
     */
    @OperateTraces(operate = "商品订单发货单-批量创建")
    int inserts(List<OrderDeliveryBean> models);

    /**
     * 按条件修改商品订单发货单
     *
     * @param model 商品订单发货单信息
     * @return 影响行数
     */
    @OperateTraces(operate = "商品订单发货单-更新")
    int update(OrderDeliveryUpdate model);

    /**
     * 按条件删除商品订单发货单
     *
     * @param condition 条件
     * @return 影响行数
     */
    @OperateTraces(operate = "商品订单发货单-删除")
    int delete(Where<OrderDeliveryWhere> condition);
}
