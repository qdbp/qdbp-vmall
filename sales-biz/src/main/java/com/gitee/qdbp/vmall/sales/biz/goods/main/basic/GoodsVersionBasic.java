package com.gitee.qdbp.vmall.sales.biz.goods.main.basic;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.able.result.ResultCode;
import com.gitee.qdbp.base.enums.DataState;
import com.gitee.qdbp.base.model.condition.OrderWhere;
import com.gitee.qdbp.base.model.condition.Where;
import com.gitee.qdbp.general.common.api.sequence.service.ILocalSequenceGenerator;
import com.gitee.qdbp.tools.utils.DateTools;
import com.gitee.qdbp.tools.utils.JsonTools;
import com.gitee.qdbp.tools.utils.VerifyTools;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionBean;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionUpdate;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionWhere;
import com.gitee.qdbp.vmall.sales.biz.goods.main.dao.IGoodsVersionBasicDao;

/**
 * 商品版本信息基础业务类
 *
 * @author zhh
 * @version 180626
 */
@Service
public class GoodsVersionBasic {

    /** 日志对象 **/
    private static final Logger log = LoggerFactory.getLogger(GoodsVersionBasic.class);

    /** 序列号生成(ID) **/
    @Autowired
    private ILocalSequenceGenerator sequenceGenerator;

    /** 商品版本信息DAO **/
    @Autowired
    private IGoodsVersionBasicDao goodsVersionBasicDao;

    /**
     * 商品版本信息查询
     *
     * @param where 查询条件
     * @return 商品版本信息
     * @throws ServiceException 查询失败
     */
    public GoodsVersionBean find(GoodsVersionWhere where) throws ServiceException {
        String msg = "Failed to query GoodsVersion. ";

        if (where == null) {
            log.error(msg + "params is null: where");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        if (where.getDataState() == null) {
            where.setDataState(DataState.NORMAL);
        }

        doHandleDate(where);

        GoodsVersionBean bean;
        try {
            bean = goodsVersionBasicDao.find(Where.of(where));
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + "\n\t" + JsonTools.toJsonString(where), e);
            throw new ServiceException(ResultCode.DB_SELECT_ERROR, e);
        }

        if (bean == null) {
            log.trace("GoodsVersion not found. {}", JsonTools.toJsonString(where));
        }
        return bean;
    }

    /**
     * 根据商品版本ID查询商品版本信息
     *
     * @param vid 商品版本ID
     * @return 商品版本信息
     * @throws ServiceException 查询失败
     */
    public GoodsVersionBean findByVid(String vid) throws ServiceException {
        String msg = "Failed to query GoodsVersion by id. ";

        if (VerifyTools.isBlank(vid)) {
            log.error(msg + "params is null: vid");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        GoodsVersionWhere where = new GoodsVersionWhere();
        where.setVid(vid);

        return find(where);
    }

    /**
     * 商品版本信息查询
     *
     * @param paging 排序分页条件
     * @return 商品版本信息列表
     * @throws ServiceException 查询失败
     */
    public PageList<GoodsVersionBean> list(OrderPaging paging) throws ServiceException {
        String msg = "Failed to query GoodsVersion. ";

        if (paging == null) {
            log.error(msg + "params is null: paging");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }
        return list(new GoodsVersionWhere(), paging);
    }

    /**
     * 商品版本信息查询
     *
     * @param where 查询条件
     * @param paging 排序分页条件
     * @return 商品版本信息列表
     * @throws ServiceException 查询失败
     */
    public PageList<GoodsVersionBean> list(GoodsVersionWhere where, OrderPaging paging) throws ServiceException {
        String msg = "Failed to query GoodsVersion. ";

        if (paging == null) {
            log.error(msg + "params is null: paging");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        if (where == null) {
            where = new GoodsVersionWhere();
            where.setDataState(DataState.NORMAL);
        } else if (where.getDataState() == null) {
            where.setDataState(DataState.NORMAL);
        }

        doHandleDate(where);

        try {
            OrderWhere<GoodsVersionWhere> ow = OrderWhere.of(where, paging.getOrderings());
            return PageList.of(goodsVersionBasicDao.list(ow, paging));
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + "\n\t" + JsonTools.toJsonString(where), e);
            throw new ServiceException(ResultCode.DB_SELECT_ERROR, e);
        }
    }

    /**
     * 根据商品版本ID查询商品版本信息列表
     *
     * @param vids 商品版本ID列表
     * @return 商品版本信息列表
     * @throws ServiceException 查询失败
     */
    public PageList<GoodsVersionBean> listByVids(List<String> vids) throws ServiceException {
        String msg = "Failed to query GoodsVersion by vid list. ";

        if (VerifyTools.isBlank(vids)) {
            log.error(msg + "params is null: vids");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        GoodsVersionWhere where = new GoodsVersionWhere();
        where.setVids(vids);

        return list(where, OrderPaging.NONE);
    }

    /**
     * 创建商品版本信息
     *
     * @param model 待新增的商品版本信息
     * @return 受影响行数
     * @throws ServiceException 创建失败
     */
    public int create(GoodsVersionBean model) throws ServiceException {
        String msg = "Failed to create GoodsVersion. ";

        if (VerifyTools.isBlank(model)) {
            log.error(msg + "params is null: model");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        if (VerifyTools.isBlank(model.getVid())) {
            model.setVid(sequenceGenerator.generate(GoodsVersionBean.TABLE));
        }
        model.setDataState(DataState.NORMAL);

        doHandleDate(model);

        // 向vml_goods_version表插入记录
        int rows;
        try {
            rows = goodsVersionBasicDao.insert(model);
        } catch (DuplicateKeyException e) {
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(model), e);
            throw new ServiceException(ResultCode.DB_DUPLICATE_KEY, e);
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(model), e);
            throw new ServiceException(ResultCode.DB_INSERT_ERROR, e);
        }

        if (rows == 0) {
            log.error(msg + "affected rows is 0. \n\t" + JsonTools.toJsonString(model));
            throw new ServiceException(ResultCode.DB_INSERT_ERROR);
        }
        return rows;
    }

    /**
     * 批量创建商品版本信息
     *
     * @param models 待新增的商品版本信息
     * @return 受影响行数
     * @throws ServiceException 创建失败
     */
    public int create(List<GoodsVersionBean> models) throws ServiceException {
        String msg = "Failed to batch create GoodsVersion. ";

        if (VerifyTools.isBlank(models)) {
            log.error(msg + "params is null: models");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        for (GoodsVersionBean model : models) {
            if (VerifyTools.isBlank(model.getVid())) {
                model.setVid(sequenceGenerator.generate(GoodsVersionBean.TABLE));
            }
            model.setDataState(DataState.NORMAL);

            doHandleDate(model);
        }

        // 向vml_goods_version表插入记录
        int rows;
        try {
            rows = goodsVersionBasicDao.inserts(models);
        } catch (DuplicateKeyException e) {
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(models), e);
            throw new ServiceException(ResultCode.DB_DUPLICATE_KEY, e);
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(models), e);
            throw new ServiceException(ResultCode.DB_INSERT_ERROR, e);
        }

        int size = models.size();
        if (rows != size) {
            String desc = size > 0 ? "affected rows " + rows + "!=" + size + ". " : "affected rows is 0. ";
            log.error(msg + desc + "\n\t" + JsonTools.toJsonString(models));
            throw new ServiceException(ResultCode.DB_INSERT_ERROR);
        }
        return rows;
    }

    /**
     * 修改商品版本信息
     *
     * @param model 待修改的内容
     * @param errorOnUnaffected 受影响行数为0时是否抛异常
     * @return 受影响行数
     * @throws ServiceException 修改失败
     */
    public int update(GoodsVersionUpdate model, boolean errorOnUnaffected) throws ServiceException {
        String msg = "Failed to update GoodsVersion. ";

        if (model == null) {
            log.error(msg + "params is null: model");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        GoodsVersionWhere where = model.getWhere(true);
        if (VerifyTools.isBlank(where.getVid()) && VerifyTools.isNotBlank(model.getVid())) {
            where.setVid(model.getVid());
        }
        model.setDataState(null); // 数据状态只允许通过删除操作来修改
        if (where.getDataState() == null) {
            where.setDataState(DataState.NORMAL);
        }
        return doUpdate(model, 0, errorOnUnaffected);
    }

    /**
     * 根据商品版本ID删除商品版本信息
     *
     * @param vids 待删除的商品版本信息的商品版本ID
     * @param errorOnRowsNotMatch 删除行数不匹配时是否抛异常
     * @return 删除行数
     * @throws ServiceException 删除失败
     */
    public int deleteByVids(List<String> vids, boolean errorOnRowsNotMatch) throws ServiceException {
        String msg = "Failed to delete GoodsVersion. ";

        if (VerifyTools.isBlank(vids)) {
            log.error(msg + "params is null: vids");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        GoodsVersionWhere where = new GoodsVersionWhere();
        where.setVids(vids);

        return doDelete(where, vids.size(), errorOnRowsNotMatch);
    }

    /**
     * 根据条件删除商品版本信息
     *
     * @param where 条件
     * @param errorOnUnaffected 删除行数为0时是否抛异常
     * @return 删除行数
     * @throws ServiceException 删除失败
     */
    public int delete(GoodsVersionWhere where, boolean errorOnUnaffected) throws ServiceException {
        String msg = "Failed to delete GoodsVersion. ";

        if (where == null) {
            log.error(msg + "params is null: where");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }
        return doDelete(where, 0, errorOnUnaffected);
    }

    /**
     * 根据条件删除商品版本信息
     *
     * @param where 条件
     * @param size 应删除条数, 0表示未知
     * @param errorOnRowsNotMatch 删除行数不匹配时是否抛异常
     * @param isId 是不是唯一主键
     * @return 删除行数
     * @throws ServiceException 删除失败
     */
    private int doDelete(GoodsVersionWhere where, int size, boolean errorOnRowsNotMatch) throws ServiceException {
        // 逻辑删除
        if (where.getDataState() == null) {
            where.setDataState(DataState.NORMAL);
        }
        GoodsVersionUpdate model = new GoodsVersionUpdate();
        model.setDataState(DataState.DELETED);
        model.setWhere(where);
        return doUpdate(model, size, errorOnRowsNotMatch);
    }

    /**
     * 修改商品版本信息
     *
     * @param model 待修改的内容
     * @param size 应删除条数, 0表示未知
     * @param errorOnRowsNotMatch 删除行数不匹配时是否抛异常
     * @return 受影响行数
     * @throws ServiceException 修改失败
     */
    private int doUpdate(GoodsVersionUpdate model, int size, boolean errorOnRowsNotMatch) throws ServiceException {
        String msg = "Failed to update GoodsVersion. ";

        if (model == null) {
            log.error(msg + "params is null: model");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        if (model.getWhere() == null) {
            log.error(msg + "params is null: model.getWhere()");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        doHandleDate(model);
        doHandleDate(model.getWhere());

        int rows;
        try {
            rows = goodsVersionBasicDao.update(model);
        } catch (DuplicateKeyException e) {
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(model), e);
            throw new ServiceException(ResultCode.DB_DUPLICATE_KEY, e);
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(model), e);
            throw new ServiceException(ResultCode.DB_UPDATE_ERROR, e);
        }

        if (size > 0 && rows != size || rows == 0) {
            String desc = size > 0 ? "affected rows != " + size + ". " : "affected rows is 0. ";
            if (errorOnRowsNotMatch) {
                log.error(msg + desc + "\n\t" + JsonTools.toJsonString(model));
                throw new ServiceException(ResultCode.DB_UPDATE_ERROR);
            } else {
                log.debug(msg + desc + "\n\t" + JsonTools.toJsonString(model));
            }
        }
        return rows;
    }

    // 由于数据库不支持毫秒, 超过500毫秒会因四舍五入导致变成下一天0点, 因此将毫秒清掉
    protected static void doHandleDate(GoodsVersionBean model) {
        if (model == null) {
            return;
        }

        if (VerifyTools.isNotBlank(model.getCreateTime())) { // 创建时间
            model.setCreateTime(DateTools.setMillisecond(model.getCreateTime(), 0));
        }
        if (VerifyTools.isNotBlank(model.getPublishTime())) { // 发布时间
            model.setPublishTime(DateTools.setMillisecond(model.getPublishTime(), 0));
        }
        if (VerifyTools.isNotBlank(model.getExpireTime())) { // 过期时间
            model.setExpireTime(DateTools.setMillisecond(model.getExpireTime(), 0));
        }
    }

    // 由于数据库不支持毫秒, 超过500毫秒会因四舍五入导致变成下一天0点, 因此将毫秒清掉
    // 处理XxxTime, XxxTimeMin和XxxTimeMax, XxxTimeMinWithDay和XxxTimeMaxWithDay
    protected static void doHandleDate(GoodsVersionWhere model) {
        if (model == null) {
            return;
        }

        if (VerifyTools.isNotBlank(model.getCreateTime())) { // 创建时间
            model.setCreateTime(DateTools.setMillisecond(model.getCreateTime(), 0));
        }
        if (VerifyTools.isNotBlank(model.getPublishTime())) { // 发布时间
            model.setPublishTime(DateTools.setMillisecond(model.getPublishTime(), 0));
        }
        if (VerifyTools.isNotBlank(model.getExpireTime())) { // 过期时间
            model.setExpireTime(DateTools.setMillisecond(model.getExpireTime(), 0));
        }

        if (VerifyTools.isNotBlank(model.getCreateTimeMin())) { // 最小创建时间
            model.setCreateTimeMin(DateTools.setMillisecond(model.getCreateTimeMin(), 0));
        }
        if (VerifyTools.isNotBlank(model.getCreateTimeMinWithDay())) { // 最小创建时间
            model.setCreateTimeMinWithDay(DateTools.toStartTime(model.getCreateTimeMinWithDay()));
        }
        if (VerifyTools.isNotBlank(model.getCreateTimeMax())) { // 最大创建时间
            model.setCreateTimeMax(DateTools.setMillisecond(model.getCreateTimeMax(), 0));
        }
        if (VerifyTools.isNotBlank(model.getCreateTimeMaxWithDay())) { // 最大创建时间
            model.setCreateTimeMaxWithDay(DateTools.setMillisecond(DateTools.toEndTime(model.getCreateTimeMaxWithDay()), 0));
        }
        if (VerifyTools.isNotBlank(model.getPublishTimeMin())) { // 最小发布时间
            model.setPublishTimeMin(DateTools.setMillisecond(model.getPublishTimeMin(), 0));
        }
        if (VerifyTools.isNotBlank(model.getPublishTimeMinWithDay())) { // 最小发布时间
            model.setPublishTimeMinWithDay(DateTools.toStartTime(model.getPublishTimeMinWithDay()));
        }
        if (VerifyTools.isNotBlank(model.getPublishTimeMax())) { // 最大发布时间
            model.setPublishTimeMax(DateTools.setMillisecond(model.getPublishTimeMax(), 0));
        }
        if (VerifyTools.isNotBlank(model.getPublishTimeMaxWithDay())) { // 最大发布时间
            model.setPublishTimeMaxWithDay(DateTools.setMillisecond(DateTools.toEndTime(model.getPublishTimeMaxWithDay()), 0));
        }
        if (VerifyTools.isNotBlank(model.getExpireTimeMin())) { // 最小过期时间
            model.setExpireTimeMin(DateTools.setMillisecond(model.getExpireTimeMin(), 0));
        }
        if (VerifyTools.isNotBlank(model.getExpireTimeMinWithDay())) { // 最小过期时间
            model.setExpireTimeMinWithDay(DateTools.toStartTime(model.getExpireTimeMinWithDay()));
        }
        if (VerifyTools.isNotBlank(model.getExpireTimeMax())) { // 最大过期时间
            model.setExpireTimeMax(DateTools.setMillisecond(model.getExpireTimeMax(), 0));
        }
        if (VerifyTools.isNotBlank(model.getExpireTimeMaxWithDay())) { // 最大过期时间
            model.setExpireTimeMaxWithDay(DateTools.setMillisecond(DateTools.toEndTime(model.getExpireTimeMaxWithDay()), 0));
        }
    }
}