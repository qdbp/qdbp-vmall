package com.gitee.qdbp.vmall.sales.biz.goods.manage.model;

import java.io.Serializable;

/**
 * 单品数量
 *
 * @author zhaohuihua
 * @version 170812
 */
public class SkuQuantityBean implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 租户编号 **/
    private String tenantCode;

    /** 单品ID **/
    private String skuId;

    /** 数量 **/
    private Integer quantity;

    /** 获取租户编号 **/
    public String getTenantCode() {
        return tenantCode;
    }

    /** 设置租户编号 **/
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /** 获取单品ID **/
    public String getSkuId() {
        return skuId;
    }

    /** 设置单品ID **/
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    /** 获取数量 **/
    public Integer getQuantity() {
        return quantity;
    }

    /** 设置数量 **/
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
