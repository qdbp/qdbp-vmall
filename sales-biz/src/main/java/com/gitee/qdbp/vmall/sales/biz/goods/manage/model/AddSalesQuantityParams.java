package com.gitee.qdbp.vmall.sales.biz.goods.manage.model;

import java.util.Date;

/**
 * 增加销量的参数
 *
 * @author zhaohuihua
 * @version 170812
 */
public class AddSalesQuantityParams extends SkuQuantityBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 销售时间 **/
    private Date salesTime;

    /** 虚拟销量, 只增加总销量 **/
    private boolean fictitious = false;

    /** 销售时间 **/
    public Date getSalesTime() {
        return salesTime;
    }

    /** 销售时间 **/
    public void setSalesTime(Date salesTime) {
        this.salesTime = salesTime;
    }

    /** 虚拟销量, 只增加总销量 **/
    public boolean isFictitious() {
        return fictitious;
    }

    /** 虚拟销量, 只增加总销量 **/
    public void setFictitious(boolean fictitious) {
        this.fictitious = fictitious;
    }

}
