package com.gitee.qdbp.vmall.sales.biz.goods.main.convert;

import com.gitee.qdbp.base.orm.mybatis.converter.JsonBeanTypeHandler;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsOptions;

/**
 * 选项类型转换类
 *
 * @author zhh
 * @version 180626
 */
public class GoodsOptionsHandler extends JsonBeanTypeHandler<GoodsOptions> {

    public GoodsOptionsHandler() {
        this.addIgnoreField("extra");
    }

    @Override
    public Class<GoodsOptions> getBeanType() {
        return GoodsOptions.class;
    }

}