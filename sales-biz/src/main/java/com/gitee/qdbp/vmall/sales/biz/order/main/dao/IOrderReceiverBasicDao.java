package com.gitee.qdbp.vmall.sales.biz.order.main.dao;

import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.Paging;
import com.gitee.qdbp.base.annotation.OperateTraces;
import com.gitee.qdbp.base.model.condition.OrderWhere;
import com.gitee.qdbp.base.model.condition.Where;
import com.gitee.qdbp.vmall.sales.api.order.main.model.OrderReceiverBean;
import com.gitee.qdbp.vmall.sales.api.order.main.model.OrderReceiverUpdate;
import com.gitee.qdbp.vmall.sales.api.order.main.model.OrderReceiverWhere;

/**
 * 订单配送信息DAO
 *
 * @author zhh
 * @version 180624
 */
@OperateTraces(enable = true)
public interface IOrderReceiverBasicDao {

    /**
     * 按条件查询单个对象,一般是根据ID或CODE查询
     *
     * @param where 查询条件
     * @return 订单配送信息
     */
    @OperateTraces(operate = "订单配送信息-查询")
    OrderReceiverBean find(Where<OrderReceiverWhere> where);

    /**
     * 按条件查询
     *
     * @param where 查询条件
     * @param paging 分页条件
     * @return 订单配送信息列表
     */
    @OperateTraces(operate = "订单配送信息-查询")
    List<OrderReceiverBean> list(OrderWhere<OrderReceiverWhere> where, Paging paging);

    /**
     * 创建订单配送信息记录
     *
     * @param model 订单配送信息信息
     * @return 影响行数
     */
    @OperateTraces(operate = "订单配送信息-创建")
    int insert(OrderReceiverBean model);

    /**
     * 批量创建订单配送信息记录
     *
     * @param model 订单配送信息信息
     * @return 影响行数
     */
    @OperateTraces(operate = "订单配送信息-批量创建")
    int inserts(List<OrderReceiverBean> models);

    /**
     * 按条件修改订单配送信息
     *
     * @param model 订单配送信息信息
     * @return 影响行数
     */
    @OperateTraces(operate = "订单配送信息-更新")
    int update(OrderReceiverUpdate model);

    /**
     * 按条件删除订单配送信息
     *
     * @param condition 条件
     * @return 影响行数
     */
    @OperateTraces(operate = "订单配送信息-删除")
    int delete(Where<OrderReceiverWhere> condition);
}
