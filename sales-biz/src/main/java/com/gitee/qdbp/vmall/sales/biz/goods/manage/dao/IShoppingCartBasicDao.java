package com.gitee.qdbp.vmall.sales.biz.goods.manage.dao;

import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.Paging;
import com.gitee.qdbp.base.annotation.OperateTraces;
import com.gitee.qdbp.base.model.condition.OrderWhere;
import com.gitee.qdbp.base.model.condition.Where;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.ShoppingCartBean;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.ShoppingCartUpdate;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.ShoppingCartWhere;

/**
 * 用户购物车DAO
 *
 * @author zhh
 * @version 180703
 */
@OperateTraces(enable = true)
public interface IShoppingCartBasicDao {

    /**
     * 按条件查询单个对象,一般是根据ID或CODE查询
     *
     * @param where 查询条件
     * @return 用户购物车
     */
    @OperateTraces(operate = "用户购物车-查询")
    ShoppingCartBean find(Where<ShoppingCartWhere> where);

    /**
     * 按条件查询
     *
     * @param where 查询条件
     * @param paging 分页条件
     * @return 用户购物车列表
     */
    @OperateTraces(operate = "用户购物车-查询")
    List<ShoppingCartBean> list(OrderWhere<ShoppingCartWhere> where, Paging paging);

    /**
     * 创建用户购物车记录
     *
     * @param model 用户购物车信息
     * @return 影响行数
     */
    @OperateTraces(operate = "用户购物车-创建")
    int insert(ShoppingCartBean model);

    /**
     * 批量创建用户购物车记录
     *
     * @param model 用户购物车信息
     * @return 影响行数
     */
    @OperateTraces(operate = "用户购物车-批量创建")
    int inserts(List<ShoppingCartBean> models);

    /**
     * 按条件修改用户购物车
     *
     * @param model 用户购物车信息
     * @return 影响行数
     */
    @OperateTraces(operate = "用户购物车-更新")
    int update(ShoppingCartUpdate model);

    /**
     * 按条件删除用户购物车
     *
     * @param condition 条件
     * @return 影响行数
     */
    @OperateTraces(operate = "用户购物车-删除")
    int delete(Where<ShoppingCartWhere> condition);
}
