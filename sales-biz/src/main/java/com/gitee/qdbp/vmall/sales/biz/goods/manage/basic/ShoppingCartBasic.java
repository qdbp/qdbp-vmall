package com.gitee.qdbp.vmall.sales.biz.goods.manage.basic;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.able.result.ResultCode;
import com.gitee.qdbp.base.enums.DataState;
import com.gitee.qdbp.base.model.condition.OrderWhere;
import com.gitee.qdbp.base.model.condition.Where;
import com.gitee.qdbp.general.common.api.sequence.service.ILocalSequenceGenerator;
import com.gitee.qdbp.tools.utils.DateTools;
import com.gitee.qdbp.tools.utils.JsonTools;
import com.gitee.qdbp.tools.utils.VerifyTools;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.ShoppingCartBean;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.ShoppingCartUpdate;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.ShoppingCartWhere;
import com.gitee.qdbp.vmall.sales.biz.goods.manage.dao.IShoppingCartBasicDao;

/**
 * 用户购物车基础业务类
 *
 * @author zhh
 * @version 180703
 */
@Service
public class ShoppingCartBasic {

    /** 日志对象 **/
    private static final Logger log = LoggerFactory.getLogger(ShoppingCartBasic.class);

    /** 序列号生成(ID) **/
    @Autowired
    private ILocalSequenceGenerator sequenceGenerator;

    /** 用户购物车DAO **/
    @Autowired
    private IShoppingCartBasicDao shoppingCartBasicDao;

    /**
     * 用户购物车查询
     *
     * @param where 查询条件
     * @return 用户购物车
     * @throws ServiceException 查询失败
     */
    public ShoppingCartBean find(ShoppingCartWhere where) throws ServiceException {
        String msg = "Failed to query ShoppingCart. ";

        if (where == null) {
            log.error(msg + "params is null: where");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        if (where.getDataState() == null) {
            where.setDataState(DataState.NORMAL);
        }

        doHandleDate(where);

        ShoppingCartBean bean;
        try {
            bean = shoppingCartBasicDao.find(Where.of(where));
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + "\n\t" + JsonTools.toJsonString(where), e);
            throw new ServiceException(ResultCode.DB_SELECT_ERROR, e);
        }

        if (bean == null) {
            log.trace("ShoppingCart not found. {}", JsonTools.toJsonString(where));
        }
        return bean;
    }

    /**
     * 根据主键查询用户购物车
     *
     * @param id 主键
     * @return 用户购物车
     * @throws ServiceException 查询失败
     */
    public ShoppingCartBean findById(String id) throws ServiceException {
        String msg = "Failed to query ShoppingCart by id. ";

        if (VerifyTools.isBlank(id)) {
            log.error(msg + "params is null: id");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        ShoppingCartWhere where = new ShoppingCartWhere();
        where.setId(id);

        return find(where);
    }

    /**
     * 用户购物车查询
     *
     * @param paging 排序分页条件
     * @return 用户购物车列表
     * @throws ServiceException 查询失败
     */
    public PageList<ShoppingCartBean> list(OrderPaging paging) throws ServiceException {
        String msg = "Failed to query ShoppingCart. ";

        if (paging == null) {
            log.error(msg + "params is null: paging");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }
        return list(new ShoppingCartWhere(), paging);
    }

    /**
     * 用户购物车查询
     *
     * @param where 查询条件
     * @param paging 排序分页条件
     * @return 用户购物车列表
     * @throws ServiceException 查询失败
     */
    public PageList<ShoppingCartBean> list(ShoppingCartWhere where, OrderPaging paging) throws ServiceException {
        String msg = "Failed to query ShoppingCart. ";

        if (paging == null) {
            log.error(msg + "params is null: paging");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        if (where == null) {
            where = new ShoppingCartWhere();
            where.setDataState(DataState.NORMAL);
        } else if (where.getDataState() == null) {
            where.setDataState(DataState.NORMAL);
        }

        doHandleDate(where);

        try {
            OrderWhere<ShoppingCartWhere> ow = OrderWhere.of(where, paging.getOrderings());
            return PageList.of(shoppingCartBasicDao.list(ow, paging));
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + "\n\t" + JsonTools.toJsonString(where), e);
            throw new ServiceException(ResultCode.DB_SELECT_ERROR, e);
        }
    }

    /**
     * 根据主键查询用户购物车列表
     *
     * @param ids 主键列表
     * @return 用户购物车列表
     * @throws ServiceException 查询失败
     */
    public PageList<ShoppingCartBean> listByIds(List<String> ids) throws ServiceException {
        String msg = "Failed to query ShoppingCart by id list. ";

        if (VerifyTools.isBlank(ids)) {
            log.error(msg + "params is null: ids");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        ShoppingCartWhere where = new ShoppingCartWhere();
        where.setIds(ids);

        return list(where, OrderPaging.NONE);
    }

    /**
     * 创建用户购物车
     *
     * @param model 待新增的用户购物车
     * @return 受影响行数
     * @throws ServiceException 创建失败
     */
    public int create(ShoppingCartBean model) throws ServiceException {
        String msg = "Failed to create ShoppingCart. ";

        if (VerifyTools.isBlank(model)) {
            log.error(msg + "params is null: model");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        if (VerifyTools.isBlank(model.getId())) {
            model.setId(sequenceGenerator.generate(ShoppingCartBean.TABLE));
        }
        model.setDataState(DataState.NORMAL);

        doHandleDate(model);

        // 向vml_shopping_cart表插入记录
        int rows;
        try {
            rows = shoppingCartBasicDao.insert(model);
        } catch (DuplicateKeyException e) {
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(model), e);
            throw new ServiceException(ResultCode.DB_DUPLICATE_KEY, e);
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(model), e);
            throw new ServiceException(ResultCode.DB_INSERT_ERROR, e);
        }

        if (rows == 0) {
            log.error(msg + "affected rows is 0. \n\t" + JsonTools.toJsonString(model));
            throw new ServiceException(ResultCode.DB_INSERT_ERROR);
        }
        return rows;
    }

    /**
     * 批量创建用户购物车
     *
     * @param models 待新增的用户购物车
     * @return 受影响行数
     * @throws ServiceException 创建失败
     */
    public int create(List<ShoppingCartBean> models) throws ServiceException {
        String msg = "Failed to batch create ShoppingCart. ";

        if (VerifyTools.isBlank(models)) {
            log.error(msg + "params is null: models");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        for (ShoppingCartBean model : models) {
            if (VerifyTools.isBlank(model.getId())) {
                model.setId(sequenceGenerator.generate(ShoppingCartBean.TABLE));
            }
            model.setDataState(DataState.NORMAL);

            doHandleDate(model);
        }

        // 向vml_shopping_cart表插入记录
        int rows;
        try {
            rows = shoppingCartBasicDao.inserts(models);
        } catch (DuplicateKeyException e) {
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(models), e);
            throw new ServiceException(ResultCode.DB_DUPLICATE_KEY, e);
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(models), e);
            throw new ServiceException(ResultCode.DB_INSERT_ERROR, e);
        }

        int size = models.size();
        if (rows != size) {
            String desc = size > 0 ? "affected rows " + rows + "!=" + size + ". " : "affected rows is 0. ";
            log.error(msg + desc + "\n\t" + JsonTools.toJsonString(models));
            throw new ServiceException(ResultCode.DB_INSERT_ERROR);
        }
        return rows;
    }

    /**
     * 修改用户购物车
     *
     * @param model 待修改的内容
     * @param errorOnUnaffected 受影响行数为0时是否抛异常
     * @return 受影响行数
     * @throws ServiceException 修改失败
     */
    public int update(ShoppingCartUpdate model, boolean errorOnUnaffected) throws ServiceException {
        String msg = "Failed to update ShoppingCart. ";

        if (model == null) {
            log.error(msg + "params is null: model");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        ShoppingCartWhere where = model.getWhere(true);
        if (VerifyTools.isBlank(where.getId()) && VerifyTools.isNotBlank(model.getId())) {
            where.setId(model.getId());
        }
        model.setDataState(null); // 数据状态只允许通过删除操作来修改
        if (where.getDataState() == null) {
            where.setDataState(DataState.NORMAL);
        }
        return doUpdate(model, 0, errorOnUnaffected);
    }

    /**
     * 根据主键删除用户购物车
     *
     * @param ids 待删除的用户购物车的主键
     * @param errorOnRowsNotMatch 删除行数不匹配时是否抛异常
     * @return 删除行数
     * @throws ServiceException 删除失败
     */
    public int deleteByIds(List<String> ids, boolean errorOnRowsNotMatch) throws ServiceException {
        String msg = "Failed to delete ShoppingCart. ";

        if (VerifyTools.isBlank(ids)) {
            log.error(msg + "params is null: ids");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        ShoppingCartWhere where = new ShoppingCartWhere();
        where.setIds(ids);

        return doDelete(where, ids.size(), errorOnRowsNotMatch);
    }

    /**
     * 根据条件删除用户购物车
     *
     * @param where 条件
     * @param errorOnUnaffected 删除行数为0时是否抛异常
     * @return 删除行数
     * @throws ServiceException 删除失败
     */
    public int delete(ShoppingCartWhere where, boolean errorOnUnaffected) throws ServiceException {
        String msg = "Failed to delete ShoppingCart. ";

        if (where == null) {
            log.error(msg + "params is null: where");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }
        return doDelete(where, 0, errorOnUnaffected);
    }

    /**
     * 根据条件删除用户购物车
     *
     * @param where 条件
     * @param size 应删除条数, 0表示未知
     * @param errorOnRowsNotMatch 删除行数不匹配时是否抛异常
     * @param isId 是不是唯一主键
     * @return 删除行数
     * @throws ServiceException 删除失败
     */
    private int doDelete(ShoppingCartWhere where, int size, boolean errorOnRowsNotMatch) throws ServiceException {
        // 逻辑删除
        if (where.getDataState() == null) {
            where.setDataState(DataState.NORMAL);
        }
        ShoppingCartUpdate model = new ShoppingCartUpdate();
        model.setDataState(DataState.DELETED);
        model.setWhere(where);
        return doUpdate(model, size, errorOnRowsNotMatch);
    }

    /**
     * 修改用户购物车
     *
     * @param model 待修改的内容
     * @param size 应删除条数, 0表示未知
     * @param errorOnRowsNotMatch 删除行数不匹配时是否抛异常
     * @return 受影响行数
     * @throws ServiceException 修改失败
     */
    private int doUpdate(ShoppingCartUpdate model, int size, boolean errorOnRowsNotMatch) throws ServiceException {
        String msg = "Failed to update ShoppingCart. ";

        if (model == null) {
            log.error(msg + "params is null: model");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        if (model.getWhere() == null) {
            log.error(msg + "params is null: model.getWhere()");
            throw new ServiceException(ResultCode.PARAMETER_IS_REQUIRED);
        }

        doHandleDate(model);
        doHandleDate(model.getWhere());

        int rows;
        try {
            rows = shoppingCartBasicDao.update(model);
        } catch (DuplicateKeyException e) {
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(model), e);
            throw new ServiceException(ResultCode.DB_DUPLICATE_KEY, e);
        } catch (Exception e) {
            ServiceException.throwWhenServiceException(e);
            log.error(msg + e.getClass().getSimpleName() + ".\n\t" + JsonTools.toJsonString(model), e);
            throw new ServiceException(ResultCode.DB_UPDATE_ERROR, e);
        }

        if (size > 0 && rows != size || rows == 0) {
            String desc = size > 0 ? "affected rows != " + size + ". " : "affected rows is 0. ";
            if (errorOnRowsNotMatch) {
                log.error(msg + desc + "\n\t" + JsonTools.toJsonString(model));
                throw new ServiceException(ResultCode.DB_UPDATE_ERROR);
            } else {
                log.debug(msg + desc + "\n\t" + JsonTools.toJsonString(model));
            }
        }
        return rows;
    }

    // 由于数据库不支持毫秒, 超过500毫秒会因四舍五入导致变成下一天0点, 因此将毫秒清掉
    protected static void doHandleDate(ShoppingCartBean model) {
        if (model == null) {
            return;
        }

        if (VerifyTools.isNotBlank(model.getCreateTime())) { // 创建时间
            model.setCreateTime(DateTools.setMillisecond(model.getCreateTime(), 0));
        }
    }

    // 由于数据库不支持毫秒, 超过500毫秒会因四舍五入导致变成下一天0点, 因此将毫秒清掉
    // 处理XxxTime, XxxTimeMin和XxxTimeMax, XxxTimeMinWithDay和XxxTimeMaxWithDay
    protected static void doHandleDate(ShoppingCartWhere model) {
        if (model == null) {
            return;
        }

        if (VerifyTools.isNotBlank(model.getCreateTime())) { // 创建时间
            model.setCreateTime(DateTools.setMillisecond(model.getCreateTime(), 0));
        }

        if (VerifyTools.isNotBlank(model.getCreateTimeMin())) { // 最小创建时间
            model.setCreateTimeMin(DateTools.setMillisecond(model.getCreateTimeMin(), 0));
        }
        if (VerifyTools.isNotBlank(model.getCreateTimeMinWithDay())) { // 最小创建时间
            model.setCreateTimeMinWithDay(DateTools.toStartTime(model.getCreateTimeMinWithDay()));
        }
        if (VerifyTools.isNotBlank(model.getCreateTimeMax())) { // 最大创建时间
            model.setCreateTimeMax(DateTools.setMillisecond(model.getCreateTimeMax(), 0));
        }
        if (VerifyTools.isNotBlank(model.getCreateTimeMaxWithDay())) { // 最大创建时间
            model.setCreateTimeMaxWithDay(DateTools.setMillisecond(DateTools.toEndTime(model.getCreateTimeMaxWithDay()), 0));
        }
    }
}