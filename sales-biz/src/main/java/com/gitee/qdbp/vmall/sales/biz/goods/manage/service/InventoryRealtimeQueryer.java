package com.gitee.qdbp.vmall.sales.biz.goods.manage.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.InventoryRealtimeBean;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.InventoryRealtimeWhere;
import com.gitee.qdbp.vmall.sales.api.goods.manage.service.IInventoryRealtimeQueryer;
import com.gitee.qdbp.vmall.sales.biz.goods.manage.basic.InventoryRealtimeBasic;

/**
 * 实时库存业务处理类
 *
 * @author zhh
 * @version 170812
 */
@Service
@Transactional(readOnly = true)
public class InventoryRealtimeQueryer implements IInventoryRealtimeQueryer {

    /** 实时库存DAO **/
    @Autowired
    private InventoryRealtimeBasic inventoryRealtimeBasic;

    @Override
    public InventoryRealtimeBean find(InventoryRealtimeWhere where) throws ServiceException {
        return inventoryRealtimeBasic.find(where);
    }

    @Override
    public PageList<InventoryRealtimeBean> list(InventoryRealtimeWhere where, OrderPaging paging) throws ServiceException {
        return inventoryRealtimeBasic.list(where, paging);
    }

    @Override
    public int findBySkuId(String skuId) throws ServiceException {
        InventoryRealtimeWhere where = new InventoryRealtimeWhere();
        where.setSkuId(skuId);
        InventoryRealtimeBean inventory = find(where);
        return inventory == null ? 0 : inventory.getStockQuantity();
    }

    @Override
    public List<InventoryRealtimeBean> listBySkuId(List<String> skuIds) throws ServiceException {
        InventoryRealtimeWhere where = new InventoryRealtimeWhere();
        where.setSkuIds(skuIds);
        PageList<InventoryRealtimeBean> inventories = list(where, OrderPaging.NONE);
        return inventories == null ? null : inventories.toList();
    }
}
