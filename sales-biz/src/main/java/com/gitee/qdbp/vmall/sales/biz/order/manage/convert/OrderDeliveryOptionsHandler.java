package com.gitee.qdbp.vmall.sales.biz.order.manage.convert;

import com.gitee.qdbp.base.orm.mybatis.converter.JsonBeanTypeHandler;
import com.gitee.qdbp.vmall.sales.api.order.manage.model.OrderDeliveryOptions;

/**
 * 选项类型转换类
 *
 * @author zhh
 * @version 180624
 */
public class OrderDeliveryOptionsHandler extends JsonBeanTypeHandler<OrderDeliveryOptions> {

    public OrderDeliveryOptionsHandler() {
        this.addIgnoreField("extra");
    }

    @Override
    public Class<OrderDeliveryOptions> getBeanType() {
        return OrderDeliveryOptions.class;
    }

}