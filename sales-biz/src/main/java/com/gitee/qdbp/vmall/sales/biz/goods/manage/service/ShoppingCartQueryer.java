package com.gitee.qdbp.vmall.sales.biz.goods.manage.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.ShoppingCartBean;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.ShoppingCartWhere;
import com.gitee.qdbp.vmall.sales.api.goods.manage.service.IShoppingCartQueryer;
import com.gitee.qdbp.vmall.sales.biz.goods.manage.basic.ShoppingCartBasic;

/**
 * 用户购物车业务处理类
 *
 * @author zhh
 * @version 180703
 */
@Service
@Transactional(readOnly = true)
public class ShoppingCartQueryer implements IShoppingCartQueryer {

    @Autowired
    private ShoppingCartBasic shoppingCartBasic;

    @Override
    public ShoppingCartBean find(String id) throws ServiceException {
        return shoppingCartBasic.findById(id);
    }

    @Override
    public ShoppingCartBean find(ShoppingCartWhere where) throws ServiceException {
        return shoppingCartBasic.find(where);
    }

    @Override
    public PageList<ShoppingCartBean> list(ShoppingCartWhere where, OrderPaging paging) throws ServiceException {
        return shoppingCartBasic.list(where, paging);
    }

}