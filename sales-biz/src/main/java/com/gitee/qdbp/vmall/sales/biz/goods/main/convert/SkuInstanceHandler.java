package com.gitee.qdbp.vmall.sales.biz.goods.main.convert;

import com.gitee.qdbp.base.orm.mybatis.converter.JsonBeanListHandler;
import com.gitee.qdbp.vmall.sales.api.specific.model.SkuInstance;

/**
 * 单品实例列表类型转换类
 *
 * @author zhh
 * @version 180627
 */
public class SkuInstanceHandler extends JsonBeanListHandler<SkuInstance> {

    public SkuInstanceHandler() {
        this.addIgnoreField("extra");
    }

    @Override
    public Class<SkuInstance> getBeanType() {
        return SkuInstance.class;
    }

}