package com.gitee.qdbp.vmall.sales.api.order.main.model;

import java.util.Date;
import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;
import com.gitee.qdbp.base.enums.DataState;

/**
 * 订单配送信息实体类
 *
 * @author zhh
 * @version 180624
 */
@OperateTraces(id = "id")
@DataIsolation("tenantCode")
public class OrderReceiverBean extends OrderReceiverBase {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 表名 **/
    public static final String TABLE = "vml_order_receiver";

    /** 主键 **/
    private String id;

    /** 商家租户编号 **/
    private String tenantCode;

    /** 订单ID **/
    private String orderId;

    /** 排序号(0为当前有效记录) **/
    private Integer sortIndex;

    /** 创建时间 **/
    private Date createTime;

    /** 选项 **/
    private OrderReceiverOptions options;

    /** 数据状态:0为正常|其他为删除 **/
    private DataState dataState;

    /** 获取主键 **/
    public String getId() {
        return id;
    }

    /** 设置主键 **/
    public void setId(String id) {
        this.id = id;
    }

    /** 获取商家租户编号 **/
    public String getTenantCode() {
        return tenantCode;
    }

    /** 设置商家租户编号 **/
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /** 获取订单ID **/
    public String getOrderId() {
        return orderId;
    }

    /** 设置订单ID **/
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /** 获取排序号(0为当前有效记录) **/
    public Integer getSortIndex() {
        return sortIndex;
    }

    /** 设置排序号(0为当前有效记录) **/
    public void setSortIndex(Integer sortIndex) {
        this.sortIndex = sortIndex;
    }

    /** 获取创建时间 **/
    public Date getCreateTime() {
        return createTime;
    }

    /** 设置创建时间 **/
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /** 获取选项 **/
    public OrderReceiverOptions getOptions() {
        return options;
    }

    /** 获取选项, force=是否强制返回非空对象 **/
    public OrderReceiverOptions getOptions(boolean force) {
        if (options == null && force) {
            options = new OrderReceiverOptions();
        }
        return options;
    }

    /** 设置选项 **/
    public void setOptions(OrderReceiverOptions options) {
        this.options = options;
    }

    /** 获取数据状态:0为正常|其他为删除 **/
    public DataState getDataState() {
        return dataState;
    }

    /** 设置数据状态:0为正常|其他为删除 **/
    public void setDataState(DataState dataState) {
        this.dataState = dataState;
    }

    /**
     * 将当前对象转换为子类对象
     *
     * @param clazz 目标类型
     * @return 目标对象
     */
    public <T extends OrderReceiverBase> T to(Class<T> clazz) {
        T instance = super.to(clazz);

        if (instance instanceof OrderReceiverBean) {
            OrderReceiverBean real = (OrderReceiverBean) instance;
            real.setId(this.getId()); // 主键
            real.setTenantCode(this.getTenantCode()); // 商家租户编号
            real.setOrderId(this.getOrderId()); // 订单ID
            real.setSortIndex(this.getSortIndex()); // 排序号(0为当前有效记录)
            real.setCreateTime(this.getCreateTime()); // 创建时间
            real.setOptions(this.getOptions()); // 选项
            real.setDataState(this.getDataState()); // 数据状态:0为正常|其他为删除
        }

        return instance;
    }

}
