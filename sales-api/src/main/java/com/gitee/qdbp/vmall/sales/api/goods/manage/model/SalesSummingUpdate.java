package com.gitee.qdbp.vmall.sales.api.goods.manage.model;

import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;

/**
 * 销量汇总更新类
 *
 * @author zhh
 * @version 180626
 */
@OperateTraces(target = "where")
@DataIsolation(target = "where")
public class SalesSummingUpdate extends SalesSummingBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 租户编号是否更新为空值 **/
    private Boolean tenantCodeToNull;

    /** 商品唯一ID是否更新为空值 **/
    private Boolean goodsUidToNull;

    /** 单品ID是否更新为空值 **/
    private Boolean skuIdToNull;

    /** 汇总类型(0.总|1.年|2.月|3.周|4.日)是否更新为空值 **/
    private Boolean sumTypeToNull;

    /** 时间范围是否更新为空值 **/
    private Boolean timeRangeToNull;
    /** 时间范围的增加值 **/
    private Integer timeRangeAdd;

    /** 销量是否更新为空值 **/
    private Boolean salesQuantityToNull;
    /** 销量的增加值 **/
    private Integer salesQuantityAdd;

    /** 更新操作的条件 **/
    private SalesSummingWhere where;

    /** 判断租户编号是否更新为空值 **/
    public Boolean isTenantCodeToNull() {
        return tenantCodeToNull;
    }

    /**
     * 设置租户编号是否更新为空值
     *
     * @param tenantCodeToNull 租户编号是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTenantCodeToNull(Boolean tenantCodeToNull) {
        this.tenantCodeToNull = tenantCodeToNull;
    }

    /** 判断商品唯一ID是否更新为空值 **/
    public Boolean isGoodsUidToNull() {
        return goodsUidToNull;
    }

    /**
     * 设置商品唯一ID是否更新为空值
     *
     * @param goodsUidToNull 商品唯一ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setGoodsUidToNull(Boolean goodsUidToNull) {
        this.goodsUidToNull = goodsUidToNull;
    }

    /** 判断单品ID是否更新为空值 **/
    public Boolean isSkuIdToNull() {
        return skuIdToNull;
    }

    /**
     * 设置单品ID是否更新为空值
     *
     * @param skuIdToNull 单品ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSkuIdToNull(Boolean skuIdToNull) {
        this.skuIdToNull = skuIdToNull;
    }

    /** 判断汇总类型(0.总|1.年|2.月|3.周|4.日)是否更新为空值 **/
    public Boolean isSumTypeToNull() {
        return sumTypeToNull;
    }

    /**
     * 设置汇总类型(0.总|1.年|2.月|3.周|4.日)是否更新为空值
     *
     * @param sumTypeToNull 汇总类型(0.总|1.年|2.月|3.周|4.日)是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSumTypeToNull(Boolean sumTypeToNull) {
        this.sumTypeToNull = sumTypeToNull;
    }

    /** 判断时间范围是否更新为空值 **/
    public Boolean isTimeRangeToNull() {
        return timeRangeToNull;
    }

    /**
     * 设置时间范围是否更新为空值
     *
     * @param timeRangeToNull 时间范围是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTimeRangeToNull(Boolean timeRangeToNull) {
        this.timeRangeToNull = timeRangeToNull;
    }

    /** 获取时间范围的增加值 **/
    public Integer getTimeRangeAdd() {
        return timeRangeAdd;
    }

    /** 设置时间范围的增加值 **/
    public void setTimeRangeAdd(Integer timeRangeAdd) {
        this.timeRangeAdd = timeRangeAdd;
    }

    /** 判断销量是否更新为空值 **/
    public Boolean isSalesQuantityToNull() {
        return salesQuantityToNull;
    }

    /**
     * 设置销量是否更新为空值
     *
     * @param salesQuantityToNull 销量是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSalesQuantityToNull(Boolean salesQuantityToNull) {
        this.salesQuantityToNull = salesQuantityToNull;
    }

    /** 获取销量的增加值 **/
    public Integer getSalesQuantityAdd() {
        return salesQuantityAdd;
    }

    /** 设置销量的增加值 **/
    public void setSalesQuantityAdd(Integer salesQuantityAdd) {
        this.salesQuantityAdd = salesQuantityAdd;
    }

    /** 获取更新操作的条件 **/
    public SalesSummingWhere getWhere() {
        return where;
    }

    /** 获取更新操作的条件, force=是否强制返回非空条件 **/
    public SalesSummingWhere getWhere(boolean force) {
        if (where == null && force) {
            where = new SalesSummingWhere();
        }
        return where;
    }

    /** 设置更新操作的条件 **/
    public void setWhere(SalesSummingWhere where) {
        this.where = where;
    }
}