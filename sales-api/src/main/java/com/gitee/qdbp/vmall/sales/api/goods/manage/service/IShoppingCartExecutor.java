package com.gitee.qdbp.vmall.sales.api.goods.manage.service;

import java.util.List;
import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.base.system.model.IAccount;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.ShoppingGoodsItem;

/**
 * 用户购物车业务接口
 *
 * @author zhh
 * @version 180703
 */
public interface IShoppingCartExecutor {

    /** 创建购物车记录 **/
    void create(String userId, ShoppingGoodsItem params, IAccount me) throws ServiceException;

    /** 修改数量 **/
    void updateQuantityById(String shoppingCartId, int quantity, IAccount me) throws ServiceException;

    /** 修改选中状态 **/
    void updateCheckedById(String shoppingCartId, boolean checked, IAccount me) throws ServiceException;

    /** 修改指定用户所有记录的选中状态 **/
    void updateCheckedByUser(String userId, boolean checked, IAccount me) throws ServiceException;

    /** 删除购物车记录 **/
    void deleteById(String shoppingCartId, IAccount me) throws ServiceException;

    /** 删除购物车记录 **/
    void deleteById(List<String> shoppingCartIds, IAccount me) throws ServiceException;

}
