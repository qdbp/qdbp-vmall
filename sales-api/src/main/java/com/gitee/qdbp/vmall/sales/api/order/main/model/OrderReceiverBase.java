package com.gitee.qdbp.vmall.sales.api.order.main.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.PartList;
import com.gitee.qdbp.base.enums.Gender;

/**
 * 订单配送信息实体类
 *
 * @author zhh
 * @version 180624
 */
public class OrderReceiverBase implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 快递公司ID **/
    private String carrierCompanyId;

    /** 收件人姓名 **/
    private String receiverName;

    /** 收件人性别(0.未知|1.男|2.女) **/
    private Gender receiverGender;

    /** 收件人手机号码 **/
    private String receiverPhone;

    /** 收件人城市(关联行政区划表) **/
    private String receiverAreaCode;

    /** 收件人地址 **/
    private String receiverAddress;

    /** 获取快递公司ID **/
    public String getCarrierCompanyId() {
        return carrierCompanyId;
    }

    /** 设置快递公司ID **/
    public void setCarrierCompanyId(String carrierCompanyId) {
        this.carrierCompanyId = carrierCompanyId;
    }

    /** 获取收件人姓名 **/
    public String getReceiverName() {
        return receiverName;
    }

    /** 设置收件人姓名 **/
    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    /** 获取收件人性别(0.未知|1.男|2.女) **/
    public Gender getReceiverGender() {
        return receiverGender;
    }

    /** 设置收件人性别(0.未知|1.男|2.女) **/
    public void setReceiverGender(Gender receiverGender) {
        this.receiverGender = receiverGender;
    }

    /** 获取收件人手机号码 **/
    public String getReceiverPhone() {
        return receiverPhone;
    }

    /** 设置收件人手机号码 **/
    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    /** 获取收件人城市(关联行政区划表) **/
    public String getReceiverAreaCode() {
        return receiverAreaCode;
    }

    /** 设置收件人城市(关联行政区划表) **/
    public void setReceiverAreaCode(String receiverAreaCode) {
        this.receiverAreaCode = receiverAreaCode;
    }

    /** 获取收件人地址 **/
    public String getReceiverAddress() {
        return receiverAddress;
    }

    /** 设置收件人地址 **/
    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    /**
     * 将当前对象转换为子类对象
     *
     * @param clazz 目标类型
     * @return 目标对象
     */
    public <T extends OrderReceiverBase> T to(Class<T> clazz) {
        T instance;
        try {
            instance = clazz.newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to create " + clazz.getSimpleName() + " instance.", e);
        }

        instance.setCarrierCompanyId(this.getCarrierCompanyId()); // 快递公司ID
        instance.setReceiverName(this.getReceiverName()); // 收件人姓名
        instance.setReceiverGender(this.getReceiverGender()); // 收件人性别(0.未知|1.男|2.女)
        instance.setReceiverPhone(this.getReceiverPhone()); // 收件人手机号码
        instance.setReceiverAreaCode(this.getReceiverAreaCode()); // 收件人城市(关联行政区划表)
        instance.setReceiverAddress(this.getReceiverAddress()); // 收件人地址
        return instance;
    }

    /**
     * 将OrderReceiverBean转换为子类对象
     *
     * @param beans 待转换的对象列表
     * @param clazz 目标类型
     * @param <T> OrderReceiverBean或子类
     * @param <C> T的子类
     * @return 目标对象列表
     */
    public static <T extends OrderReceiverBase, C extends T> List<C> to(List<T> beans, Class<C> clazz) {
        if (beans == null) {
            return null;
        }
        List<C> list;
        if (beans instanceof PartList) {
            PartList<C> partlist = new PartList<>();
            partlist.setTotal(((PartList<?>) beans).getTotal());
            list = partlist;
        } else {
            list = new ArrayList<>();
        }
        for (OrderReceiverBase bean : beans) {
            list.add(bean.to(clazz));
        }
        return list;
    }

}