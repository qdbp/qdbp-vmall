package com.gitee.qdbp.vmall.sales.api.goods.manage.service;

import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.ShoppingCartBean;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.ShoppingCartWhere;

/**
 * 用户购物车业务接口
 *
 * @author zhh
 * @version 180703
 */
public interface IShoppingCartQueryer {

    ShoppingCartBean find(String id) throws ServiceException;

    ShoppingCartBean find(ShoppingCartWhere where) throws ServiceException;

    PageList<ShoppingCartBean> list(ShoppingCartWhere where, OrderPaging paging) throws ServiceException;

}