package com.gitee.qdbp.vmall.sales.api.goods.main.model;

/**
 * 商品发布参数(单一商品,没有规格)
 *
 * @author zhaohuihua
 * @version 170812
 */
public class PublishSingleParams extends GoodsPriceBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 商品版本ID **/
    private String vid;
    /** 初始销售量 **/
    private Integer salesQuantityAdd;
    /** 初始库存量 **/
    private Integer stockQuantityAdd;
    /** 操作说明 **/
    private String description;

    /** 商品版本ID **/
    public String getVid() {
        return vid;
    }

    /** 商品版本ID **/
    public void setVid(String vid) {
        this.vid = vid;
    }

    /** 初始销售量 **/
    public Integer getSalesQuantityAdd() {
        return salesQuantityAdd;
    }

    /** 初始销售量 **/
    public void setSalesQuantityAdd(Integer salesQuantityAdd) {
        this.salesQuantityAdd = salesQuantityAdd;
    }

    /** 初始库存量 **/
    public Integer getStockQuantityAdd() {
        return stockQuantityAdd;
    }

    /** 初始库存量 **/
    public void setStockQuantityAdd(Integer stockQuantityAdd) {
        this.stockQuantityAdd = stockQuantityAdd;
    }

    /** 操作说明 **/
    public String getDescription() {
        return description;
    }

    /** 操作说明 **/
    public void setDescription(String description) {
        this.description = description;
    }

}
