package com.gitee.qdbp.vmall.sales.api.goods.main.model;

import java.util.List;
import com.gitee.qdbp.able.model.reusable.ExtraData;

/**
 * 图片信息
 *
 * @author zhh
 * @version 170701
 */
public class GoodsImages extends ExtraData {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 列表图 **/
    private String preview;

    /** 轮播图 **/
    private List<String> details;

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
        if (this.preview == null && details != null && !details.isEmpty()) {
            this.preview = details.get(0);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((details == null) ? 0 : details.hashCode());
        result = prime * result + ((preview == null) ? 0 : preview.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        GoodsImages other = (GoodsImages) obj;
        if (details == null) {
            if (other.details != null) return false;
        } else if (!details.equals(other.details)) return false;
        if (preview == null) {
            if (other.preview != null) return false;
        } else if (!preview.equals(other.preview)) return false;
        return true;
    }

}
