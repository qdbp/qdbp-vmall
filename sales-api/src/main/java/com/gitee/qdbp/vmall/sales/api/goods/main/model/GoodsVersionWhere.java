package com.gitee.qdbp.vmall.sales.api.goods.main.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import com.gitee.qdbp.general.common.enums.VersionState;
import com.gitee.qdbp.vmall.sales.enums.GoodsState;

/**
 * 商品版本信息查询类
 *
 * @author zhh
 * @version 180627
 */
public class GoodsVersionWhere extends GoodsVersionBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 商品版本ID列表 **/
    private List<String> vids;

    /** 商品版本ID前模匹配条件 **/
    private String vidStarts;

    /** 商品版本ID后模匹配条件 **/
    private String vidEnds;

    /** 商品版本ID模糊查询条件 **/
    private String vidLike;

    /** 商品唯一ID空值/非空值查询 **/
    private Boolean uidIsNull;

    /** 商品唯一ID列表 **/
    private List<String> uids;

    /** 商品唯一ID前模匹配条件 **/
    private String uidStarts;

    /** 商品唯一ID后模匹配条件 **/
    private String uidEnds;

    /** 商品唯一ID模糊查询条件 **/
    private String uidLike;

    /** 租户编号空值/非空值查询 **/
    private Boolean tenantCodeIsNull;

    /** 租户编号前模匹配条件 **/
    private String tenantCodeStarts;

    /** 租户编号后模匹配条件 **/
    private String tenantCodeEnds;

    /** 租户编号模糊查询条件 **/
    private String tenantCodeLike;

    /** 部门编号空值/非空值查询 **/
    private Boolean deptCodeIsNull;

    /** 部门编号前模匹配条件 **/
    private String deptCodeStarts;

    /** 部门编号后模匹配条件 **/
    private String deptCodeEnds;

    /** 部门编号模糊查询条件 **/
    private String deptCodeLike;

    /** 分类编号空值/非空值查询 **/
    private Boolean categoryCodeIsNull;

    /** 分类编号前模匹配条件 **/
    private String categoryCodeStarts;

    /** 分类编号后模匹配条件 **/
    private String categoryCodeEnds;

    /** 分类编号模糊查询条件 **/
    private String categoryCodeLike;

    /** 品牌编号空值/非空值查询 **/
    private Boolean brandCodeIsNull;

    /** 品牌编号前模匹配条件 **/
    private String brandCodeStarts;

    /** 品牌编号后模匹配条件 **/
    private String brandCodeEnds;

    /** 品牌编号模糊查询条件 **/
    private String brandCodeLike;

    /** 型号空值/非空值查询 **/
    private Boolean modelNumberIsNull;

    /** 型号前模匹配条件 **/
    private String modelNumberStarts;

    /** 型号后模匹配条件 **/
    private String modelNumberEnds;

    /** 型号模糊查询条件 **/
    private String modelNumberLike;

    /** 商品名称空值/非空值查询 **/
    private Boolean nameIsNull;

    /** 商品名称前模匹配条件 **/
    private String nameStarts;

    /** 商品名称后模匹配条件 **/
    private String nameEnds;

    /** 商品名称模糊查询条件 **/
    private String nameLike;

    /** 标题空值/非空值查询 **/
    private Boolean titleIsNull;

    /** 标题前模匹配条件 **/
    private String titleStarts;

    /** 标题后模匹配条件 **/
    private String titleEnds;

    /** 标题模糊查询条件 **/
    private String titleLike;

    /** 单位(个/件/台/袋等)空值/非空值查询 **/
    private Boolean unitIsNull;

    /** 单位(个/件/台/袋等)前模匹配条件 **/
    private String unitStarts;

    /** 单位(个/件/台/袋等)后模匹配条件 **/
    private String unitEnds;

    /** 单位(个/件/台/袋等)模糊查询条件 **/
    private String unitLike;

    /** 图片信息空值/非空值查询 **/
    private Boolean imageInfoIsNull;

    /** 市场价格描述空值/非空值查询 **/
    private Boolean marketPriceTextIsNull;

    /** 市场价格描述前模匹配条件 **/
    private String marketPriceTextStarts;

    /** 市场价格描述后模匹配条件 **/
    private String marketPriceTextEnds;

    /** 市场价格描述模糊查询条件 **/
    private String marketPriceTextLike;

    /** 销售价格描述(用于主商品展示的价格描述, 如80~99,199起)空值/非空值查询 **/
    private Boolean salesPriceTextIsNull;

    /** 销售价格描述(用于主商品展示的价格描述, 如80~99,199起)前模匹配条件 **/
    private String salesPriceTextStarts;

    /** 销售价格描述(用于主商品展示的价格描述, 如80~99,199起)后模匹配条件 **/
    private String salesPriceTextEnds;

    /** 销售价格描述(用于主商品展示的价格描述, 如80~99,199起)模糊查询条件 **/
    private String salesPriceTextLike;

    /** 价格值(实际售价)空值/非空值查询 **/
    private Boolean priceValueIsNull;

    /** 最小价格值(实际售价) **/
    private Double priceValueMin;

    /** 最大价格值(实际售价) **/
    private Double priceValueMax;

    /** 总销量空值/非空值查询 **/
    private Boolean salesQuantityTotalIsNull;

    /** 最小总销量 **/
    private Integer salesQuantityTotalMin;

    /** 最大总销量 **/
    private Integer salesQuantityTotalMax;

    /** 库存量空值/非空值查询 **/
    private Boolean stockQuantityIsNull;

    /** 最小库存量 **/
    private Integer stockQuantityMin;

    /** 最大库存量 **/
    private Integer stockQuantityMax;

    /** 摘要空值/非空值查询 **/
    private Boolean introTextIsNull;

    /** 摘要前模匹配条件 **/
    private String introTextStarts;

    /** 摘要后模匹配条件 **/
    private String introTextEnds;

    /** 摘要模糊查询条件 **/
    private String introTextLike;

    /** 详细描述(富文本)空值/非空值查询 **/
    private Boolean descDetailsIsNull;

    /** 详细描述(富文本)前模匹配条件 **/
    private String descDetailsStarts;

    /** 详细描述(富文本)后模匹配条件 **/
    private String descDetailsEnds;

    /** 详细描述(富文本)模糊查询条件 **/
    private String descDetailsLike;

    /** 排序号空值/非空值查询 **/
    private Boolean sortIndexIsNull;

    /** 最小排序号 **/
    private Long sortIndexMin;

    /** 最大排序号 **/
    private Long sortIndexMax;

    /** 规格分类ID空值/非空值查询 **/
    private Boolean specificKindIdIsNull;

    /** 规格分类ID前模匹配条件 **/
    private String specificKindIdStarts;

    /** 规格分类ID后模匹配条件 **/
    private String specificKindIdEnds;

    /** 规格分类ID模糊查询条件 **/
    private String specificKindIdLike;

    /** 规格数据列表空值/非空值查询 **/
    private Boolean specificKindDataIsNull;

    /** 单品实例列表空值/非空值查询 **/
    private Boolean skuInstanceDataIsNull;

    /** 创建人ID空值/非空值查询 **/
    private Boolean creatorIdIsNull;

    /** 创建人ID前模匹配条件 **/
    private String creatorIdStarts;

    /** 创建人ID后模匹配条件 **/
    private String creatorIdEnds;

    /** 创建人ID模糊查询条件 **/
    private String creatorIdLike;

    /** 最小创建时间 **/
    private Date createTimeMin;

    /** 最大创建时间 **/
    private Date createTimeMax;

    /** 最小创建时间 **/
    private Date createTimeMinWithDay;

    /** 最大创建时间 **/
    private Date createTimeMaxWithDay;

    /** 发布时间空值/非空值查询 **/
    private Boolean publishTimeIsNull;

    /** 最小发布时间 **/
    private Date publishTimeMin;

    /** 最大发布时间 **/
    private Date publishTimeMax;

    /** 最小发布时间 **/
    private Date publishTimeMinWithDay;

    /** 最大发布时间 **/
    private Date publishTimeMaxWithDay;

    /** 过期时间空值/非空值查询 **/
    private Boolean expireTimeIsNull;

    /** 最小过期时间 **/
    private Date expireTimeMin;

    /** 最大过期时间 **/
    private Date expireTimeMax;

    /** 最小过期时间 **/
    private Date expireTimeMinWithDay;

    /** 最大过期时间 **/
    private Date expireTimeMaxWithDay;

    /** 选项空值/非空值查询 **/
    private Boolean optionsIsNull;

    /** 提交说明空值/非空值查询 **/
    private Boolean submitDescIsNull;

    /** 提交说明前模匹配条件 **/
    private String submitDescStarts;

    /** 提交说明后模匹配条件 **/
    private String submitDescEnds;

    /** 提交说明模糊查询条件 **/
    private String submitDescLike;

    /** 编辑序号(每编辑一次递增1)空值/非空值查询 **/
    private Boolean editIndexIsNull;

    /** 最小编辑序号(每编辑一次递增1) **/
    private Integer editIndexMin;

    /** 最大编辑序号(每编辑一次递增1) **/
    private Integer editIndexMax;

    /** 商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)空值/非空值查询 **/
    private Boolean goodsStateIsNull;

    /** 商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)列表 **/
    private List<GoodsState> goodsStates;

    /** 版本状态(0.正本|1.副本|2.归档)空值/非空值查询 **/
    private Boolean versionStateIsNull;

    /** 版本状态(0.正本|1.副本|2.归档)列表 **/
    private List<VersionState> versionStates;

    /** 查询关键字空值/非空值查询 **/
    private Boolean queryKeywordsIsNull;

    /** 查询关键字前模匹配条件 **/
    private String queryKeywordsStarts;

    /** 查询关键字后模匹配条件 **/
    private String queryKeywordsEnds;

    /** 查询关键字模糊查询条件 **/
    private String queryKeywordsLike;

    /** 按Keywords搜索 **/
    private String searchByKeywords;

    /** 获取商品版本ID列表 **/
    public List<String> getVids() {
        return vids;
    }

    /** 设置商品版本ID列表 **/
    public void setVids(List<String> vids) {
        this.vids = vids;
    }

    /** 增加商品版本ID **/
    public void addVid(String... vids) {
        if (this.vids == null) {
            this.vids = new ArrayList<>();
        }
        this.vids.addAll(Arrays.asList(vids));
    }

    /** 获取商品版本ID前模匹配条件 **/
    public String getVidStarts() {
        return vidStarts;
    }

    /** 设置商品版本ID前模匹配条件 **/
    public void setVidStarts(String vidStarts) {
        this.vidStarts = vidStarts;
    }

    /** 获取商品版本ID后模匹配条件 **/
    public String getVidEnds() {
        return vidEnds;
    }

    /** 设置商品版本ID后模匹配条件 **/
    public void setVidEnds(String vidEnds) {
        this.vidEnds = vidEnds;
    }

    /** 获取商品版本ID模糊查询条件 **/
    public String getVidLike() {
        return vidLike;
    }

    /** 设置商品版本ID模糊查询条件 **/
    public void setVidLike(String vidLike) {
        this.vidLike = vidLike;
    }

    /** 判断商品唯一ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getUidIsNull() {
        return uidIsNull;
    }

    /**
     * 设置商品唯一ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param uidIsNull 商品唯一ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setUidIsNull(Boolean uidIsNull) {
        this.uidIsNull = uidIsNull;
    }

    /** 获取商品唯一ID列表 **/
    public List<String> getUids() {
        return uids;
    }

    /** 设置商品唯一ID列表 **/
    public void setUids(List<String> uids) {
        this.uids = uids;
    }

    /** 增加商品唯一ID **/
    public void addUid(String... uids) {
        if (this.uids == null) {
            this.uids = new ArrayList<>();
        }
        this.uids.addAll(Arrays.asList(uids));
    }

    /** 获取商品唯一ID前模匹配条件 **/
    public String getUidStarts() {
        return uidStarts;
    }

    /** 设置商品唯一ID前模匹配条件 **/
    public void setUidStarts(String uidStarts) {
        this.uidStarts = uidStarts;
    }

    /** 获取商品唯一ID后模匹配条件 **/
    public String getUidEnds() {
        return uidEnds;
    }

    /** 设置商品唯一ID后模匹配条件 **/
    public void setUidEnds(String uidEnds) {
        this.uidEnds = uidEnds;
    }

    /** 获取商品唯一ID模糊查询条件 **/
    public String getUidLike() {
        return uidLike;
    }

    /** 设置商品唯一ID模糊查询条件 **/
    public void setUidLike(String uidLike) {
        this.uidLike = uidLike;
    }

    /** 判断租户编号是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getTenantCodeIsNull() {
        return tenantCodeIsNull;
    }

    /**
     * 设置租户编号空值查询(true:空值查询|false:非空值查询)
     *
     * @param tenantCodeIsNull 租户编号空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTenantCodeIsNull(Boolean tenantCodeIsNull) {
        this.tenantCodeIsNull = tenantCodeIsNull;
    }

    /** 获取租户编号前模匹配条件 **/
    public String getTenantCodeStarts() {
        return tenantCodeStarts;
    }

    /** 设置租户编号前模匹配条件 **/
    public void setTenantCodeStarts(String tenantCodeStarts) {
        this.tenantCodeStarts = tenantCodeStarts;
    }

    /** 获取租户编号后模匹配条件 **/
    public String getTenantCodeEnds() {
        return tenantCodeEnds;
    }

    /** 设置租户编号后模匹配条件 **/
    public void setTenantCodeEnds(String tenantCodeEnds) {
        this.tenantCodeEnds = tenantCodeEnds;
    }

    /** 获取租户编号模糊查询条件 **/
    public String getTenantCodeLike() {
        return tenantCodeLike;
    }

    /** 设置租户编号模糊查询条件 **/
    public void setTenantCodeLike(String tenantCodeLike) {
        this.tenantCodeLike = tenantCodeLike;
    }

    /** 判断部门编号是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getDeptCodeIsNull() {
        return deptCodeIsNull;
    }

    /** 设置部门编号空值查询(true:空值查询|false:非空值查询) **/
    public void setDeptCodeIsNull(Boolean deptCodeIsNull) {
        this.deptCodeIsNull = deptCodeIsNull;
    }

    /** 获取部门编号前模匹配条件 **/
    public String getDeptCodeStarts() {
        return deptCodeStarts;
    }

    /** 设置部门编号前模匹配条件 **/
    public void setDeptCodeStarts(String deptCodeStarts) {
        this.deptCodeStarts = deptCodeStarts;
    }

    /** 获取部门编号后模匹配条件 **/
    public String getDeptCodeEnds() {
        return deptCodeEnds;
    }

    /** 设置部门编号后模匹配条件 **/
    public void setDeptCodeEnds(String deptCodeEnds) {
        this.deptCodeEnds = deptCodeEnds;
    }

    /** 获取部门编号模糊查询条件 **/
    public String getDeptCodeLike() {
        return deptCodeLike;
    }

    /** 设置部门编号模糊查询条件 **/
    public void setDeptCodeLike(String deptCodeLike) {
        this.deptCodeLike = deptCodeLike;
    }

    /** 判断分类编号是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getCategoryCodeIsNull() {
        return categoryCodeIsNull;
    }

    /**
     * 设置分类编号空值查询(true:空值查询|false:非空值查询)
     *
     * @param categoryCodeIsNull 分类编号空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setCategoryCodeIsNull(Boolean categoryCodeIsNull) {
        this.categoryCodeIsNull = categoryCodeIsNull;
    }

    /** 获取分类编号前模匹配条件 **/
    public String getCategoryCodeStarts() {
        return categoryCodeStarts;
    }

    /** 设置分类编号前模匹配条件 **/
    public void setCategoryCodeStarts(String categoryCodeStarts) {
        this.categoryCodeStarts = categoryCodeStarts;
    }

    /** 获取分类编号后模匹配条件 **/
    public String getCategoryCodeEnds() {
        return categoryCodeEnds;
    }

    /** 设置分类编号后模匹配条件 **/
    public void setCategoryCodeEnds(String categoryCodeEnds) {
        this.categoryCodeEnds = categoryCodeEnds;
    }

    /** 获取分类编号模糊查询条件 **/
    public String getCategoryCodeLike() {
        return categoryCodeLike;
    }

    /** 设置分类编号模糊查询条件 **/
    public void setCategoryCodeLike(String categoryCodeLike) {
        this.categoryCodeLike = categoryCodeLike;
    }

    /** 判断品牌编号是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getBrandCodeIsNull() {
        return brandCodeIsNull;
    }

    /** 设置品牌编号空值查询(true:空值查询|false:非空值查询) **/
    public void setBrandCodeIsNull(Boolean brandCodeIsNull) {
        this.brandCodeIsNull = brandCodeIsNull;
    }

    /** 获取品牌编号前模匹配条件 **/
    public String getBrandCodeStarts() {
        return brandCodeStarts;
    }

    /** 设置品牌编号前模匹配条件 **/
    public void setBrandCodeStarts(String brandCodeStarts) {
        this.brandCodeStarts = brandCodeStarts;
    }

    /** 获取品牌编号后模匹配条件 **/
    public String getBrandCodeEnds() {
        return brandCodeEnds;
    }

    /** 设置品牌编号后模匹配条件 **/
    public void setBrandCodeEnds(String brandCodeEnds) {
        this.brandCodeEnds = brandCodeEnds;
    }

    /** 获取品牌编号模糊查询条件 **/
    public String getBrandCodeLike() {
        return brandCodeLike;
    }

    /** 设置品牌编号模糊查询条件 **/
    public void setBrandCodeLike(String brandCodeLike) {
        this.brandCodeLike = brandCodeLike;
    }

    /** 判断型号是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getModelNumberIsNull() {
        return modelNumberIsNull;
    }

    /** 设置型号空值查询(true:空值查询|false:非空值查询) **/
    public void setModelNumberIsNull(Boolean modelNumberIsNull) {
        this.modelNumberIsNull = modelNumberIsNull;
    }

    /** 获取型号前模匹配条件 **/
    public String getModelNumberStarts() {
        return modelNumberStarts;
    }

    /** 设置型号前模匹配条件 **/
    public void setModelNumberStarts(String modelNumberStarts) {
        this.modelNumberStarts = modelNumberStarts;
    }

    /** 获取型号后模匹配条件 **/
    public String getModelNumberEnds() {
        return modelNumberEnds;
    }

    /** 设置型号后模匹配条件 **/
    public void setModelNumberEnds(String modelNumberEnds) {
        this.modelNumberEnds = modelNumberEnds;
    }

    /** 获取型号模糊查询条件 **/
    public String getModelNumberLike() {
        return modelNumberLike;
    }

    /** 设置型号模糊查询条件 **/
    public void setModelNumberLike(String modelNumberLike) {
        this.modelNumberLike = modelNumberLike;
    }

    /** 判断商品名称是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getNameIsNull() {
        return nameIsNull;
    }

    /** 设置商品名称空值查询(true:空值查询|false:非空值查询) **/
    public void setNameIsNull(Boolean nameIsNull) {
        this.nameIsNull = nameIsNull;
    }

    /** 获取商品名称前模匹配条件 **/
    public String getNameStarts() {
        return nameStarts;
    }

    /** 设置商品名称前模匹配条件 **/
    public void setNameStarts(String nameStarts) {
        this.nameStarts = nameStarts;
    }

    /** 获取商品名称后模匹配条件 **/
    public String getNameEnds() {
        return nameEnds;
    }

    /** 设置商品名称后模匹配条件 **/
    public void setNameEnds(String nameEnds) {
        this.nameEnds = nameEnds;
    }

    /** 获取商品名称模糊查询条件 **/
    public String getNameLike() {
        return nameLike;
    }

    /** 设置商品名称模糊查询条件 **/
    public void setNameLike(String nameLike) {
        this.nameLike = nameLike;
    }

    /** 判断标题是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getTitleIsNull() {
        return titleIsNull;
    }

    /** 设置标题空值查询(true:空值查询|false:非空值查询) **/
    public void setTitleIsNull(Boolean titleIsNull) {
        this.titleIsNull = titleIsNull;
    }

    /** 获取标题前模匹配条件 **/
    public String getTitleStarts() {
        return titleStarts;
    }

    /** 设置标题前模匹配条件 **/
    public void setTitleStarts(String titleStarts) {
        this.titleStarts = titleStarts;
    }

    /** 获取标题后模匹配条件 **/
    public String getTitleEnds() {
        return titleEnds;
    }

    /** 设置标题后模匹配条件 **/
    public void setTitleEnds(String titleEnds) {
        this.titleEnds = titleEnds;
    }

    /** 获取标题模糊查询条件 **/
    public String getTitleLike() {
        return titleLike;
    }

    /** 设置标题模糊查询条件 **/
    public void setTitleLike(String titleLike) {
        this.titleLike = titleLike;
    }

    /** 判断单位(个/件/台/袋等)是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getUnitIsNull() {
        return unitIsNull;
    }

    /** 设置单位(个/件/台/袋等)空值查询(true:空值查询|false:非空值查询) **/
    public void setUnitIsNull(Boolean unitIsNull) {
        this.unitIsNull = unitIsNull;
    }

    /** 获取单位(个/件/台/袋等)前模匹配条件 **/
    public String getUnitStarts() {
        return unitStarts;
    }

    /** 设置单位(个/件/台/袋等)前模匹配条件 **/
    public void setUnitStarts(String unitStarts) {
        this.unitStarts = unitStarts;
    }

    /** 获取单位(个/件/台/袋等)后模匹配条件 **/
    public String getUnitEnds() {
        return unitEnds;
    }

    /** 设置单位(个/件/台/袋等)后模匹配条件 **/
    public void setUnitEnds(String unitEnds) {
        this.unitEnds = unitEnds;
    }

    /** 获取单位(个/件/台/袋等)模糊查询条件 **/
    public String getUnitLike() {
        return unitLike;
    }

    /** 设置单位(个/件/台/袋等)模糊查询条件 **/
    public void setUnitLike(String unitLike) {
        this.unitLike = unitLike;
    }

    /** 判断图片信息是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getImageInfoIsNull() {
        return imageInfoIsNull;
    }

    /** 设置图片信息空值查询(true:空值查询|false:非空值查询) **/
    public void setImageInfoIsNull(Boolean imageInfoIsNull) {
        this.imageInfoIsNull = imageInfoIsNull;
    }

    /** 判断市场价格描述是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getMarketPriceTextIsNull() {
        return marketPriceTextIsNull;
    }

    /** 设置市场价格描述空值查询(true:空值查询|false:非空值查询) **/
    public void setMarketPriceTextIsNull(Boolean marketPriceTextIsNull) {
        this.marketPriceTextIsNull = marketPriceTextIsNull;
    }

    /** 获取市场价格描述前模匹配条件 **/
    public String getMarketPriceTextStarts() {
        return marketPriceTextStarts;
    }

    /** 设置市场价格描述前模匹配条件 **/
    public void setMarketPriceTextStarts(String marketPriceTextStarts) {
        this.marketPriceTextStarts = marketPriceTextStarts;
    }

    /** 获取市场价格描述后模匹配条件 **/
    public String getMarketPriceTextEnds() {
        return marketPriceTextEnds;
    }

    /** 设置市场价格描述后模匹配条件 **/
    public void setMarketPriceTextEnds(String marketPriceTextEnds) {
        this.marketPriceTextEnds = marketPriceTextEnds;
    }

    /** 获取市场价格描述模糊查询条件 **/
    public String getMarketPriceTextLike() {
        return marketPriceTextLike;
    }

    /** 设置市场价格描述模糊查询条件 **/
    public void setMarketPriceTextLike(String marketPriceTextLike) {
        this.marketPriceTextLike = marketPriceTextLike;
    }

    /** 判断销售价格描述(用于主商品展示的价格描述, 如80~99,199起)是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSalesPriceTextIsNull() {
        return salesPriceTextIsNull;
    }

    /** 设置销售价格描述(用于主商品展示的价格描述, 如80~99,199起)空值查询(true:空值查询|false:非空值查询) **/
    public void setSalesPriceTextIsNull(Boolean salesPriceTextIsNull) {
        this.salesPriceTextIsNull = salesPriceTextIsNull;
    }

    /** 获取销售价格描述(用于主商品展示的价格描述, 如80~99,199起)前模匹配条件 **/
    public String getSalesPriceTextStarts() {
        return salesPriceTextStarts;
    }

    /** 设置销售价格描述(用于主商品展示的价格描述, 如80~99,199起)前模匹配条件 **/
    public void setSalesPriceTextStarts(String salesPriceTextStarts) {
        this.salesPriceTextStarts = salesPriceTextStarts;
    }

    /** 获取销售价格描述(用于主商品展示的价格描述, 如80~99,199起)后模匹配条件 **/
    public String getSalesPriceTextEnds() {
        return salesPriceTextEnds;
    }

    /** 设置销售价格描述(用于主商品展示的价格描述, 如80~99,199起)后模匹配条件 **/
    public void setSalesPriceTextEnds(String salesPriceTextEnds) {
        this.salesPriceTextEnds = salesPriceTextEnds;
    }

    /** 获取销售价格描述(用于主商品展示的价格描述, 如80~99,199起)模糊查询条件 **/
    public String getSalesPriceTextLike() {
        return salesPriceTextLike;
    }

    /** 设置销售价格描述(用于主商品展示的价格描述, 如80~99,199起)模糊查询条件 **/
    public void setSalesPriceTextLike(String salesPriceTextLike) {
        this.salesPriceTextLike = salesPriceTextLike;
    }

    /** 判断价格值(实际售价)是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getPriceValueIsNull() {
        return priceValueIsNull;
    }

    /** 设置价格值(实际售价)空值查询(true:空值查询|false:非空值查询) **/
    public void setPriceValueIsNull(Boolean priceValueIsNull) {
        this.priceValueIsNull = priceValueIsNull;
    }

    /** 获取最小价格值(实际售价) **/
    public Double getPriceValueMin() {
        return priceValueMin;
    }

    /** 设置最小价格值(实际售价) **/
    public void setPriceValueMin(Double priceValueMin) {
        this.priceValueMin = priceValueMin;
    }

    /** 获取最大价格值(实际售价) **/
    public Double getPriceValueMax() {
        return priceValueMax;
    }

    /** 设置最大价格值(实际售价) **/
    public void setPriceValueMax(Double priceValueMax) {
        this.priceValueMax = priceValueMax;
    }

    /** 判断总销量是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSalesQuantityTotalIsNull() {
        return salesQuantityTotalIsNull;
    }

    /** 设置总销量空值查询(true:空值查询|false:非空值查询) **/
    public void setSalesQuantityTotalIsNull(Boolean salesQuantityTotalIsNull) {
        this.salesQuantityTotalIsNull = salesQuantityTotalIsNull;
    }

    /** 获取最小总销量 **/
    public Integer getSalesQuantityTotalMin() {
        return salesQuantityTotalMin;
    }

    /** 设置最小总销量 **/
    public void setSalesQuantityTotalMin(Integer salesQuantityTotalMin) {
        this.salesQuantityTotalMin = salesQuantityTotalMin;
    }

    /** 获取最大总销量 **/
    public Integer getSalesQuantityTotalMax() {
        return salesQuantityTotalMax;
    }

    /** 设置最大总销量 **/
    public void setSalesQuantityTotalMax(Integer salesQuantityTotalMax) {
        this.salesQuantityTotalMax = salesQuantityTotalMax;
    }

    /** 判断库存量是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getStockQuantityIsNull() {
        return stockQuantityIsNull;
    }

    /** 设置库存量空值查询(true:空值查询|false:非空值查询) **/
    public void setStockQuantityIsNull(Boolean stockQuantityIsNull) {
        this.stockQuantityIsNull = stockQuantityIsNull;
    }

    /** 获取最小库存量 **/
    public Integer getStockQuantityMin() {
        return stockQuantityMin;
    }

    /** 设置最小库存量 **/
    public void setStockQuantityMin(Integer stockQuantityMin) {
        this.stockQuantityMin = stockQuantityMin;
    }

    /** 获取最大库存量 **/
    public Integer getStockQuantityMax() {
        return stockQuantityMax;
    }

    /** 设置最大库存量 **/
    public void setStockQuantityMax(Integer stockQuantityMax) {
        this.stockQuantityMax = stockQuantityMax;
    }

    /** 判断摘要是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getIntroTextIsNull() {
        return introTextIsNull;
    }

    /** 设置摘要空值查询(true:空值查询|false:非空值查询) **/
    public void setIntroTextIsNull(Boolean introTextIsNull) {
        this.introTextIsNull = introTextIsNull;
    }

    /** 获取摘要前模匹配条件 **/
    public String getIntroTextStarts() {
        return introTextStarts;
    }

    /** 设置摘要前模匹配条件 **/
    public void setIntroTextStarts(String introTextStarts) {
        this.introTextStarts = introTextStarts;
    }

    /** 获取摘要后模匹配条件 **/
    public String getIntroTextEnds() {
        return introTextEnds;
    }

    /** 设置摘要后模匹配条件 **/
    public void setIntroTextEnds(String introTextEnds) {
        this.introTextEnds = introTextEnds;
    }

    /** 获取摘要模糊查询条件 **/
    public String getIntroTextLike() {
        return introTextLike;
    }

    /** 设置摘要模糊查询条件 **/
    public void setIntroTextLike(String introTextLike) {
        this.introTextLike = introTextLike;
    }

    /** 判断详细描述(富文本)是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getDescDetailsIsNull() {
        return descDetailsIsNull;
    }

    /** 设置详细描述(富文本)空值查询(true:空值查询|false:非空值查询) **/
    public void setDescDetailsIsNull(Boolean descDetailsIsNull) {
        this.descDetailsIsNull = descDetailsIsNull;
    }

    /** 获取详细描述(富文本)前模匹配条件 **/
    public String getDescDetailsStarts() {
        return descDetailsStarts;
    }

    /** 设置详细描述(富文本)前模匹配条件 **/
    public void setDescDetailsStarts(String descDetailsStarts) {
        this.descDetailsStarts = descDetailsStarts;
    }

    /** 获取详细描述(富文本)后模匹配条件 **/
    public String getDescDetailsEnds() {
        return descDetailsEnds;
    }

    /** 设置详细描述(富文本)后模匹配条件 **/
    public void setDescDetailsEnds(String descDetailsEnds) {
        this.descDetailsEnds = descDetailsEnds;
    }

    /** 获取详细描述(富文本)模糊查询条件 **/
    public String getDescDetailsLike() {
        return descDetailsLike;
    }

    /** 设置详细描述(富文本)模糊查询条件 **/
    public void setDescDetailsLike(String descDetailsLike) {
        this.descDetailsLike = descDetailsLike;
    }

    /** 判断排序号是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSortIndexIsNull() {
        return sortIndexIsNull;
    }

    /** 设置排序号空值查询(true:空值查询|false:非空值查询) **/
    public void setSortIndexIsNull(Boolean sortIndexIsNull) {
        this.sortIndexIsNull = sortIndexIsNull;
    }

    /** 获取最小排序号 **/
    public Long getSortIndexMin() {
        return sortIndexMin;
    }

    /** 设置最小排序号 **/
    public void setSortIndexMin(Long sortIndexMin) {
        this.sortIndexMin = sortIndexMin;
    }

    /** 获取最大排序号 **/
    public Long getSortIndexMax() {
        return sortIndexMax;
    }

    /** 设置最大排序号 **/
    public void setSortIndexMax(Long sortIndexMax) {
        this.sortIndexMax = sortIndexMax;
    }

    /** 判断规格分类ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSpecificKindIdIsNull() {
        return specificKindIdIsNull;
    }

    /** 设置规格分类ID空值查询(true:空值查询|false:非空值查询) **/
    public void setSpecificKindIdIsNull(Boolean specificKindIdIsNull) {
        this.specificKindIdIsNull = specificKindIdIsNull;
    }

    /** 获取规格分类ID前模匹配条件 **/
    public String getSpecificKindIdStarts() {
        return specificKindIdStarts;
    }

    /** 设置规格分类ID前模匹配条件 **/
    public void setSpecificKindIdStarts(String specificKindIdStarts) {
        this.specificKindIdStarts = specificKindIdStarts;
    }

    /** 获取规格分类ID后模匹配条件 **/
    public String getSpecificKindIdEnds() {
        return specificKindIdEnds;
    }

    /** 设置规格分类ID后模匹配条件 **/
    public void setSpecificKindIdEnds(String specificKindIdEnds) {
        this.specificKindIdEnds = specificKindIdEnds;
    }

    /** 获取规格分类ID模糊查询条件 **/
    public String getSpecificKindIdLike() {
        return specificKindIdLike;
    }

    /** 设置规格分类ID模糊查询条件 **/
    public void setSpecificKindIdLike(String specificKindIdLike) {
        this.specificKindIdLike = specificKindIdLike;
    }

    /** 判断规格数据列表是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSpecificKindDataIsNull() {
        return specificKindDataIsNull;
    }

    /** 设置规格数据列表空值查询(true:空值查询|false:非空值查询) **/
    public void setSpecificKindDataIsNull(Boolean specificKindDataIsNull) {
        this.specificKindDataIsNull = specificKindDataIsNull;
    }

    /** 判断单品实例列表是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSkuInstanceDataIsNull() {
        return skuInstanceDataIsNull;
    }

    /** 设置单品实例列表空值查询(true:空值查询|false:非空值查询) **/
    public void setSkuInstanceDataIsNull(Boolean skuInstanceDataIsNull) {
        this.skuInstanceDataIsNull = skuInstanceDataIsNull;
    }

    /** 判断创建人ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getCreatorIdIsNull() {
        return creatorIdIsNull;
    }

    /**
     * 设置创建人ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param creatorIdIsNull 创建人ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setCreatorIdIsNull(Boolean creatorIdIsNull) {
        this.creatorIdIsNull = creatorIdIsNull;
    }

    /** 获取创建人ID前模匹配条件 **/
    public String getCreatorIdStarts() {
        return creatorIdStarts;
    }

    /** 设置创建人ID前模匹配条件 **/
    public void setCreatorIdStarts(String creatorIdStarts) {
        this.creatorIdStarts = creatorIdStarts;
    }

    /** 获取创建人ID后模匹配条件 **/
    public String getCreatorIdEnds() {
        return creatorIdEnds;
    }

    /** 设置创建人ID后模匹配条件 **/
    public void setCreatorIdEnds(String creatorIdEnds) {
        this.creatorIdEnds = creatorIdEnds;
    }

    /** 获取创建人ID模糊查询条件 **/
    public String getCreatorIdLike() {
        return creatorIdLike;
    }

    /** 设置创建人ID模糊查询条件 **/
    public void setCreatorIdLike(String creatorIdLike) {
        this.creatorIdLike = creatorIdLike;
    }

    /** 获取最小创建时间 **/
    public Date getCreateTimeMin() {
        return createTimeMin;
    }

    /** 设置最小创建时间 **/
    public void setCreateTimeMin(Date createTimeMin) {
        this.createTimeMin = createTimeMin;
    }

    /** 获取最大创建时间 **/
    public Date getCreateTimeMax() {
        return createTimeMax;
    }

    /** 设置最大创建时间 **/
    public void setCreateTimeMax(Date createTimeMax) {
        this.createTimeMax = createTimeMax;
    }

    /** 获取最小创建时间 **/
    public Date getCreateTimeMinWithDay() {
        return createTimeMinWithDay;
    }

    /** 设置最小创建时间 **/
    public void setCreateTimeMinWithDay(Date createTimeMinWithDay) {
        this.createTimeMinWithDay = createTimeMinWithDay;
    }

    /** 获取最大创建时间 **/
    public Date getCreateTimeMaxWithDay() {
        return createTimeMaxWithDay;
    }

    /** 设置最大创建时间 **/
    public void setCreateTimeMaxWithDay(Date createTimeMaxWithDay) {
        this.createTimeMaxWithDay = createTimeMaxWithDay;
    }

    /** 判断发布时间是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getPublishTimeIsNull() {
        return publishTimeIsNull;
    }

    /** 设置发布时间空值查询(true:空值查询|false:非空值查询) **/
    public void setPublishTimeIsNull(Boolean publishTimeIsNull) {
        this.publishTimeIsNull = publishTimeIsNull;
    }

    /** 获取最小发布时间 **/
    public Date getPublishTimeMin() {
        return publishTimeMin;
    }

    /** 设置最小发布时间 **/
    public void setPublishTimeMin(Date publishTimeMin) {
        this.publishTimeMin = publishTimeMin;
    }

    /** 获取最大发布时间 **/
    public Date getPublishTimeMax() {
        return publishTimeMax;
    }

    /** 设置最大发布时间 **/
    public void setPublishTimeMax(Date publishTimeMax) {
        this.publishTimeMax = publishTimeMax;
    }

    /** 获取最小发布时间 **/
    public Date getPublishTimeMinWithDay() {
        return publishTimeMinWithDay;
    }

    /** 设置最小发布时间 **/
    public void setPublishTimeMinWithDay(Date publishTimeMinWithDay) {
        this.publishTimeMinWithDay = publishTimeMinWithDay;
    }

    /** 获取最大发布时间 **/
    public Date getPublishTimeMaxWithDay() {
        return publishTimeMaxWithDay;
    }

    /** 设置最大发布时间 **/
    public void setPublishTimeMaxWithDay(Date publishTimeMaxWithDay) {
        this.publishTimeMaxWithDay = publishTimeMaxWithDay;
    }

    /** 判断过期时间是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getExpireTimeIsNull() {
        return expireTimeIsNull;
    }

    /** 设置过期时间空值查询(true:空值查询|false:非空值查询) **/
    public void setExpireTimeIsNull(Boolean expireTimeIsNull) {
        this.expireTimeIsNull = expireTimeIsNull;
    }

    /** 获取最小过期时间 **/
    public Date getExpireTimeMin() {
        return expireTimeMin;
    }

    /** 设置最小过期时间 **/
    public void setExpireTimeMin(Date expireTimeMin) {
        this.expireTimeMin = expireTimeMin;
    }

    /** 获取最大过期时间 **/
    public Date getExpireTimeMax() {
        return expireTimeMax;
    }

    /** 设置最大过期时间 **/
    public void setExpireTimeMax(Date expireTimeMax) {
        this.expireTimeMax = expireTimeMax;
    }

    /** 获取最小过期时间 **/
    public Date getExpireTimeMinWithDay() {
        return expireTimeMinWithDay;
    }

    /** 设置最小过期时间 **/
    public void setExpireTimeMinWithDay(Date expireTimeMinWithDay) {
        this.expireTimeMinWithDay = expireTimeMinWithDay;
    }

    /** 获取最大过期时间 **/
    public Date getExpireTimeMaxWithDay() {
        return expireTimeMaxWithDay;
    }

    /** 设置最大过期时间 **/
    public void setExpireTimeMaxWithDay(Date expireTimeMaxWithDay) {
        this.expireTimeMaxWithDay = expireTimeMaxWithDay;
    }

    /** 判断选项是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getOptionsIsNull() {
        return optionsIsNull;
    }

    /** 设置选项空值查询(true:空值查询|false:非空值查询) **/
    public void setOptionsIsNull(Boolean optionsIsNull) {
        this.optionsIsNull = optionsIsNull;
    }

    /** 判断提交说明是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSubmitDescIsNull() {
        return submitDescIsNull;
    }

    /** 设置提交说明空值查询(true:空值查询|false:非空值查询) **/
    public void setSubmitDescIsNull(Boolean submitDescIsNull) {
        this.submitDescIsNull = submitDescIsNull;
    }

    /** 获取提交说明前模匹配条件 **/
    public String getSubmitDescStarts() {
        return submitDescStarts;
    }

    /** 设置提交说明前模匹配条件 **/
    public void setSubmitDescStarts(String submitDescStarts) {
        this.submitDescStarts = submitDescStarts;
    }

    /** 获取提交说明后模匹配条件 **/
    public String getSubmitDescEnds() {
        return submitDescEnds;
    }

    /** 设置提交说明后模匹配条件 **/
    public void setSubmitDescEnds(String submitDescEnds) {
        this.submitDescEnds = submitDescEnds;
    }

    /** 获取提交说明模糊查询条件 **/
    public String getSubmitDescLike() {
        return submitDescLike;
    }

    /** 设置提交说明模糊查询条件 **/
    public void setSubmitDescLike(String submitDescLike) {
        this.submitDescLike = submitDescLike;
    }

    /** 判断编辑序号(每编辑一次递增1)是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getEditIndexIsNull() {
        return editIndexIsNull;
    }

    /**
     * 设置编辑序号(每编辑一次递增1)空值查询(true:空值查询|false:非空值查询)
     *
     * @param editIndexIsNull 编辑序号(每编辑一次递增1)空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setEditIndexIsNull(Boolean editIndexIsNull) {
        this.editIndexIsNull = editIndexIsNull;
    }

    /** 获取最小编辑序号(每编辑一次递增1) **/
    public Integer getEditIndexMin() {
        return editIndexMin;
    }

    /** 设置最小编辑序号(每编辑一次递增1) **/
    public void setEditIndexMin(Integer editIndexMin) {
        this.editIndexMin = editIndexMin;
    }

    /** 获取最大编辑序号(每编辑一次递增1) **/
    public Integer getEditIndexMax() {
        return editIndexMax;
    }

    /** 设置最大编辑序号(每编辑一次递增1) **/
    public void setEditIndexMax(Integer editIndexMax) {
        this.editIndexMax = editIndexMax;
    }

    /** 判断商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getGoodsStateIsNull() {
        return goodsStateIsNull;
    }

    /**
     * 设置商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)空值查询(true:空值查询|false:非空值查询)
     *
     * @param goodsStateIsNull 商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setGoodsStateIsNull(Boolean goodsStateIsNull) {
        this.goodsStateIsNull = goodsStateIsNull;
    }

    /** 获取商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)列表 **/
    public List<GoodsState> getGoodsStates() {
        return goodsStates;
    }

    /** 设置商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)列表 **/
    public void setGoodsStates(List<GoodsState> goodsStates) {
        this.goodsStates = goodsStates;
    }

    /** 增加商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架) **/
    public void addGoodsState(GoodsState... goodsStates) {
        if (this.goodsStates == null) {
            this.goodsStates = new ArrayList<>();
        }
        this.goodsStates.addAll(Arrays.asList(goodsStates));
    }

    /** 判断版本状态(0.正本|1.副本|2.归档)是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getVersionStateIsNull() {
        return versionStateIsNull;
    }

    /**
     * 设置版本状态(0.正本|1.副本|2.归档)空值查询(true:空值查询|false:非空值查询)
     *
     * @param versionStateIsNull 版本状态(0.正本|1.副本|2.归档)空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setVersionStateIsNull(Boolean versionStateIsNull) {
        this.versionStateIsNull = versionStateIsNull;
    }

    /** 获取版本状态(0.正本|1.副本|2.归档)列表 **/
    public List<VersionState> getVersionStates() {
        return versionStates;
    }

    /** 设置版本状态(0.正本|1.副本|2.归档)列表 **/
    public void setVersionStates(List<VersionState> versionStates) {
        this.versionStates = versionStates;
    }

    /** 增加版本状态(0.正本|1.副本|2.归档) **/
    public void addVersionState(VersionState... versionStates) {
        if (this.versionStates == null) {
            this.versionStates = new ArrayList<>();
        }
        this.versionStates.addAll(Arrays.asList(versionStates));
    }

    /** 判断查询关键字是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getQueryKeywordsIsNull() {
        return queryKeywordsIsNull;
    }

    /** 设置查询关键字空值查询(true:空值查询|false:非空值查询) **/
    public void setQueryKeywordsIsNull(Boolean queryKeywordsIsNull) {
        this.queryKeywordsIsNull = queryKeywordsIsNull;
    }

    /** 获取查询关键字前模匹配条件 **/
    public String getQueryKeywordsStarts() {
        return queryKeywordsStarts;
    }

    /** 设置查询关键字前模匹配条件 **/
    public void setQueryKeywordsStarts(String queryKeywordsStarts) {
        this.queryKeywordsStarts = queryKeywordsStarts;
    }

    /** 获取查询关键字后模匹配条件 **/
    public String getQueryKeywordsEnds() {
        return queryKeywordsEnds;
    }

    /** 设置查询关键字后模匹配条件 **/
    public void setQueryKeywordsEnds(String queryKeywordsEnds) {
        this.queryKeywordsEnds = queryKeywordsEnds;
    }

    /** 获取查询关键字模糊查询条件 **/
    public String getQueryKeywordsLike() {
        return queryKeywordsLike;
    }

    /** 设置查询关键字模糊查询条件 **/
    public void setQueryKeywordsLike(String queryKeywordsLike) {
        this.queryKeywordsLike = queryKeywordsLike;
    }

    /** 获取按Keywords搜索的条件 **/
    public String getSearchByKeywords() {
        return searchByKeywords;
    }

    /** 设置按Keywords搜索的条件 **/
    public void setSearchByKeywords(String searchByKeywords) {
        this.searchByKeywords = searchByKeywords;
    }

}