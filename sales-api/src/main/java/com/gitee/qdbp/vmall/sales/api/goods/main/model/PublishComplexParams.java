package com.gitee.qdbp.vmall.sales.api.goods.main.model;

import java.util.List;

/**
 * 商品发布复合参数(带规格,有多个单品)
 *
 * @author zhaohuihua
 * @version 170812
 */
public class PublishComplexParams extends GoodsPriceBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 商品版本ID **/
    private String vid;
    /** 单品参数 **/
    private List<SkuPublishParams> skuPublishParams;
    /** 操作说明 **/
    private String description;

    /** 商品版本ID **/
    public String getVid() {
        return vid;
    }

    /** 商品版本ID **/
    public void setVid(String vid) {
        this.vid = vid;
    }

    /** 单品参数 **/
    public List<SkuPublishParams> getSkuPublishParams() {
        return skuPublishParams;
    }

    /** 单品参数 **/
    public void setSkuPublishParams(List<SkuPublishParams> skuPublishParams) {
        this.skuPublishParams = skuPublishParams;
    }

    /** 操作说明 **/
    public String getDescription() {
        return description;
    }

    /** 操作说明 **/
    public void setDescription(String description) {
        this.description = description;
    }

}
