package com.gitee.qdbp.vmall.sales.api.goods.manage.service;

import java.util.List;
import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.InventoryRealtimeBean;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.InventoryRealtimeWhere;

/**
 * 实时库存业务接口
 *
 * @author zhh
 * @version 170812
 */
public interface IInventoryRealtimeQueryer {

    InventoryRealtimeBean find(InventoryRealtimeWhere where) throws ServiceException;

    PageList<InventoryRealtimeBean> list(InventoryRealtimeWhere where, OrderPaging paging) throws ServiceException;

    /** 查询单品库存 **/
    int findBySkuId(String skuId) throws ServiceException;

    /** 查询单品库存 **/
    List<InventoryRealtimeBean> listBySkuId(List<String> skuIds) throws ServiceException;
}
