package com.gitee.qdbp.vmall.sales.api.goods.manage.model;

import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;

/**
 * 用户购物车更新类
 *
 * @author zhh
 * @version 180703
 */
@OperateTraces(target = "where")
@DataIsolation(target = "where")
public class ShoppingCartUpdate extends ShoppingCartBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 商家租户编号是否更新为空值 **/
    private Boolean tenantCodeToNull;

    /** 消费者用户ID是否更新为空值 **/
    private Boolean userIdToNull;

    /** 商品唯一ID是否更新为空值 **/
    private Boolean goodsUidToNull;

    /** 商品版本ID是否更新为空值 **/
    private Boolean goodsVidToNull;

    /** 单品ID是否更新为空值 **/
    private Boolean skuIdToNull;

    /** 购买数量是否更新为空值 **/
    private Boolean quantityToNull;
    /** 购买数量的增加值 **/
    private Integer quantityAdd;

    /** 是否选中是否更新为空值 **/
    private Boolean checkedToNull;

    /** 查询关键字是否更新为空值 **/
    private Boolean queryKeywordsToNull;

    /** 更新操作的条件 **/
    private ShoppingCartWhere where;

    /** 判断商家租户编号是否更新为空值 **/
    public Boolean isTenantCodeToNull() {
        return tenantCodeToNull;
    }

    /**
     * 设置商家租户编号是否更新为空值
     *
     * @param tenantCodeToNull 商家租户编号是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTenantCodeToNull(Boolean tenantCodeToNull) {
        this.tenantCodeToNull = tenantCodeToNull;
    }

    /** 判断消费者用户ID是否更新为空值 **/
    public Boolean isUserIdToNull() {
        return userIdToNull;
    }

    /**
     * 设置消费者用户ID是否更新为空值
     *
     * @param userIdToNull 消费者用户ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setUserIdToNull(Boolean userIdToNull) {
        this.userIdToNull = userIdToNull;
    }

    /** 判断商品唯一ID是否更新为空值 **/
    public Boolean isGoodsUidToNull() {
        return goodsUidToNull;
    }

    /**
     * 设置商品唯一ID是否更新为空值
     *
     * @param goodsUidToNull 商品唯一ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setGoodsUidToNull(Boolean goodsUidToNull) {
        this.goodsUidToNull = goodsUidToNull;
    }

    /** 判断商品版本ID是否更新为空值 **/
    public Boolean isGoodsVidToNull() {
        return goodsVidToNull;
    }

    /**
     * 设置商品版本ID是否更新为空值
     *
     * @param goodsVidToNull 商品版本ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setGoodsVidToNull(Boolean goodsVidToNull) {
        this.goodsVidToNull = goodsVidToNull;
    }

    /** 判断单品ID是否更新为空值 **/
    public Boolean isSkuIdToNull() {
        return skuIdToNull;
    }

    /**
     * 设置单品ID是否更新为空值
     *
     * @param skuIdToNull 单品ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSkuIdToNull(Boolean skuIdToNull) {
        this.skuIdToNull = skuIdToNull;
    }

    /** 判断购买数量是否更新为空值 **/
    public Boolean isQuantityToNull() {
        return quantityToNull;
    }

    /**
     * 设置购买数量是否更新为空值
     *
     * @param quantityToNull 购买数量是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setQuantityToNull(Boolean quantityToNull) {
        this.quantityToNull = quantityToNull;
    }

    /** 获取购买数量的增加值 **/
    public Integer getQuantityAdd() {
        return quantityAdd;
    }

    /** 设置购买数量的增加值 **/
    public void setQuantityAdd(Integer quantityAdd) {
        this.quantityAdd = quantityAdd;
    }

    /** 判断是否选中是否更新为空值 **/
    public Boolean isCheckedToNull() {
        return checkedToNull;
    }

    /**
     * 设置是否选中是否更新为空值
     *
     * @param checkedToNull 是否选中是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setCheckedToNull(Boolean checkedToNull) {
        this.checkedToNull = checkedToNull;
    }

    /** 判断查询关键字是否更新为空值 **/
    public Boolean isQueryKeywordsToNull() {
        return queryKeywordsToNull;
    }

    /** 设置查询关键字是否更新为空值 **/
    public void setQueryKeywordsToNull(Boolean queryKeywordsToNull) {
        this.queryKeywordsToNull = queryKeywordsToNull;
    }

    /** 获取更新操作的条件 **/
    public ShoppingCartWhere getWhere() {
        return where;
    }

    /** 获取更新操作的条件, force=是否强制返回非空条件 **/
    public ShoppingCartWhere getWhere(boolean force) {
        if (where == null && force) {
            where = new ShoppingCartWhere();
        }
        return where;
    }

    /** 设置更新操作的条件 **/
    public void setWhere(ShoppingCartWhere where) {
        this.where = where;
    }
}