package com.gitee.qdbp.vmall.sales.enums;

/**
 * SumType枚举类<br>
 * 汇总类型(0.总|1.年|2.月|3.周|4.日)
 *
 * @author zhh
 * @version 170812
 */
public enum SumType {

    /** 0.总 **/
    ALL,

    /** 1.年 **/
    YEAR,

    /** 2.月 **/
    MONTH,

    /** 3.周 **/
    WEEK,

    /** 4.日 **/
    DAY;
}
