package com.gitee.qdbp.vmall.sales.api.order.main.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import com.gitee.qdbp.base.enums.Gender;

/**
 * 订单配送信息查询类
 *
 * @author zhh
 * @version 180624
 */
public class OrderReceiverWhere extends OrderReceiverBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 主键列表 **/
    private List<String> ids;

    /** 主键前模匹配条件 **/
    private String idStarts;

    /** 主键后模匹配条件 **/
    private String idEnds;

    /** 主键模糊查询条件 **/
    private String idLike;

    /** 商家租户编号空值/非空值查询 **/
    private Boolean tenantCodeIsNull;

    /** 商家租户编号前模匹配条件 **/
    private String tenantCodeStarts;

    /** 商家租户编号后模匹配条件 **/
    private String tenantCodeEnds;

    /** 商家租户编号模糊查询条件 **/
    private String tenantCodeLike;

    /** 订单ID空值/非空值查询 **/
    private Boolean orderIdIsNull;

    /** 订单ID列表 **/
    private List<String> orderIds;

    /** 订单ID前模匹配条件 **/
    private String orderIdStarts;

    /** 订单ID后模匹配条件 **/
    private String orderIdEnds;

    /** 订单ID模糊查询条件 **/
    private String orderIdLike;

    /** 快递公司ID空值/非空值查询 **/
    private Boolean carrierCompanyIdIsNull;

    /** 快递公司ID前模匹配条件 **/
    private String carrierCompanyIdStarts;

    /** 快递公司ID后模匹配条件 **/
    private String carrierCompanyIdEnds;

    /** 快递公司ID模糊查询条件 **/
    private String carrierCompanyIdLike;

    /** 收件人姓名空值/非空值查询 **/
    private Boolean receiverNameIsNull;

    /** 收件人姓名前模匹配条件 **/
    private String receiverNameStarts;

    /** 收件人姓名后模匹配条件 **/
    private String receiverNameEnds;

    /** 收件人姓名模糊查询条件 **/
    private String receiverNameLike;

    /** 收件人性别(0.未知|1.男|2.女)空值/非空值查询 **/
    private Boolean receiverGenderIsNull;

    /** 收件人性别(0.未知|1.男|2.女)列表 **/
    private List<Gender> receiverGenders;

    /** 收件人手机号码空值/非空值查询 **/
    private Boolean receiverPhoneIsNull;

    /** 收件人手机号码前模匹配条件 **/
    private String receiverPhoneStarts;

    /** 收件人手机号码后模匹配条件 **/
    private String receiverPhoneEnds;

    /** 收件人手机号码模糊查询条件 **/
    private String receiverPhoneLike;

    /** 收件人城市(关联行政区划表)空值/非空值查询 **/
    private Boolean receiverAreaCodeIsNull;

    /** 收件人城市(关联行政区划表)前模匹配条件 **/
    private String receiverAreaCodeStarts;

    /** 收件人城市(关联行政区划表)后模匹配条件 **/
    private String receiverAreaCodeEnds;

    /** 收件人城市(关联行政区划表)模糊查询条件 **/
    private String receiverAreaCodeLike;

    /** 收件人地址空值/非空值查询 **/
    private Boolean receiverAddressIsNull;

    /** 收件人地址前模匹配条件 **/
    private String receiverAddressStarts;

    /** 收件人地址后模匹配条件 **/
    private String receiverAddressEnds;

    /** 收件人地址模糊查询条件 **/
    private String receiverAddressLike;

    /** 排序号(0为当前有效记录)空值/非空值查询 **/
    private Boolean sortIndexIsNull;

    /** 最小排序号(0为当前有效记录) **/
    private Integer sortIndexMin;

    /** 最大排序号(0为当前有效记录) **/
    private Integer sortIndexMax;

    /** 最小创建时间 **/
    private Date createTimeMin;

    /** 最大创建时间 **/
    private Date createTimeMax;

    /** 最小创建时间 **/
    private Date createTimeMinWithDay;

    /** 最大创建时间 **/
    private Date createTimeMaxWithDay;

    /** 选项空值/非空值查询 **/
    private Boolean optionsIsNull;

    /** 获取主键列表 **/
    public List<String> getIds() {
        return ids;
    }

    /** 设置主键列表 **/
    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    /** 增加主键 **/
    public void addId(String... ids) {
        if (this.ids == null) {
            this.ids = new ArrayList<>();
        }
        this.ids.addAll(Arrays.asList(ids));
    }

    /** 获取主键前模匹配条件 **/
    public String getIdStarts() {
        return idStarts;
    }

    /** 设置主键前模匹配条件 **/
    public void setIdStarts(String idStarts) {
        this.idStarts = idStarts;
    }

    /** 获取主键后模匹配条件 **/
    public String getIdEnds() {
        return idEnds;
    }

    /** 设置主键后模匹配条件 **/
    public void setIdEnds(String idEnds) {
        this.idEnds = idEnds;
    }

    /** 获取主键模糊查询条件 **/
    public String getIdLike() {
        return idLike;
    }

    /** 设置主键模糊查询条件 **/
    public void setIdLike(String idLike) {
        this.idLike = idLike;
    }

    /** 判断商家租户编号是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getTenantCodeIsNull() {
        return tenantCodeIsNull;
    }

    /**
     * 设置商家租户编号空值查询(true:空值查询|false:非空值查询)
     *
     * @param tenantCodeIsNull 商家租户编号空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTenantCodeIsNull(Boolean tenantCodeIsNull) {
        this.tenantCodeIsNull = tenantCodeIsNull;
    }

    /** 获取商家租户编号前模匹配条件 **/
    public String getTenantCodeStarts() {
        return tenantCodeStarts;
    }

    /** 设置商家租户编号前模匹配条件 **/
    public void setTenantCodeStarts(String tenantCodeStarts) {
        this.tenantCodeStarts = tenantCodeStarts;
    }

    /** 获取商家租户编号后模匹配条件 **/
    public String getTenantCodeEnds() {
        return tenantCodeEnds;
    }

    /** 设置商家租户编号后模匹配条件 **/
    public void setTenantCodeEnds(String tenantCodeEnds) {
        this.tenantCodeEnds = tenantCodeEnds;
    }

    /** 获取商家租户编号模糊查询条件 **/
    public String getTenantCodeLike() {
        return tenantCodeLike;
    }

    /** 设置商家租户编号模糊查询条件 **/
    public void setTenantCodeLike(String tenantCodeLike) {
        this.tenantCodeLike = tenantCodeLike;
    }

    /** 判断订单ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getOrderIdIsNull() {
        return orderIdIsNull;
    }

    /**
     * 设置订单ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param orderIdIsNull 订单ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setOrderIdIsNull(Boolean orderIdIsNull) {
        this.orderIdIsNull = orderIdIsNull;
    }

    /** 获取订单ID列表 **/
    public List<String> getOrderIds() {
        return orderIds;
    }

    /** 设置订单ID列表 **/
    public void setOrderIds(List<String> orderIds) {
        this.orderIds = orderIds;
    }

    /** 增加订单ID **/
    public void addOrderId(String... orderIds) {
        if (this.orderIds == null) {
            this.orderIds = new ArrayList<>();
        }
        this.orderIds.addAll(Arrays.asList(orderIds));
    }

    /** 获取订单ID前模匹配条件 **/
    public String getOrderIdStarts() {
        return orderIdStarts;
    }

    /** 设置订单ID前模匹配条件 **/
    public void setOrderIdStarts(String orderIdStarts) {
        this.orderIdStarts = orderIdStarts;
    }

    /** 获取订单ID后模匹配条件 **/
    public String getOrderIdEnds() {
        return orderIdEnds;
    }

    /** 设置订单ID后模匹配条件 **/
    public void setOrderIdEnds(String orderIdEnds) {
        this.orderIdEnds = orderIdEnds;
    }

    /** 获取订单ID模糊查询条件 **/
    public String getOrderIdLike() {
        return orderIdLike;
    }

    /** 设置订单ID模糊查询条件 **/
    public void setOrderIdLike(String orderIdLike) {
        this.orderIdLike = orderIdLike;
    }

    /** 判断快递公司ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getCarrierCompanyIdIsNull() {
        return carrierCompanyIdIsNull;
    }

    /** 设置快递公司ID空值查询(true:空值查询|false:非空值查询) **/
    public void setCarrierCompanyIdIsNull(Boolean carrierCompanyIdIsNull) {
        this.carrierCompanyIdIsNull = carrierCompanyIdIsNull;
    }

    /** 获取快递公司ID前模匹配条件 **/
    public String getCarrierCompanyIdStarts() {
        return carrierCompanyIdStarts;
    }

    /** 设置快递公司ID前模匹配条件 **/
    public void setCarrierCompanyIdStarts(String carrierCompanyIdStarts) {
        this.carrierCompanyIdStarts = carrierCompanyIdStarts;
    }

    /** 获取快递公司ID后模匹配条件 **/
    public String getCarrierCompanyIdEnds() {
        return carrierCompanyIdEnds;
    }

    /** 设置快递公司ID后模匹配条件 **/
    public void setCarrierCompanyIdEnds(String carrierCompanyIdEnds) {
        this.carrierCompanyIdEnds = carrierCompanyIdEnds;
    }

    /** 获取快递公司ID模糊查询条件 **/
    public String getCarrierCompanyIdLike() {
        return carrierCompanyIdLike;
    }

    /** 设置快递公司ID模糊查询条件 **/
    public void setCarrierCompanyIdLike(String carrierCompanyIdLike) {
        this.carrierCompanyIdLike = carrierCompanyIdLike;
    }

    /** 判断收件人姓名是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getReceiverNameIsNull() {
        return receiverNameIsNull;
    }

    /** 设置收件人姓名空值查询(true:空值查询|false:非空值查询) **/
    public void setReceiverNameIsNull(Boolean receiverNameIsNull) {
        this.receiverNameIsNull = receiverNameIsNull;
    }

    /** 获取收件人姓名前模匹配条件 **/
    public String getReceiverNameStarts() {
        return receiverNameStarts;
    }

    /** 设置收件人姓名前模匹配条件 **/
    public void setReceiverNameStarts(String receiverNameStarts) {
        this.receiverNameStarts = receiverNameStarts;
    }

    /** 获取收件人姓名后模匹配条件 **/
    public String getReceiverNameEnds() {
        return receiverNameEnds;
    }

    /** 设置收件人姓名后模匹配条件 **/
    public void setReceiverNameEnds(String receiverNameEnds) {
        this.receiverNameEnds = receiverNameEnds;
    }

    /** 获取收件人姓名模糊查询条件 **/
    public String getReceiverNameLike() {
        return receiverNameLike;
    }

    /** 设置收件人姓名模糊查询条件 **/
    public void setReceiverNameLike(String receiverNameLike) {
        this.receiverNameLike = receiverNameLike;
    }

    /** 判断收件人性别(0.未知|1.男|2.女)是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getReceiverGenderIsNull() {
        return receiverGenderIsNull;
    }

    /** 设置收件人性别(0.未知|1.男|2.女)空值查询(true:空值查询|false:非空值查询) **/
    public void setReceiverGenderIsNull(Boolean receiverGenderIsNull) {
        this.receiverGenderIsNull = receiverGenderIsNull;
    }

    /** 获取收件人性别(0.未知|1.男|2.女)列表 **/
    public List<Gender> getReceiverGenders() {
        return receiverGenders;
    }

    /** 设置收件人性别(0.未知|1.男|2.女)列表 **/
    public void setReceiverGenders(List<Gender> receiverGenders) {
        this.receiverGenders = receiverGenders;
    }

    /** 增加收件人性别(0.未知|1.男|2.女) **/
    public void addReceiverGender(Gender... receiverGenders) {
        if (this.receiverGenders == null) {
            this.receiverGenders = new ArrayList<>();
        }
        this.receiverGenders.addAll(Arrays.asList(receiverGenders));
    }

    /** 判断收件人手机号码是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getReceiverPhoneIsNull() {
        return receiverPhoneIsNull;
    }

    /** 设置收件人手机号码空值查询(true:空值查询|false:非空值查询) **/
    public void setReceiverPhoneIsNull(Boolean receiverPhoneIsNull) {
        this.receiverPhoneIsNull = receiverPhoneIsNull;
    }

    /** 获取收件人手机号码前模匹配条件 **/
    public String getReceiverPhoneStarts() {
        return receiverPhoneStarts;
    }

    /** 设置收件人手机号码前模匹配条件 **/
    public void setReceiverPhoneStarts(String receiverPhoneStarts) {
        this.receiverPhoneStarts = receiverPhoneStarts;
    }

    /** 获取收件人手机号码后模匹配条件 **/
    public String getReceiverPhoneEnds() {
        return receiverPhoneEnds;
    }

    /** 设置收件人手机号码后模匹配条件 **/
    public void setReceiverPhoneEnds(String receiverPhoneEnds) {
        this.receiverPhoneEnds = receiverPhoneEnds;
    }

    /** 获取收件人手机号码模糊查询条件 **/
    public String getReceiverPhoneLike() {
        return receiverPhoneLike;
    }

    /** 设置收件人手机号码模糊查询条件 **/
    public void setReceiverPhoneLike(String receiverPhoneLike) {
        this.receiverPhoneLike = receiverPhoneLike;
    }

    /** 判断收件人城市(关联行政区划表)是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getReceiverAreaCodeIsNull() {
        return receiverAreaCodeIsNull;
    }

    /** 设置收件人城市(关联行政区划表)空值查询(true:空值查询|false:非空值查询) **/
    public void setReceiverAreaCodeIsNull(Boolean receiverAreaCodeIsNull) {
        this.receiverAreaCodeIsNull = receiverAreaCodeIsNull;
    }

    /** 获取收件人城市(关联行政区划表)前模匹配条件 **/
    public String getReceiverAreaCodeStarts() {
        return receiverAreaCodeStarts;
    }

    /** 设置收件人城市(关联行政区划表)前模匹配条件 **/
    public void setReceiverAreaCodeStarts(String receiverAreaCodeStarts) {
        this.receiverAreaCodeStarts = receiverAreaCodeStarts;
    }

    /** 获取收件人城市(关联行政区划表)后模匹配条件 **/
    public String getReceiverAreaCodeEnds() {
        return receiverAreaCodeEnds;
    }

    /** 设置收件人城市(关联行政区划表)后模匹配条件 **/
    public void setReceiverAreaCodeEnds(String receiverAreaCodeEnds) {
        this.receiverAreaCodeEnds = receiverAreaCodeEnds;
    }

    /** 获取收件人城市(关联行政区划表)模糊查询条件 **/
    public String getReceiverAreaCodeLike() {
        return receiverAreaCodeLike;
    }

    /** 设置收件人城市(关联行政区划表)模糊查询条件 **/
    public void setReceiverAreaCodeLike(String receiverAreaCodeLike) {
        this.receiverAreaCodeLike = receiverAreaCodeLike;
    }

    /** 判断收件人地址是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getReceiverAddressIsNull() {
        return receiverAddressIsNull;
    }

    /** 设置收件人地址空值查询(true:空值查询|false:非空值查询) **/
    public void setReceiverAddressIsNull(Boolean receiverAddressIsNull) {
        this.receiverAddressIsNull = receiverAddressIsNull;
    }

    /** 获取收件人地址前模匹配条件 **/
    public String getReceiverAddressStarts() {
        return receiverAddressStarts;
    }

    /** 设置收件人地址前模匹配条件 **/
    public void setReceiverAddressStarts(String receiverAddressStarts) {
        this.receiverAddressStarts = receiverAddressStarts;
    }

    /** 获取收件人地址后模匹配条件 **/
    public String getReceiverAddressEnds() {
        return receiverAddressEnds;
    }

    /** 设置收件人地址后模匹配条件 **/
    public void setReceiverAddressEnds(String receiverAddressEnds) {
        this.receiverAddressEnds = receiverAddressEnds;
    }

    /** 获取收件人地址模糊查询条件 **/
    public String getReceiverAddressLike() {
        return receiverAddressLike;
    }

    /** 设置收件人地址模糊查询条件 **/
    public void setReceiverAddressLike(String receiverAddressLike) {
        this.receiverAddressLike = receiverAddressLike;
    }

    /** 判断排序号(0为当前有效记录)是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSortIndexIsNull() {
        return sortIndexIsNull;
    }

    /**
     * 设置排序号(0为当前有效记录)空值查询(true:空值查询|false:非空值查询)
     *
     * @param sortIndexIsNull 排序号(0为当前有效记录)空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSortIndexIsNull(Boolean sortIndexIsNull) {
        this.sortIndexIsNull = sortIndexIsNull;
    }

    /** 获取最小排序号(0为当前有效记录) **/
    public Integer getSortIndexMin() {
        return sortIndexMin;
    }

    /** 设置最小排序号(0为当前有效记录) **/
    public void setSortIndexMin(Integer sortIndexMin) {
        this.sortIndexMin = sortIndexMin;
    }

    /** 获取最大排序号(0为当前有效记录) **/
    public Integer getSortIndexMax() {
        return sortIndexMax;
    }

    /** 设置最大排序号(0为当前有效记录) **/
    public void setSortIndexMax(Integer sortIndexMax) {
        this.sortIndexMax = sortIndexMax;
    }

    /** 获取最小创建时间 **/
    public Date getCreateTimeMin() {
        return createTimeMin;
    }

    /** 设置最小创建时间 **/
    public void setCreateTimeMin(Date createTimeMin) {
        this.createTimeMin = createTimeMin;
    }

    /** 获取最大创建时间 **/
    public Date getCreateTimeMax() {
        return createTimeMax;
    }

    /** 设置最大创建时间 **/
    public void setCreateTimeMax(Date createTimeMax) {
        this.createTimeMax = createTimeMax;
    }

    /** 获取最小创建时间 **/
    public Date getCreateTimeMinWithDay() {
        return createTimeMinWithDay;
    }

    /** 设置最小创建时间 **/
    public void setCreateTimeMinWithDay(Date createTimeMinWithDay) {
        this.createTimeMinWithDay = createTimeMinWithDay;
    }

    /** 获取最大创建时间 **/
    public Date getCreateTimeMaxWithDay() {
        return createTimeMaxWithDay;
    }

    /** 设置最大创建时间 **/
    public void setCreateTimeMaxWithDay(Date createTimeMaxWithDay) {
        this.createTimeMaxWithDay = createTimeMaxWithDay;
    }

    /** 判断选项是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getOptionsIsNull() {
        return optionsIsNull;
    }

    /** 设置选项空值查询(true:空值查询|false:非空值查询) **/
    public void setOptionsIsNull(Boolean optionsIsNull) {
        this.optionsIsNull = optionsIsNull;
    }

}