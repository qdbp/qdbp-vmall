package com.gitee.qdbp.vmall.sales.api.goods.manage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.PartList;
import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;

/**
 * 实时库存实体类
 *
 * @author zhh
 * @version 180626
 */
@OperateTraces(id = "id")
@DataIsolation("tenantCode")
public class InventoryRealtimeBean implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 表名 **/
    public static final String TABLE = "vml_inventory_realtime";

    /** 主键 **/
    private String id;

    /** 租户编号 **/
    private String tenantCode;

    /** 商品唯一ID **/
    private String goodsUid;

    /** 单品ID **/
    private String skuId;

    /** 可售库存量 **/
    private Integer stockQuantity;

    /** 已售库存量(待发货) **/
    private Integer soldQuantity;

    /** 获取主键 **/
    public String getId() {
        return id;
    }

    /** 设置主键 **/
    public void setId(String id) {
        this.id = id;
    }

    /** 获取租户编号 **/
    public String getTenantCode() {
        return tenantCode;
    }

    /** 设置租户编号 **/
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /** 获取商品唯一ID **/
    public String getGoodsUid() {
        return goodsUid;
    }

    /** 设置商品唯一ID **/
    public void setGoodsUid(String goodsUid) {
        this.goodsUid = goodsUid;
    }

    /** 获取单品ID **/
    public String getSkuId() {
        return skuId;
    }

    /** 设置单品ID **/
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    /** 获取可售库存量 **/
    public Integer getStockQuantity() {
        return stockQuantity;
    }

    /** 设置可售库存量 **/
    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    /** 获取已售库存量(待发货) **/
    public Integer getSoldQuantity() {
        return soldQuantity;
    }

    /** 设置已售库存量(待发货) **/
    public void setSoldQuantity(Integer soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    /**
     * 将当前对象转换为子类对象
     *
     * @param clazz 目标类型
     * @return 目标对象
     */
    public <T extends InventoryRealtimeBean> T to(Class<T> clazz) {
        T instance;
        try {
            instance = clazz.newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to create " + clazz.getSimpleName() + " instance.", e);
        }

        instance.setId(this.getId()); // 主键
        instance.setTenantCode(this.getTenantCode()); // 租户编号
        instance.setGoodsUid(this.getGoodsUid()); // 商品唯一ID
        instance.setSkuId(this.getSkuId()); // 单品ID
        instance.setStockQuantity(this.getStockQuantity()); // 可售库存量
        instance.setSoldQuantity(this.getSoldQuantity()); // 已售库存量(待发货)
        return instance;
    }

    /**
     * 将InventoryRealtimeBean转换为子类对象
     *
     * @param beans 待转换的对象列表
     * @param clazz 目标类型
     * @param <T> InventoryRealtimeBean或子类
     * @param <C> T的子类
     * @return 目标对象列表
     */
    public static <T extends InventoryRealtimeBean, C extends T> List<C> to(List<T> beans, Class<C> clazz) {
        if (beans == null) {
            return null;
        }
        List<C> list;
        if (beans instanceof PartList) {
            PartList<C> partlist = new PartList<>();
            partlist.setTotal(((PartList<?>) beans).getTotal());
            list = partlist;
        } else {
            list = new ArrayList<>();
        }
        for (InventoryRealtimeBean bean : beans) {
            list.add(bean.to(clazz));
        }
        return list;
    }

}