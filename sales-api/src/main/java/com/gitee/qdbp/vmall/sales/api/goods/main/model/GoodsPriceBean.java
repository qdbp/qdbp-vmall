package com.gitee.qdbp.vmall.sales.api.goods.main.model;

import java.io.Serializable;

/**
 * 商品价格Bean
 *
 * @author zhaohuihua
 * @version 170814
 */
public class GoodsPriceBean implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 市场价格描述 **/
    private String marketPriceText;

    /** 销售价格描述(用于主商品展示的价格描述, 如80~99,199起) **/
    private String salesPriceText;

    /** 价格值(实际售价) **/
    private Double priceValue;

    /** 获取市场价格描述 **/
    public String getMarketPriceText() {
        return marketPriceText;
    }

    /** 设置市场价格描述 **/
    public void setMarketPriceText(String marketPriceText) {
        this.marketPriceText = marketPriceText;
    }

    /** 获取销售价格描述(用于主商品展示的价格描述, 如80~99,199起) **/
    public String getSalesPriceText() {
        return salesPriceText;
    }

    /** 设置销售价格描述(用于主商品展示的价格描述, 如80~99,199起) **/
    public void setSalesPriceText(String salesPriceText) {
        this.salesPriceText = salesPriceText;
    }

    /** 获取价格值(实际售价) **/
    public Double getPriceValue() {
        return priceValue;
    }

    /** 设置价格值(实际售价) **/
    public void setPriceValue(Double priceValue) {
        this.priceValue = priceValue;
    }
}
