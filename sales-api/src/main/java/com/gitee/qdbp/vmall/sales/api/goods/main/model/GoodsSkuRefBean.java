package com.gitee.qdbp.vmall.sales.api.goods.main.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.PartList;
import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;

/**
 * 商品单品关联实体类
 *
 * @author zhh
 * @version 180627
 */
@OperateTraces(id = "id")
@DataIsolation(enable = false)
public class GoodsSkuRefBean implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 表名 **/
    public static final String TABLE = "vml_goods_sku_ref";

    /** 主键 **/
    private String id;

    /** 商品唯一ID **/
    private String goodsUid;

    /** 单品ID **/
    private String skuId;

    /** 获取主键 **/
    public String getId() {
        return id;
    }

    /** 设置主键 **/
    public void setId(String id) {
        this.id = id;
    }

    /** 获取商品唯一ID **/
    public String getGoodsUid() {
        return goodsUid;
    }

    /** 设置商品唯一ID **/
    public void setGoodsUid(String goodsUid) {
        this.goodsUid = goodsUid;
    }

    /** 获取单品ID **/
    public String getSkuId() {
        return skuId;
    }

    /** 设置单品ID **/
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    /**
     * 将当前对象转换为子类对象
     *
     * @param clazz 目标类型
     * @return 目标对象
     */
    public <T extends GoodsSkuRefBean> T to(Class<T> clazz) {
        T instance;
        try {
            instance = clazz.newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to create " + clazz.getSimpleName() + " instance.", e);
        }

        instance.setId(this.getId()); // 主键
        instance.setGoodsUid(this.getGoodsUid()); // 商品唯一ID
        instance.setSkuId(this.getSkuId()); // 单品ID
        return instance;
    }

    /**
     * 将GoodsSkuRefBean转换为子类对象
     *
     * @param beans 待转换的对象列表
     * @param clazz 目标类型
     * @param <T> GoodsSkuRefBean或子类
     * @param <C> T的子类
     * @return 目标对象列表
     */
    public static <T extends GoodsSkuRefBean, C extends T> List<C> to(List<T> beans, Class<C> clazz) {
        if (beans == null) {
            return null;
        }
        List<C> list;
        if (beans instanceof PartList) {
            PartList<C> partlist = new PartList<>();
            partlist.setTotal(((PartList<?>) beans).getTotal());
            list = partlist;
        } else {
            list = new ArrayList<>();
        }
        for (GoodsSkuRefBean bean : beans) {
            list.add(bean.to(clazz));
        }
        return list;
    }

}