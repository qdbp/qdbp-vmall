package com.gitee.qdbp.vmall.sales.api.order.manage.model;

import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;

/**
 * 商品订单发货单更新类
 *
 * @author zhh
 * @version 180626
 */
@OperateTraces(target = "where")
@DataIsolation(target = "where")
public class OrderDeliveryUpdate extends OrderDeliveryBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 租户编号是否更新为空值 **/
    private Boolean tenantCodeToNull;

    /** 订单ID是否更新为空值 **/
    private Boolean orderIdToNull;

    /** 发货人ID(内部员工)是否更新为空值 **/
    private Boolean delivererIdToNull;

    /** 收件人姓名是否更新为空值 **/
    private Boolean receiverNameToNull;

    /** 收件人性别(0.未知|1.男|2.女)是否更新为空值 **/
    private Boolean receiverGenderToNull;

    /** 收件人手机号码是否更新为空值 **/
    private Boolean receiverPhoneToNull;

    /** 收件人城市(关联行政区划表)是否更新为空值 **/
    private Boolean receiverAreaCodeToNull;

    /** 收件人地址是否更新为空值 **/
    private Boolean receiverAddressToNull;

    /** 快递公司ID是否更新为空值 **/
    private Boolean carrierCompanyIdToNull;

    /** 快递单号是否更新为空值 **/
    private Boolean carrierVoucherCodeToNull;

    /** 取件快递员姓名是否更新为空值 **/
    private Boolean carrierClerkNameToNull;

    /** 取件快递员电话是否更新为空值 **/
    private Boolean carrierClerkPhoneToNull;

    /** 选项是否更新为空值 **/
    private Boolean optionsToNull;

    /** 更新操作的条件 **/
    private OrderDeliveryWhere where;

    /** 判断租户编号是否更新为空值 **/
    public Boolean isTenantCodeToNull() {
        return tenantCodeToNull;
    }

    /**
     * 设置租户编号是否更新为空值
     *
     * @param tenantCodeToNull 租户编号是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTenantCodeToNull(Boolean tenantCodeToNull) {
        this.tenantCodeToNull = tenantCodeToNull;
    }

    /** 判断订单ID是否更新为空值 **/
    public Boolean isOrderIdToNull() {
        return orderIdToNull;
    }

    /**
     * 设置订单ID是否更新为空值
     *
     * @param orderIdToNull 订单ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setOrderIdToNull(Boolean orderIdToNull) {
        this.orderIdToNull = orderIdToNull;
    }

    /** 判断发货人ID(内部员工)是否更新为空值 **/
    public Boolean isDelivererIdToNull() {
        return delivererIdToNull;
    }

    /** 设置发货人ID(内部员工)是否更新为空值 **/
    public void setDelivererIdToNull(Boolean delivererIdToNull) {
        this.delivererIdToNull = delivererIdToNull;
    }

    /** 判断收件人姓名是否更新为空值 **/
    public Boolean isReceiverNameToNull() {
        return receiverNameToNull;
    }

    /** 设置收件人姓名是否更新为空值 **/
    public void setReceiverNameToNull(Boolean receiverNameToNull) {
        this.receiverNameToNull = receiverNameToNull;
    }

    /** 判断收件人性别(0.未知|1.男|2.女)是否更新为空值 **/
    public Boolean isReceiverGenderToNull() {
        return receiverGenderToNull;
    }

    /** 设置收件人性别(0.未知|1.男|2.女)是否更新为空值 **/
    public void setReceiverGenderToNull(Boolean receiverGenderToNull) {
        this.receiverGenderToNull = receiverGenderToNull;
    }

    /** 判断收件人手机号码是否更新为空值 **/
    public Boolean isReceiverPhoneToNull() {
        return receiverPhoneToNull;
    }

    /** 设置收件人手机号码是否更新为空值 **/
    public void setReceiverPhoneToNull(Boolean receiverPhoneToNull) {
        this.receiverPhoneToNull = receiverPhoneToNull;
    }

    /** 判断收件人城市(关联行政区划表)是否更新为空值 **/
    public Boolean isReceiverAreaCodeToNull() {
        return receiverAreaCodeToNull;
    }

    /** 设置收件人城市(关联行政区划表)是否更新为空值 **/
    public void setReceiverAreaCodeToNull(Boolean receiverAreaCodeToNull) {
        this.receiverAreaCodeToNull = receiverAreaCodeToNull;
    }

    /** 判断收件人地址是否更新为空值 **/
    public Boolean isReceiverAddressToNull() {
        return receiverAddressToNull;
    }

    /** 设置收件人地址是否更新为空值 **/
    public void setReceiverAddressToNull(Boolean receiverAddressToNull) {
        this.receiverAddressToNull = receiverAddressToNull;
    }

    /** 判断快递公司ID是否更新为空值 **/
    public Boolean isCarrierCompanyIdToNull() {
        return carrierCompanyIdToNull;
    }

    /** 设置快递公司ID是否更新为空值 **/
    public void setCarrierCompanyIdToNull(Boolean carrierCompanyIdToNull) {
        this.carrierCompanyIdToNull = carrierCompanyIdToNull;
    }

    /** 判断快递单号是否更新为空值 **/
    public Boolean isCarrierVoucherCodeToNull() {
        return carrierVoucherCodeToNull;
    }

    /** 设置快递单号是否更新为空值 **/
    public void setCarrierVoucherCodeToNull(Boolean carrierVoucherCodeToNull) {
        this.carrierVoucherCodeToNull = carrierVoucherCodeToNull;
    }

    /** 判断取件快递员姓名是否更新为空值 **/
    public Boolean isCarrierClerkNameToNull() {
        return carrierClerkNameToNull;
    }

    /** 设置取件快递员姓名是否更新为空值 **/
    public void setCarrierClerkNameToNull(Boolean carrierClerkNameToNull) {
        this.carrierClerkNameToNull = carrierClerkNameToNull;
    }

    /** 判断取件快递员电话是否更新为空值 **/
    public Boolean isCarrierClerkPhoneToNull() {
        return carrierClerkPhoneToNull;
    }

    /** 设置取件快递员电话是否更新为空值 **/
    public void setCarrierClerkPhoneToNull(Boolean carrierClerkPhoneToNull) {
        this.carrierClerkPhoneToNull = carrierClerkPhoneToNull;
    }

    /** 判断选项是否更新为空值 **/
    public Boolean isOptionsToNull() {
        return optionsToNull;
    }

    /** 设置选项是否更新为空值 **/
    public void setOptionsToNull(Boolean optionsToNull) {
        this.optionsToNull = optionsToNull;
    }

    /** 获取更新操作的条件 **/
    public OrderDeliveryWhere getWhere() {
        return where;
    }

    /** 获取更新操作的条件, force=是否强制返回非空条件 **/
    public OrderDeliveryWhere getWhere(boolean force) {
        if (where == null && force) {
            where = new OrderDeliveryWhere();
        }
        return where;
    }

    /** 设置更新操作的条件 **/
    public void setWhere(OrderDeliveryWhere where) {
        this.where = where;
    }
}