package com.gitee.qdbp.vmall.sales.api.goods.main.service;

import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionBean;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionWhere;

/**
 * 商品版本信息业务接口
 *
 * @author zhh
 * @version 180627
 */
public interface IGoodsVersionQueryer {

    GoodsVersionBean findByVid(String vid) throws ServiceException;

    GoodsVersionBean findByUid(String uid) throws ServiceException;

    GoodsVersionBean find(GoodsVersionWhere where) throws ServiceException;

    PageList<GoodsVersionBean> list(GoodsVersionWhere where, OrderPaging paging) throws ServiceException;
}
