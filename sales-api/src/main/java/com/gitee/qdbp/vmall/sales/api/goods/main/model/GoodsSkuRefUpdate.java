package com.gitee.qdbp.vmall.sales.api.goods.main.model;

import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;

/**
 * 商品单品关联更新类
 *
 * @author zhh
 * @version 180627
 */
@OperateTraces(target = "where")
@DataIsolation(enable = false)
public class GoodsSkuRefUpdate extends GoodsSkuRefBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 商品唯一ID是否更新为空值 **/
    private Boolean goodsUidToNull;

    /** 单品ID是否更新为空值 **/
    private Boolean skuIdToNull;

    /** 更新操作的条件 **/
    private GoodsSkuRefWhere where;

    /** 判断商品唯一ID是否更新为空值 **/
    public Boolean isGoodsUidToNull() {
        return goodsUidToNull;
    }

    /**
     * 设置商品唯一ID是否更新为空值
     *
     * @param goodsUidToNull 商品唯一ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setGoodsUidToNull(Boolean goodsUidToNull) {
        this.goodsUidToNull = goodsUidToNull;
    }

    /** 判断单品ID是否更新为空值 **/
    public Boolean isSkuIdToNull() {
        return skuIdToNull;
    }

    /**
     * 设置单品ID是否更新为空值
     *
     * @param skuIdToNull 单品ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSkuIdToNull(Boolean skuIdToNull) {
        this.skuIdToNull = skuIdToNull;
    }

    /** 获取更新操作的条件 **/
    public GoodsSkuRefWhere getWhere() {
        return where;
    }

    /** 获取更新操作的条件, force=是否强制返回非空条件 **/
    public GoodsSkuRefWhere getWhere(boolean force) {
        if (where == null && force) {
            where = new GoodsSkuRefWhere();
        }
        return where;
    }

    /** 设置更新操作的条件 **/
    public void setWhere(GoodsSkuRefWhere where) {
        this.where = where;
    }
}