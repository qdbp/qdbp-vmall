package com.gitee.qdbp.vmall.sales.enums;

public enum MallScene {
    /** 商品分类 **/
    GOODS_CATEGORY("mall.enums.goods.category"),
    /** 商品品牌 **/
    GOODS_BRAND("mall.enums.goods.brand"),
    /** 快递公司 **/
    CARRIER_COMPANY("mall.enums.carrier.company");

    private String key;

    MallScene(String key) {
        this.key = key;
    }

    public String key() {
        return key;
    }
}
