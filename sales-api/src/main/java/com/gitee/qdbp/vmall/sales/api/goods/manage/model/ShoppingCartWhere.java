package com.gitee.qdbp.vmall.sales.api.goods.manage.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 用户购物车查询类
 *
 * @author zhh
 * @version 180705
 */
public class ShoppingCartWhere extends ShoppingCartBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 主键列表 **/
    private List<String> ids;

    /** 主键前模匹配条件 **/
    private String idStarts;

    /** 主键后模匹配条件 **/
    private String idEnds;

    /** 主键模糊查询条件 **/
    private String idLike;

    /** 商家租户编号空值/非空值查询 **/
    private Boolean tenantCodeIsNull;

    /** 商家租户编号前模匹配条件 **/
    private String tenantCodeStarts;

    /** 商家租户编号后模匹配条件 **/
    private String tenantCodeEnds;

    /** 商家租户编号模糊查询条件 **/
    private String tenantCodeLike;

    /** 消费者用户ID空值/非空值查询 **/
    private Boolean userIdIsNull;

    /** 消费者用户ID前模匹配条件 **/
    private String userIdStarts;

    /** 消费者用户ID后模匹配条件 **/
    private String userIdEnds;

    /** 消费者用户ID模糊查询条件 **/
    private String userIdLike;

    /** 商品唯一ID空值/非空值查询 **/
    private Boolean goodsUidIsNull;

    /** 商品唯一ID列表 **/
    private List<String> goodsUids;

    /** 商品唯一ID前模匹配条件 **/
    private String goodsUidStarts;

    /** 商品唯一ID后模匹配条件 **/
    private String goodsUidEnds;

    /** 商品唯一ID模糊查询条件 **/
    private String goodsUidLike;

    /** 商品版本ID空值/非空值查询 **/
    private Boolean goodsVidIsNull;

    /** 商品版本ID列表 **/
    private List<String> goodsVids;

    /** 商品版本ID前模匹配条件 **/
    private String goodsVidStarts;

    /** 商品版本ID后模匹配条件 **/
    private String goodsVidEnds;

    /** 商品版本ID模糊查询条件 **/
    private String goodsVidLike;

    /** 单品ID空值/非空值查询 **/
    private Boolean skuIdIsNull;

    /** 单品ID列表 **/
    private List<String> skuIds;

    /** 单品ID前模匹配条件 **/
    private String skuIdStarts;

    /** 单品ID后模匹配条件 **/
    private String skuIdEnds;

    /** 单品ID模糊查询条件 **/
    private String skuIdLike;

    /** 购买数量空值/非空值查询 **/
    private Boolean quantityIsNull;

    /** 最小购买数量 **/
    private Integer quantityMin;

    /** 最大购买数量 **/
    private Integer quantityMax;

    /** 是否选中空值/非空值查询 **/
    private Boolean checkedIsNull;

    /** 最小创建时间 **/
    private Date createTimeMin;

    /** 最大创建时间 **/
    private Date createTimeMax;

    /** 最小创建时间 **/
    private Date createTimeMinWithDay;

    /** 最大创建时间 **/
    private Date createTimeMaxWithDay;

    /** 查询关键字空值/非空值查询 **/
    private Boolean queryKeywordsIsNull;

    /** 查询关键字前模匹配条件 **/
    private String queryKeywordsStarts;

    /** 查询关键字后模匹配条件 **/
    private String queryKeywordsEnds;

    /** 查询关键字模糊查询条件 **/
    private String queryKeywordsLike;

    /** 按QueryKeywords搜索 **/
    private String searchByQueryKeywords;

    /** 获取主键列表 **/
    public List<String> getIds() {
        return ids;
    }

    /** 设置主键列表 **/
    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    /** 增加主键 **/
    public void addId(String... ids) {
        if (this.ids == null) {
            this.ids = new ArrayList<>();
        }
        this.ids.addAll(Arrays.asList(ids));
    }

    /** 获取主键前模匹配条件 **/
    public String getIdStarts() {
        return idStarts;
    }

    /** 设置主键前模匹配条件 **/
    public void setIdStarts(String idStarts) {
        this.idStarts = idStarts;
    }

    /** 获取主键后模匹配条件 **/
    public String getIdEnds() {
        return idEnds;
    }

    /** 设置主键后模匹配条件 **/
    public void setIdEnds(String idEnds) {
        this.idEnds = idEnds;
    }

    /** 获取主键模糊查询条件 **/
    public String getIdLike() {
        return idLike;
    }

    /** 设置主键模糊查询条件 **/
    public void setIdLike(String idLike) {
        this.idLike = idLike;
    }

    /** 判断商家租户编号是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getTenantCodeIsNull() {
        return tenantCodeIsNull;
    }

    /**
     * 设置商家租户编号空值查询(true:空值查询|false:非空值查询)
     *
     * @param tenantCodeIsNull 商家租户编号空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTenantCodeIsNull(Boolean tenantCodeIsNull) {
        this.tenantCodeIsNull = tenantCodeIsNull;
    }

    /** 获取商家租户编号前模匹配条件 **/
    public String getTenantCodeStarts() {
        return tenantCodeStarts;
    }

    /** 设置商家租户编号前模匹配条件 **/
    public void setTenantCodeStarts(String tenantCodeStarts) {
        this.tenantCodeStarts = tenantCodeStarts;
    }

    /** 获取商家租户编号后模匹配条件 **/
    public String getTenantCodeEnds() {
        return tenantCodeEnds;
    }

    /** 设置商家租户编号后模匹配条件 **/
    public void setTenantCodeEnds(String tenantCodeEnds) {
        this.tenantCodeEnds = tenantCodeEnds;
    }

    /** 获取商家租户编号模糊查询条件 **/
    public String getTenantCodeLike() {
        return tenantCodeLike;
    }

    /** 设置商家租户编号模糊查询条件 **/
    public void setTenantCodeLike(String tenantCodeLike) {
        this.tenantCodeLike = tenantCodeLike;
    }

    /** 判断消费者用户ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getUserIdIsNull() {
        return userIdIsNull;
    }

    /**
     * 设置消费者用户ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param userIdIsNull 消费者用户ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setUserIdIsNull(Boolean userIdIsNull) {
        this.userIdIsNull = userIdIsNull;
    }

    /** 获取消费者用户ID前模匹配条件 **/
    public String getUserIdStarts() {
        return userIdStarts;
    }

    /** 设置消费者用户ID前模匹配条件 **/
    public void setUserIdStarts(String userIdStarts) {
        this.userIdStarts = userIdStarts;
    }

    /** 获取消费者用户ID后模匹配条件 **/
    public String getUserIdEnds() {
        return userIdEnds;
    }

    /** 设置消费者用户ID后模匹配条件 **/
    public void setUserIdEnds(String userIdEnds) {
        this.userIdEnds = userIdEnds;
    }

    /** 获取消费者用户ID模糊查询条件 **/
    public String getUserIdLike() {
        return userIdLike;
    }

    /** 设置消费者用户ID模糊查询条件 **/
    public void setUserIdLike(String userIdLike) {
        this.userIdLike = userIdLike;
    }

    /** 判断商品唯一ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getGoodsUidIsNull() {
        return goodsUidIsNull;
    }

    /**
     * 设置商品唯一ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param goodsUidIsNull 商品唯一ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setGoodsUidIsNull(Boolean goodsUidIsNull) {
        this.goodsUidIsNull = goodsUidIsNull;
    }

    /** 获取商品唯一ID列表 **/
    public List<String> getGoodsUids() {
        return goodsUids;
    }

    /** 设置商品唯一ID列表 **/
    public void setGoodsUids(List<String> goodsUids) {
        this.goodsUids = goodsUids;
    }

    /** 增加商品唯一ID **/
    public void addGoodsUid(String... goodsUids) {
        if (this.goodsUids == null) {
            this.goodsUids = new ArrayList<>();
        }
        this.goodsUids.addAll(Arrays.asList(goodsUids));
    }

    /** 获取商品唯一ID前模匹配条件 **/
    public String getGoodsUidStarts() {
        return goodsUidStarts;
    }

    /** 设置商品唯一ID前模匹配条件 **/
    public void setGoodsUidStarts(String goodsUidStarts) {
        this.goodsUidStarts = goodsUidStarts;
    }

    /** 获取商品唯一ID后模匹配条件 **/
    public String getGoodsUidEnds() {
        return goodsUidEnds;
    }

    /** 设置商品唯一ID后模匹配条件 **/
    public void setGoodsUidEnds(String goodsUidEnds) {
        this.goodsUidEnds = goodsUidEnds;
    }

    /** 获取商品唯一ID模糊查询条件 **/
    public String getGoodsUidLike() {
        return goodsUidLike;
    }

    /** 设置商品唯一ID模糊查询条件 **/
    public void setGoodsUidLike(String goodsUidLike) {
        this.goodsUidLike = goodsUidLike;
    }

    /** 判断商品版本ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getGoodsVidIsNull() {
        return goodsVidIsNull;
    }

    /**
     * 设置商品版本ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param goodsVidIsNull 商品版本ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setGoodsVidIsNull(Boolean goodsVidIsNull) {
        this.goodsVidIsNull = goodsVidIsNull;
    }

    /** 获取商品版本ID列表 **/
    public List<String> getGoodsVids() {
        return goodsVids;
    }

    /** 设置商品版本ID列表 **/
    public void setGoodsVids(List<String> goodsVids) {
        this.goodsVids = goodsVids;
    }

    /** 增加商品版本ID **/
    public void addGoodsVid(String... goodsVids) {
        if (this.goodsVids == null) {
            this.goodsVids = new ArrayList<>();
        }
        this.goodsVids.addAll(Arrays.asList(goodsVids));
    }

    /** 获取商品版本ID前模匹配条件 **/
    public String getGoodsVidStarts() {
        return goodsVidStarts;
    }

    /** 设置商品版本ID前模匹配条件 **/
    public void setGoodsVidStarts(String goodsVidStarts) {
        this.goodsVidStarts = goodsVidStarts;
    }

    /** 获取商品版本ID后模匹配条件 **/
    public String getGoodsVidEnds() {
        return goodsVidEnds;
    }

    /** 设置商品版本ID后模匹配条件 **/
    public void setGoodsVidEnds(String goodsVidEnds) {
        this.goodsVidEnds = goodsVidEnds;
    }

    /** 获取商品版本ID模糊查询条件 **/
    public String getGoodsVidLike() {
        return goodsVidLike;
    }

    /** 设置商品版本ID模糊查询条件 **/
    public void setGoodsVidLike(String goodsVidLike) {
        this.goodsVidLike = goodsVidLike;
    }

    /** 判断单品ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSkuIdIsNull() {
        return skuIdIsNull;
    }

    /**
     * 设置单品ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param skuIdIsNull 单品ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSkuIdIsNull(Boolean skuIdIsNull) {
        this.skuIdIsNull = skuIdIsNull;
    }

    /** 获取单品ID列表 **/
    public List<String> getSkuIds() {
        return skuIds;
    }

    /** 设置单品ID列表 **/
    public void setSkuIds(List<String> skuIds) {
        this.skuIds = skuIds;
    }

    /** 增加单品ID **/
    public void addSkuId(String... skuIds) {
        if (this.skuIds == null) {
            this.skuIds = new ArrayList<>();
        }
        this.skuIds.addAll(Arrays.asList(skuIds));
    }

    /** 获取单品ID前模匹配条件 **/
    public String getSkuIdStarts() {
        return skuIdStarts;
    }

    /** 设置单品ID前模匹配条件 **/
    public void setSkuIdStarts(String skuIdStarts) {
        this.skuIdStarts = skuIdStarts;
    }

    /** 获取单品ID后模匹配条件 **/
    public String getSkuIdEnds() {
        return skuIdEnds;
    }

    /** 设置单品ID后模匹配条件 **/
    public void setSkuIdEnds(String skuIdEnds) {
        this.skuIdEnds = skuIdEnds;
    }

    /** 获取单品ID模糊查询条件 **/
    public String getSkuIdLike() {
        return skuIdLike;
    }

    /** 设置单品ID模糊查询条件 **/
    public void setSkuIdLike(String skuIdLike) {
        this.skuIdLike = skuIdLike;
    }

    /** 判断购买数量是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getQuantityIsNull() {
        return quantityIsNull;
    }

    /**
     * 设置购买数量空值查询(true:空值查询|false:非空值查询)
     *
     * @param quantityIsNull 购买数量空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setQuantityIsNull(Boolean quantityIsNull) {
        this.quantityIsNull = quantityIsNull;
    }

    /** 获取最小购买数量 **/
    public Integer getQuantityMin() {
        return quantityMin;
    }

    /** 设置最小购买数量 **/
    public void setQuantityMin(Integer quantityMin) {
        this.quantityMin = quantityMin;
    }

    /** 获取最大购买数量 **/
    public Integer getQuantityMax() {
        return quantityMax;
    }

    /** 设置最大购买数量 **/
    public void setQuantityMax(Integer quantityMax) {
        this.quantityMax = quantityMax;
    }

    /** 判断是否选中是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getCheckedIsNull() {
        return checkedIsNull;
    }

    /**
     * 设置是否选中空值查询(true:空值查询|false:非空值查询)
     *
     * @param checkedIsNull 是否选中空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setCheckedIsNull(Boolean checkedIsNull) {
        this.checkedIsNull = checkedIsNull;
    }

    /** 获取最小创建时间 **/
    public Date getCreateTimeMin() {
        return createTimeMin;
    }

    /** 设置最小创建时间 **/
    public void setCreateTimeMin(Date createTimeMin) {
        this.createTimeMin = createTimeMin;
    }

    /** 获取最大创建时间 **/
    public Date getCreateTimeMax() {
        return createTimeMax;
    }

    /** 设置最大创建时间 **/
    public void setCreateTimeMax(Date createTimeMax) {
        this.createTimeMax = createTimeMax;
    }

    /** 获取最小创建时间 **/
    public Date getCreateTimeMinWithDay() {
        return createTimeMinWithDay;
    }

    /** 设置最小创建时间 **/
    public void setCreateTimeMinWithDay(Date createTimeMinWithDay) {
        this.createTimeMinWithDay = createTimeMinWithDay;
    }

    /** 获取最大创建时间 **/
    public Date getCreateTimeMaxWithDay() {
        return createTimeMaxWithDay;
    }

    /** 设置最大创建时间 **/
    public void setCreateTimeMaxWithDay(Date createTimeMaxWithDay) {
        this.createTimeMaxWithDay = createTimeMaxWithDay;
    }

    /** 判断查询关键字是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getQueryKeywordsIsNull() {
        return queryKeywordsIsNull;
    }

    /** 设置查询关键字空值查询(true:空值查询|false:非空值查询) **/
    public void setQueryKeywordsIsNull(Boolean queryKeywordsIsNull) {
        this.queryKeywordsIsNull = queryKeywordsIsNull;
    }

    /** 获取查询关键字前模匹配条件 **/
    public String getQueryKeywordsStarts() {
        return queryKeywordsStarts;
    }

    /** 设置查询关键字前模匹配条件 **/
    public void setQueryKeywordsStarts(String queryKeywordsStarts) {
        this.queryKeywordsStarts = queryKeywordsStarts;
    }

    /** 获取查询关键字后模匹配条件 **/
    public String getQueryKeywordsEnds() {
        return queryKeywordsEnds;
    }

    /** 设置查询关键字后模匹配条件 **/
    public void setQueryKeywordsEnds(String queryKeywordsEnds) {
        this.queryKeywordsEnds = queryKeywordsEnds;
    }

    /** 获取查询关键字模糊查询条件 **/
    public String getQueryKeywordsLike() {
        return queryKeywordsLike;
    }

    /** 设置查询关键字模糊查询条件 **/
    public void setQueryKeywordsLike(String queryKeywordsLike) {
        this.queryKeywordsLike = queryKeywordsLike;
    }

    /** 获取按QueryKeywords搜索的条件 **/
    public String getSearchByQueryKeywords() {
        return searchByQueryKeywords;
    }

    /** 设置按QueryKeywords搜索的条件 **/
    public void setSearchByQueryKeywords(String searchByQueryKeywords) {
        this.searchByQueryKeywords = searchByQueryKeywords;
    }

}