package com.gitee.qdbp.vmall.sales.api.specific.model;

import java.io.Serializable;
import java.util.List;

/**
 * 商品规格分类
 *
 * @author zhaohuihua
 * @version 180625
 */
public class SpecificKind implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 规格分类ID **/
    private String id;
    /** 规格分类名称: 如颜色, 尺码 **/
    private String name;
    /** 规格项列表: 如颜色的红/黄/蓝; 尺码的XS/S/M/L/XL **/
    private List<SpecificItem> items;

    /** 规格分类ID **/
    public String getId() {
        return id;
    }

    /** 规格分类ID **/
    public void setId(String id) {
        this.id = id;
    }

    /** 规格名称: 如颜色, 尺码 **/
    public String getName() {
        return name;
    }

    /** 规格名称: 如颜色, 尺码 **/
    public void setName(String name) {
        this.name = name;
    }

    /** 规格项: 如颜色的红/黄/蓝; 尺码的XS/S/M/L/XL **/
    public List<SpecificItem> getItems() {
        return items;
    }

    /** 规格项: 如颜色的红/黄/蓝; 尺码的XS/S/M/L/XL **/
    public void setItems(List<SpecificItem> items) {
        this.items = items;
    }

}
