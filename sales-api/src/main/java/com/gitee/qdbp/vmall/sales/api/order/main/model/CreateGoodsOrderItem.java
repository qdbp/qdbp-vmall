package com.gitee.qdbp.vmall.sales.api.order.main.model;

import java.io.Serializable;

public class CreateGoodsOrderItem implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 商品版本ID(必填) **/
    private String goodsVid;
    /** 单品ID(复合商品必填) **/
    private String skuId;
    /** 购买数量 **/
    private Integer quantity;
    /** 折扣金额 **/
    private Double discountAmount;

    /** 商品版本ID(必填) **/
    public String getGoodsVid() {
        return goodsVid;
    }

    /** 商品版本ID(必填) **/
    public void setGoodsVid(String goodsVid) {
        this.goodsVid = goodsVid;
    }

    /** 单品ID(复合商品必填) **/
    public String getSkuId() {
        return skuId;
    }

    /** 单品ID(复合商品必填) **/
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    /** 购买数量 **/
    public Integer getQuantity() {
        return quantity;
    }

    /** 购买数量 **/
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /** 折扣金额 **/
    public Double getDiscountAmount() {
        return discountAmount;
    }

    /** 折扣金额 **/
    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

}
