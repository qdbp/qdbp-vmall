package com.gitee.qdbp.vmall.sales.api.goods.main.model;

/**
 * 修改价格的参数(单一商品,没有规格)
 *
 * @author zhaohuihua
 * @version 170814
 */
public class PriceSingleParams extends GoodsPriceBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 版本ID **/
    private String vid;

    /** 编辑序号 **/
    private Integer editIndex;

    /** 操作说明 **/
    private String description;

    /** 获取版本ID **/
    public String getVid() {
        return vid;
    }

    /** 设置版本ID **/
    public void setVid(String vid) {
        this.vid = vid;
    }

    /** 获取编辑序号 **/
    public Integer getEditIndex() {
        return editIndex;
    }

    /** 设置编辑序号 **/
    public void setEditIndex(Integer editIndex) {
        this.editIndex = editIndex;
    }

    /** 操作说明 **/
    public String getDescription() {
        return description;
    }

    /** 操作说明 **/
    public void setDescription(String description) {
        this.description = description;
    }
}
