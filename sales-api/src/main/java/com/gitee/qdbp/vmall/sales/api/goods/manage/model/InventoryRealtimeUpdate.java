package com.gitee.qdbp.vmall.sales.api.goods.manage.model;

import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;

/**
 * 实时库存更新类
 *
 * @author zhh
 * @version 180626
 */
@OperateTraces(target = "where")
@DataIsolation(target = "where")
public class InventoryRealtimeUpdate extends InventoryRealtimeBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 租户编号是否更新为空值 **/
    private Boolean tenantCodeToNull;

    /** 商品唯一ID是否更新为空值 **/
    private Boolean goodsUidToNull;

    /** 单品ID是否更新为空值 **/
    private Boolean skuIdToNull;

    /** 可售库存量是否更新为空值 **/
    private Boolean stockQuantityToNull;
    /** 可售库存量的增加值 **/
    private Integer stockQuantityAdd;

    /** 已售库存量(待发货)是否更新为空值 **/
    private Boolean soldQuantityToNull;
    /** 已售库存量(待发货)的增加值 **/
    private Integer soldQuantityAdd;

    /** 更新操作的条件 **/
    private InventoryRealtimeWhere where;

    /** 判断租户编号是否更新为空值 **/
    public Boolean isTenantCodeToNull() {
        return tenantCodeToNull;
    }

    /**
     * 设置租户编号是否更新为空值
     *
     * @param tenantCodeToNull 租户编号是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTenantCodeToNull(Boolean tenantCodeToNull) {
        this.tenantCodeToNull = tenantCodeToNull;
    }

    /** 判断商品唯一ID是否更新为空值 **/
    public Boolean isGoodsUidToNull() {
        return goodsUidToNull;
    }

    /**
     * 设置商品唯一ID是否更新为空值
     *
     * @param goodsUidToNull 商品唯一ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setGoodsUidToNull(Boolean goodsUidToNull) {
        this.goodsUidToNull = goodsUidToNull;
    }

    /** 判断单品ID是否更新为空值 **/
    public Boolean isSkuIdToNull() {
        return skuIdToNull;
    }

    /**
     * 设置单品ID是否更新为空值
     *
     * @param skuIdToNull 单品ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSkuIdToNull(Boolean skuIdToNull) {
        this.skuIdToNull = skuIdToNull;
    }

    /** 判断可售库存量是否更新为空值 **/
    public Boolean isStockQuantityToNull() {
        return stockQuantityToNull;
    }

    /**
     * 设置可售库存量是否更新为空值
     *
     * @param stockQuantityToNull 可售库存量是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setStockQuantityToNull(Boolean stockQuantityToNull) {
        this.stockQuantityToNull = stockQuantityToNull;
    }

    /** 获取可售库存量的增加值 **/
    public Integer getStockQuantityAdd() {
        return stockQuantityAdd;
    }

    /** 设置可售库存量的增加值 **/
    public void setStockQuantityAdd(Integer stockQuantityAdd) {
        this.stockQuantityAdd = stockQuantityAdd;
    }

    /** 判断已售库存量(待发货)是否更新为空值 **/
    public Boolean isSoldQuantityToNull() {
        return soldQuantityToNull;
    }

    /**
     * 设置已售库存量(待发货)是否更新为空值
     *
     * @param soldQuantityToNull 已售库存量(待发货)是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSoldQuantityToNull(Boolean soldQuantityToNull) {
        this.soldQuantityToNull = soldQuantityToNull;
    }

    /** 获取已售库存量(待发货)的增加值 **/
    public Integer getSoldQuantityAdd() {
        return soldQuantityAdd;
    }

    /** 设置已售库存量(待发货)的增加值 **/
    public void setSoldQuantityAdd(Integer soldQuantityAdd) {
        this.soldQuantityAdd = soldQuantityAdd;
    }

    /** 获取更新操作的条件 **/
    public InventoryRealtimeWhere getWhere() {
        return where;
    }

    /** 获取更新操作的条件, force=是否强制返回非空条件 **/
    public InventoryRealtimeWhere getWhere(boolean force) {
        if (where == null && force) {
            where = new InventoryRealtimeWhere();
        }
        return where;
    }

    /** 设置更新操作的条件 **/
    public void setWhere(InventoryRealtimeWhere where) {
        this.where = where;
    }
}