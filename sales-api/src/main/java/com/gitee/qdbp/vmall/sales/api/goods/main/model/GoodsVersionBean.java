package com.gitee.qdbp.vmall.sales.api.goods.main.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.PartList;
import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;
import com.gitee.qdbp.base.enums.DataState;
import com.gitee.qdbp.base.system.model.CreatorIdAware;
import com.gitee.qdbp.general.common.enums.VersionState;
import com.gitee.qdbp.vmall.sales.api.specific.model.SkuInstance;
import com.gitee.qdbp.vmall.sales.api.specific.model.SpecificKind;
import com.gitee.qdbp.vmall.sales.enums.GoodsState;

/**
 * 商品版本信息实体类<br>
 * 商品分为单一商品和复合商品: 单一商品没有规格, SkuId等于GoodsUid; 复合商品有规格, 并由这些规格组成多个sku单品.
 *
 * @author zhh
 * @version 180626
 */
@OperateTraces(id = "vid")
@DataIsolation("tenantCode")
public class GoodsVersionBean implements CreatorIdAware, Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 表名 **/
    public static final String TABLE = "vml_goods_version";

    /** 商品版本ID **/
    private String vid;

    /** 商品唯一ID **/
    private String uid;

    /** 租户编号 **/
    private String tenantCode;

    /** 部门编号 **/
    private String deptCode;

    /** 分类编号 **/
    private String categoryCode;

    /** 品牌编号 **/
    private String brandCode;

    /** 型号 **/
    private String modelNumber;

    /** 商品名称 **/
    private String name;

    /** 标题 **/
    private String title;

    /** 单位(个/件/台/袋等) **/
    private String unit;

    /** 图片信息 **/
    private GoodsImages imageInfo;

    /** 市场价格描述 **/
    private String marketPriceText;

    /** 销售价格描述(用于主商品展示的价格描述, 如80~99,199起) **/
    private String salesPriceText;

    /** 价格值(实际售价) **/
    private Double priceValue;

    /** 总销量 **/
    private Integer salesQuantityTotal;

    /** 库存量 **/
    private Integer stockQuantity;

    /** 摘要 **/
    private String introText;

    /** 详细描述(富文本) **/
    private String descDetails;

    /** 排序号 **/
    private Long sortIndex;

    /** 规格分类ID **/
    private String specificKindId;

    /** 规格数据列表 **/
    private List<SpecificKind> specificKindData;

    /** 单品实例列表 **/
    private List<SkuInstance> skuInstanceData;

    /** 创建人ID **/
    private String creatorId;

    /** 创建时间 **/
    private Date createTime;

    /** 发布时间 **/
    private Date publishTime;

    /** 过期时间 **/
    private Date expireTime;

    /** 选项 **/
    private GoodsOptions options;

    /** 提交说明 **/
    private String submitDesc;

    /** 编辑序号(每编辑一次递增1) **/
    private Integer editIndex;

    /** 商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架) **/
    private GoodsState goodsState;

    /** 版本状态(0.正本|1.副本|2.归档) **/
    private VersionState versionState;

    /** 查询关键字 **/
    private String queryKeywords;

    /** 数据状态:0为正常|其他为删除 **/
    private DataState dataState;

    /** 获取商品版本ID **/
    public String getVid() {
        return vid;
    }

    /** 设置商品版本ID **/
    public void setVid(String vid) {
        this.vid = vid;
    }

    /** 获取商品唯一ID **/
    public String getUid() {
        return uid;
    }

    /** 设置商品唯一ID **/
    public void setUid(String uid) {
        this.uid = uid;
    }

    /** 获取租户编号 **/
    public String getTenantCode() {
        return tenantCode;
    }

    /** 设置租户编号 **/
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /** 获取部门编号 **/
    public String getDeptCode() {
        return deptCode;
    }

    /** 设置部门编号 **/
    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    /** 获取分类编号 **/
    public String getCategoryCode() {
        return categoryCode;
    }

    /** 设置分类编号 **/
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    /** 获取品牌编号 **/
    public String getBrandCode() {
        return brandCode;
    }

    /** 设置品牌编号 **/
    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    /** 获取型号 **/
    public String getModelNumber() {
        return modelNumber;
    }

    /** 设置型号 **/
    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    /** 获取商品名称 **/
    public String getName() {
        return name;
    }

    /** 设置商品名称 **/
    public void setName(String name) {
        this.name = name;
    }

    /** 获取标题 **/
    public String getTitle() {
        return title;
    }

    /** 设置标题 **/
    public void setTitle(String title) {
        this.title = title;
    }

    /** 获取单位(个/件/台/袋等) **/
    public String getUnit() {
        return unit;
    }

    /** 设置单位(个/件/台/袋等) **/
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /** 获取图片信息 **/
    public GoodsImages getImageInfo() {
        return imageInfo;
    }

    /** 获取图片信息, force=是否强制返回非空对象 **/
    public GoodsImages getImageInfo(boolean force) {
        if (imageInfo == null && force) {
            imageInfo = new GoodsImages();
        }
        return imageInfo;
    }

    /** 设置图片信息 **/
    public void setImageInfo(GoodsImages imageInfo) {
        this.imageInfo = imageInfo;
    }

    /** 获取市场价格描述 **/
    public String getMarketPriceText() {
        return marketPriceText;
    }

    /** 设置市场价格描述 **/
    public void setMarketPriceText(String marketPriceText) {
        this.marketPriceText = marketPriceText;
    }

    /** 获取销售价格描述(用于主商品展示的价格描述, 如80~99,199起) **/
    public String getSalesPriceText() {
        return salesPriceText;
    }

    /** 设置销售价格描述(用于主商品展示的价格描述, 如80~99,199起) **/
    public void setSalesPriceText(String salesPriceText) {
        this.salesPriceText = salesPriceText;
    }

    /** 获取价格值(实际售价) **/
    public Double getPriceValue() {
        return priceValue;
    }

    /** 设置价格值(实际售价) **/
    public void setPriceValue(Double priceValue) {
        this.priceValue = priceValue;
    }

    /** 获取总销量 **/
    public Integer getSalesQuantityTotal() {
        return salesQuantityTotal;
    }

    /** 设置总销量 **/
    public void setSalesQuantityTotal(Integer salesQuantityTotal) {
        this.salesQuantityTotal = salesQuantityTotal;
    }

    /** 获取库存量 **/
    public Integer getStockQuantity() {
        return stockQuantity;
    }

    /** 设置库存量 **/
    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    /** 获取摘要 **/
    public String getIntroText() {
        return introText;
    }

    /** 设置摘要 **/
    public void setIntroText(String introText) {
        this.introText = introText;
    }

    /** 获取详细描述(富文本) **/
    public String getDescDetails() {
        return descDetails;
    }

    /** 设置详细描述(富文本) **/
    public void setDescDetails(String descDetails) {
        this.descDetails = descDetails;
    }

    /** 获取排序号 **/
    public Long getSortIndex() {
        return sortIndex;
    }

    /** 设置排序号 **/
    public void setSortIndex(Long sortIndex) {
        this.sortIndex = sortIndex;
    }

    /** 获取规格分类ID **/
    public String getSpecificKindId() {
        return specificKindId;
    }

    /** 设置规格分类ID **/
    public void setSpecificKindId(String specificKindId) {
        this.specificKindId = specificKindId;
    }

    /** 获取规格数据列表 **/
    public List<SpecificKind> getSpecificKindData() {
        return specificKindData;
    }

    /** 获取规格数据列表, force=是否强制返回非空对象 **/
    public List<SpecificKind> getSpecificKindData(boolean force) {
        if (specificKindData == null && force) {
            specificKindData = new ArrayList<>();
        }
        return specificKindData;
    }

    /** 设置规格数据列表 **/
    public void setSpecificKindData(List<SpecificKind> specificKindData) {
        this.specificKindData = specificKindData;
    }

    /** 获取单品实例列表 **/
    public List<SkuInstance> getSkuInstanceData() {
        return skuInstanceData;
    }

    /** 获取单品实例列表, force=是否强制返回非空对象 **/
    public List<SkuInstance> getSkuInstanceData(boolean force) {
        if (skuInstanceData == null && force) {
            skuInstanceData = new ArrayList<>();
        }
        return skuInstanceData;
    }

    /** 设置单品实例列表 **/
    public void setSkuInstanceData(List<SkuInstance> skuInstanceData) {
        this.skuInstanceData = skuInstanceData;
    }

    /** 获取创建人ID **/
    public String getCreatorId() {
        return creatorId;
    }

    /** 设置创建人ID **/
    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    /** 获取创建时间 **/
    public Date getCreateTime() {
        return createTime;
    }

    /** 设置创建时间 **/
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /** 获取发布时间 **/
    public Date getPublishTime() {
        return publishTime;
    }

    /** 设置发布时间 **/
    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    /** 获取过期时间 **/
    public Date getExpireTime() {
        return expireTime;
    }

    /** 设置过期时间 **/
    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    /** 获取选项 **/
    public GoodsOptions getOptions() {
        return options;
    }

    /** 获取选项, force=是否强制返回非空对象 **/
    public GoodsOptions getOptions(boolean force) {
        if (options == null && force) {
            options = new GoodsOptions();
        }
        return options;
    }

    /** 设置选项 **/
    public void setOptions(GoodsOptions options) {
        this.options = options;
    }

    /** 获取提交说明 **/
    public String getSubmitDesc() {
        return submitDesc;
    }

    /** 设置提交说明 **/
    public void setSubmitDesc(String submitDesc) {
        this.submitDesc = submitDesc;
    }

    /** 获取编辑序号(每编辑一次递增1) **/
    public Integer getEditIndex() {
        return editIndex;
    }

    /** 设置编辑序号(每编辑一次递增1) **/
    public void setEditIndex(Integer editIndex) {
        this.editIndex = editIndex;
    }

    /** 获取商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架) **/
    public GoodsState getGoodsState() {
        return goodsState;
    }

    /** 设置商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架) **/
    public void setGoodsState(GoodsState goodsState) {
        this.goodsState = goodsState;
    }

    /** 获取版本状态(0.正本|1.副本|2.归档) **/
    public VersionState getVersionState() {
        return versionState;
    }

    /** 设置版本状态(0.正本|1.副本|2.归档) **/
    public void setVersionState(VersionState versionState) {
        this.versionState = versionState;
    }

    /** 获取查询关键字 **/
    public String getQueryKeywords() {
        return queryKeywords;
    }

    /** 设置查询关键字 **/
    public void setQueryKeywords(String queryKeywords) {
        this.queryKeywords = queryKeywords;
    }

    /** 获取数据状态:0为正常|其他为删除 **/
    public DataState getDataState() {
        return dataState;
    }

    /** 设置数据状态:0为正常|其他为删除 **/
    public void setDataState(DataState dataState) {
        this.dataState = dataState;
    }

    /**
     * 将当前对象转换为子类对象
     *
     * @param clazz 目标类型
     * @return 目标对象
     */
    public <T extends GoodsVersionBean> T to(Class<T> clazz) {
        T instance;
        try {
            instance = clazz.newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to create " + clazz.getSimpleName() + " instance.", e);
        }

        instance.setVid(this.getVid()); // 商品版本ID
        instance.setUid(this.getUid()); // 商品唯一ID
        instance.setTenantCode(this.getTenantCode()); // 租户编号
        instance.setDeptCode(this.getDeptCode()); // 部门编号
        instance.setCategoryCode(this.getCategoryCode()); // 分类编号
        instance.setBrandCode(this.getBrandCode()); // 品牌编号
        instance.setModelNumber(this.getModelNumber()); // 型号
        instance.setName(this.getName()); // 商品名称
        instance.setTitle(this.getTitle()); // 标题
        instance.setUnit(this.getUnit()); // 单位(个/件/台/袋等)
        instance.setImageInfo(this.getImageInfo()); // 图片信息
        instance.setMarketPriceText(this.getMarketPriceText()); // 市场价格描述
        instance.setSalesPriceText(this.getSalesPriceText()); // 销售价格描述(用于主商品展示的价格描述, 如80~99,199起)
        instance.setPriceValue(this.getPriceValue()); // 价格值(实际售价)
        instance.setSalesQuantityTotal(this.getSalesQuantityTotal()); // 总销量
        instance.setStockQuantity(this.getStockQuantity()); // 库存量
        instance.setIntroText(this.getIntroText()); // 摘要
        instance.setDescDetails(this.getDescDetails()); // 详细描述(富文本)
        instance.setSortIndex(this.getSortIndex()); // 排序号
        instance.setSpecificKindId(this.getSpecificKindId()); // 规格分类ID
        instance.setSpecificKindData(this.getSpecificKindData()); // 规格数据列表
        instance.setSkuInstanceData(this.getSkuInstanceData()); // 单品实例列表
        instance.setCreatorId(this.getCreatorId()); // 创建人ID
        instance.setCreateTime(this.getCreateTime()); // 创建时间
        instance.setPublishTime(this.getPublishTime()); // 发布时间
        instance.setExpireTime(this.getExpireTime()); // 过期时间
        instance.setOptions(this.getOptions()); // 选项
        instance.setSubmitDesc(this.getSubmitDesc()); // 提交说明
        instance.setEditIndex(this.getEditIndex()); // 编辑序号(每编辑一次递增1)
        instance.setGoodsState(this.getGoodsState()); // 商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)
        instance.setVersionState(this.getVersionState()); // 版本状态(0.正本|1.副本|2.归档)
        instance.setQueryKeywords(this.getQueryKeywords()); // 查询关键字
        instance.setDataState(this.getDataState()); // 数据状态:0为正常|其他为删除
        return instance;
    }

    /**
     * 将GoodsVersionBean转换为子类对象
     *
     * @param beans 待转换的对象列表
     * @param clazz 目标类型
     * @param <T> GoodsVersionBean或子类
     * @param <C> T的子类
     * @return 目标对象列表
     */
    public static <T extends GoodsVersionBean, C extends T> List<C> to(List<T> beans, Class<C> clazz) {
        if (beans == null) {
            return null;
        }
        List<C> list;
        if (beans instanceof PartList) {
            PartList<C> partlist = new PartList<>();
            partlist.setTotal(((PartList<?>) beans).getTotal());
            list = partlist;
        } else {
            list = new ArrayList<>();
        }
        for (GoodsVersionBean bean : beans) {
            list.add(bean.to(clazz));
        }
        return list;
    }

}