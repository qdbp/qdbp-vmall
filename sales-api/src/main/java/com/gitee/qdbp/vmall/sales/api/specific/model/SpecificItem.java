package com.gitee.qdbp.vmall.sales.api.specific.model;

import java.io.Serializable;

/**
 * 商品规格项
 *
 * @author zhaohuihua
 * @version 180625
 */
public class SpecificItem implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 规格项ID **/
    private String id;
    /** 规格项文本 **/
    private String text;
    /** 规格项颜色 **/
    private String color;
    /** 规格项图片 **/
    private String image;

    /** 规格项ID **/
    public String getId() {
        return id;
    }

    /** 规格项ID **/
    public void setId(String id) {
        this.id = id;
    }

    /** 规格项文本 **/
    public String getText() {
        return text;
    }

    /** 规格项文本 **/
    public void setText(String text) {
        this.text = text;
    }

    /** 规格项颜色 **/
    public String getColor() {
        return color;
    }

    /** 规格项颜色 **/
    public void setColor(String color) {
        this.color = color;
    }

    /** 规格项图片 **/
    public String getImage() {
        return image;
    }

    /** 规格项图片 **/
    public void setImage(String image) {
        this.image = image;
    }

}
