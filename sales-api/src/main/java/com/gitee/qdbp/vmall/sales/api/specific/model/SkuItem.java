package com.gitee.qdbp.vmall.sales.api.specific.model;

import java.io.Serializable;

/**
 * 单品规格项
 *
 * @author zhaohuihua
 * @version 180625
 */
public class SkuItem implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 单品规格ID **/
    private String id;
    /** 规格分类Id **/
    private String specificKindId;
    /** 规格分类名称: 如颜色, 尺码 **/
    private String specificKindName;
    /** 规格项Id **/
    private String specificItemId;
    /** 规格项文本: 如颜色的红/黄/蓝; 尺码的XS/S/M/L/XL **/
    private String specificItemText;
    /** 规格项颜色 **/
    private String specificItemColor;
    /** 规格项图片 **/
    private String specificItemImage;

    /** 单品规格ID **/
    public String getId() {
        return id;
    }

    /** 单品规格ID **/
    public void setId(String id) {
        this.id = id;
    }

    /** 规格分类Id **/
    public String getSpecificKindId() {
        return specificKindId;
    }

    /** 规格分类Id **/
    public void setSpecificKindId(String kindId) {
        this.specificKindId = kindId;
    }

    /** 规格分类名称: 如颜色, 尺码 **/
    public String getSpecificKindName() {
        return specificKindName;
    }

    /** 规格分类名称: 如颜色, 尺码 **/
    public void setSpecificKindName(String kindName) {
        this.specificKindName = kindName;
    }

    /** 规格项Id **/
    public String getSpecificItemId() {
        return specificItemId;
    }

    /** 规格项Id **/
    public void setSpecificItemId(String specificItemId) {
        this.specificItemId = specificItemId;
    }

    /** 规格项文本: 如颜色的红/黄/蓝; 尺码的XS/S/M/L/XL **/
    public String getSpecificItemText() {
        return specificItemText;
    }

    /** 规格项文本: 如颜色的红/黄/蓝; 尺码的XS/S/M/L/XL **/
    public void setSpecificItemText(String specificItemText) {
        this.specificItemText = specificItemText;
    }

    /** 规格项颜色 **/
    public String getSpecificItemColor() {
        return specificItemColor;
    }

    /** 规格项颜色 **/
    public void setSpecificItemColor(String specificItemColor) {
        this.specificItemColor = specificItemColor;
    }

    /** 规格项图片 **/
    public String getSpecificItemImage() {
        return specificItemImage;
    }

    /** 规格项图片 **/
    public void setSpecificItemImage(String specificItemImage) {
        this.specificItemImage = specificItemImage;
    }

}
