package com.gitee.qdbp.vmall.sales.api.goods.manage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.PartList;
import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;
import com.gitee.qdbp.vmall.sales.enums.SumType;

/**
 * 销量汇总实体类
 *
 * @author zhh
 * @version 180626
 */
@OperateTraces(id = "id")
@DataIsolation("tenantCode")
public class SalesSummingBean implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 表名 **/
    public static final String TABLE = "vml_sales_summing";

    /** 主键 **/
    private String id;

    /** 租户编号 **/
    private String tenantCode;

    /** 商品唯一ID **/
    private String goodsUid;

    /** 单品ID **/
    private String skuId;

    /** 汇总类型(0.总|1.年|2.月|3.周|4.日) **/
    private SumType sumType;

    /** 时间范围 **/
    private Integer timeRange;

    /** 销量 **/
    private Integer salesQuantity;

    /** 获取主键 **/
    public String getId() {
        return id;
    }

    /** 设置主键 **/
    public void setId(String id) {
        this.id = id;
    }

    /** 获取租户编号 **/
    public String getTenantCode() {
        return tenantCode;
    }

    /** 设置租户编号 **/
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /** 获取商品唯一ID **/
    public String getGoodsUid() {
        return goodsUid;
    }

    /** 设置商品唯一ID **/
    public void setGoodsUid(String goodsUid) {
        this.goodsUid = goodsUid;
    }

    /** 获取单品ID **/
    public String getSkuId() {
        return skuId;
    }

    /** 设置单品ID **/
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    /** 获取汇总类型(0.总|1.年|2.月|3.周|4.日) **/
    public SumType getSumType() {
        return sumType;
    }

    /** 设置汇总类型(0.总|1.年|2.月|3.周|4.日) **/
    public void setSumType(SumType sumType) {
        this.sumType = sumType;
    }

    /** 获取时间范围 **/
    public Integer getTimeRange() {
        return timeRange;
    }

    /** 设置时间范围 **/
    public void setTimeRange(Integer timeRange) {
        this.timeRange = timeRange;
    }

    /** 获取销量 **/
    public Integer getSalesQuantity() {
        return salesQuantity;
    }

    /** 设置销量 **/
    public void setSalesQuantity(Integer salesQuantity) {
        this.salesQuantity = salesQuantity;
    }

    /**
     * 将当前对象转换为子类对象
     *
     * @param clazz 目标类型
     * @return 目标对象
     */
    public <T extends SalesSummingBean> T to(Class<T> clazz) {
        T instance;
        try {
            instance = clazz.newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to create " + clazz.getSimpleName() + " instance.", e);
        }

        instance.setId(this.getId()); // 主键
        instance.setTenantCode(this.getTenantCode()); // 租户编号
        instance.setGoodsUid(this.getGoodsUid()); // 商品唯一ID
        instance.setSkuId(this.getSkuId()); // 单品ID
        instance.setSumType(this.getSumType()); // 汇总类型(0.总|1.年|2.月|3.周|4.日)
        instance.setTimeRange(this.getTimeRange()); // 时间范围
        instance.setSalesQuantity(this.getSalesQuantity()); // 销量
        return instance;
    }

    /**
     * 将SalesSummingBean转换为子类对象
     *
     * @param beans 待转换的对象列表
     * @param clazz 目标类型
     * @param <T> SalesSummingBean或子类
     * @param <C> T的子类
     * @return 目标对象列表
     */
    public static <T extends SalesSummingBean, C extends T> List<C> to(List<T> beans, Class<C> clazz) {
        if (beans == null) {
            return null;
        }
        List<C> list;
        if (beans instanceof PartList) {
            PartList<C> partlist = new PartList<>();
            partlist.setTotal(((PartList<?>) beans).getTotal());
            list = partlist;
        } else {
            list = new ArrayList<>();
        }
        for (SalesSummingBean bean : beans) {
            list.add(bean.to(clazz));
        }
        return list;
    }

}