package com.gitee.qdbp.vmall.sales.api.goods.main.model;

import java.io.Serializable;

/**
 * 调整数量的参数(单一商品,没有规格)
 *
 * @author zhaohuihua
 * @version 180626
 */
public class QuantitySingleParams implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 商品版本ID **/
    private String uid;
    /** 增加数量 **/
    private int quantityAdd;

    /** 商品版本ID **/
    public String getUid() {
        return uid;
    }

    /** 商品版本ID **/
    public void setUid(String uid) {
        this.uid = uid;
    }

    /** 增加数量 **/
    public int getQuantityAdd() {
        return quantityAdd;
    }

    /** 增加数量 **/
    public void setQuantityAdd(int quantityAdd) {
        this.quantityAdd = quantityAdd;
    }

}
