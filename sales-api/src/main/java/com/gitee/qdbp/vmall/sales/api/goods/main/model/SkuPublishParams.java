package com.gitee.qdbp.vmall.sales.api.goods.main.model;

import java.io.Serializable;

/**
 * 商品发布参数(单一商品,没有规格)
 *
 * @author zhaohuihua
 * @version 170812
 */
public class SkuPublishParams implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 单品ID **/
    private String skuId;
    /** 初始销售量 **/
    private Integer salesQuantityAdd;
    /** 初始库存量 **/
    private Integer stockQuantityAdd;
    /** 市场价格描述 **/
    private String marketPriceText;
    /** 价格值(实际售价) **/
    private Double priceValue;

    /** 单品ID **/
    public String getSkuId() {
        return skuId;
    }

    /** 单品ID **/
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    /** 初始销售量 **/
    public Integer getSalesQuantityAdd() {
        return salesQuantityAdd;
    }

    /** 初始销售量 **/
    public void setSalesQuantityAdd(Integer salesQuantityAdd) {
        this.salesQuantityAdd = salesQuantityAdd;
    }

    /** 初始库存量 **/
    public Integer getStockQuantityAdd() {
        return stockQuantityAdd;
    }

    /** 初始库存量 **/
    public void setStockQuantityAdd(Integer stockQuantityAdd) {
        this.stockQuantityAdd = stockQuantityAdd;
    }

    /** 获取市场价格描述 **/
    public String getMarketPriceText() {
        return marketPriceText;
    }

    /** 设置市场价格描述 **/
    public void setMarketPriceText(String marketPriceText) {
        this.marketPriceText = marketPriceText;
    }

    /** 获取价格值(实际售价) **/
    public Double getPriceValue() {
        return priceValue;
    }

    /** 设置价格值(实际售价) **/
    public void setPriceValue(Double priceValue) {
        this.priceValue = priceValue;
    }

}
