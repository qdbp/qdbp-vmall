package com.gitee.qdbp.vmall.sales.api.goods.main.model;

import com.gitee.qdbp.able.model.reusable.ExtraData;

/**
 * 选项
 *
 * @author zhh
 * @version 170701
 */
public class GoodsOptions extends ExtraData {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    private String approveDesc;

    public String getApproveDesc() {
        return approveDesc;
    }

    public void setApproveDesc(String approveDesc) {
        this.approveDesc = approveDesc;
    }

}
