package com.gitee.qdbp.vmall.sales.api.goods.manage.service;

import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.SalesSummingBean;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.SalesSummingWhere;

/**
 * 销量汇总业务接口
 *
 * @author zhh
 * @version 170812
 */
public interface ISalesSummingQueryer {

    SalesSummingBean find(String id) throws ServiceException;

    SalesSummingBean find(SalesSummingWhere where) throws ServiceException;

    PageList<SalesSummingBean> list(SalesSummingWhere where, OrderPaging paging) throws ServiceException;

}