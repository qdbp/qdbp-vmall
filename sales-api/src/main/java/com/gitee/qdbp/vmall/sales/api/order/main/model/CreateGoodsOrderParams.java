package com.gitee.qdbp.vmall.sales.api.order.main.model;

import java.io.Serializable;
import java.util.List;
import com.gitee.qdbp.general.trade.api.order.main.model.OrderFormOptions;

/**
 * 创建订单的参数
 *
 * @author zhaohuihua
 * @version V1.0 2017年8月9日
 */
public class CreateGoodsOrderParams implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 商家租户编号 **/
    private String tenantCode;

    /** 折扣金额 **/
    private Double discountAmount;

    /** 运费金额 **/
    private Double freightAmount;

    /** 选项 **/
    private OrderFormOptions options;

    /** 商品列表 **/
    private List<CreateGoodsOrderItem> items;

    /** 获取商家租户编号 **/
    public String getTenantCode() {
        return tenantCode;
    }

    /** 设置商家租户编号 **/
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /** 获取折扣金额 **/
    public Double getDiscountAmount() {
        return discountAmount;
    }

    /** 设置折扣金额 **/
    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    /** 获取运费金额 **/
    public Double getFreightAmount() {
        return freightAmount;
    }

    /** 设置运费金额 **/
    public void setFreightAmount(Double freightAmount) {
        this.freightAmount = freightAmount;
    }

    /** 获取选项 **/
    public OrderFormOptions getOptions() {
        return options;
    }

    /** 获取选项, force=是否强制返回非空对象 **/
    public OrderFormOptions getOptions(boolean force) {
        if (options == null && force) {
            options = new OrderFormOptions();
        }
        return options;
    }

    /** 设置选项 **/
    public void setOptions(OrderFormOptions options) {
        this.options = options;
    }

    /** 商品列表 **/
    public List<CreateGoodsOrderItem> getItems() {
        return items;
    }

    /** 商品列表 **/
    public void setItems(List<CreateGoodsOrderItem> items) {
        this.items = items;
    }

}
