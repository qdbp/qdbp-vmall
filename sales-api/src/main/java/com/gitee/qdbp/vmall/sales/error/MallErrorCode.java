package com.gitee.qdbp.vmall.sales.error;

import com.gitee.qdbp.able.result.IResultMessage;

/**
 * 错误返回码枚举类
 *
 * @author zhaohuihua
 * @version 170707
 */
public enum MallErrorCode implements IResultMessage {

    EDIT_BY_ERROR_APPROVE_STATE("记录处于不允许编辑状态"),

    EDIT_BY_DIRTY_READ("记录在提交前已被他人编辑"),

    EDIT_BY_NOT_ONESELF("不允许编辑其他人的记录"),

    ZNB_NOT_ENOUGH_ERROR("用户积分不够支付当前订单"),

    GOODS_RECORD_NOT_FOUND("商品信息不存在"),

    GOODS_ALREADY_EXPIRED("商品已下架"),

    GOODS_INSUFFICIENT_INVENTORY("商品库存量不足"),

    ORDER_RECORD_NOT_FOUND("订单信息不存在"),

    PAYMENT_RECORD_NOT_FOUND("订单支付信息不存在"),

    ORDER_IN_PAYING_FOR_USER("存在支付中的订单"),

    ORDER_ALREADY_PROCESS("该笔订单已处理"),

    ORDER_PAYMENT_PROCESSING("订单支付中, 请等待系统处理"),

    ORDER_PAYMENT_FAILED("订单支付失败");

    /** 错误描述 **/
    private final String message;

    private MallErrorCode(String message) {
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.name();
    }

    @Override
    public String getMessage() {
        return message;
    }
}
