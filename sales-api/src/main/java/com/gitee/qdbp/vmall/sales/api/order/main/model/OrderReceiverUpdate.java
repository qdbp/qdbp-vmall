package com.gitee.qdbp.vmall.sales.api.order.main.model;

import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;

/**
 * 订单配送信息更新类
 *
 * @author zhh
 * @version 180624
 */
@OperateTraces(target = "where")
@DataIsolation(target = "where")
public class OrderReceiverUpdate extends OrderReceiverBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 商家租户编号是否更新为空值 **/
    private Boolean tenantCodeToNull;

    /** 订单ID是否更新为空值 **/
    private Boolean orderIdToNull;

    /** 快递公司ID是否更新为空值 **/
    private Boolean carrierCompanyIdToNull;

    /** 收件人姓名是否更新为空值 **/
    private Boolean receiverNameToNull;

    /** 收件人性别(0.未知|1.男|2.女)是否更新为空值 **/
    private Boolean receiverGenderToNull;

    /** 收件人手机号码是否更新为空值 **/
    private Boolean receiverPhoneToNull;

    /** 收件人城市(关联行政区划表)是否更新为空值 **/
    private Boolean receiverAreaCodeToNull;

    /** 收件人地址是否更新为空值 **/
    private Boolean receiverAddressToNull;

    /** 排序号(0为当前有效记录)是否更新为空值 **/
    private Boolean sortIndexToNull;
    /** 排序号(0为当前有效记录)的增加值 **/
    private Integer sortIndexAdd;

    /** 选项是否更新为空值 **/
    private Boolean optionsToNull;

    /** 更新操作的条件 **/
    private OrderReceiverWhere where;

    /** 判断商家租户编号是否更新为空值 **/
    public Boolean isTenantCodeToNull() {
        return tenantCodeToNull;
    }

    /**
     * 设置商家租户编号是否更新为空值
     *
     * @param tenantCodeToNull 商家租户编号是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTenantCodeToNull(Boolean tenantCodeToNull) {
        this.tenantCodeToNull = tenantCodeToNull;
    }

    /** 判断订单ID是否更新为空值 **/
    public Boolean isOrderIdToNull() {
        return orderIdToNull;
    }

    /**
     * 设置订单ID是否更新为空值
     *
     * @param orderIdToNull 订单ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setOrderIdToNull(Boolean orderIdToNull) {
        this.orderIdToNull = orderIdToNull;
    }

    /** 判断快递公司ID是否更新为空值 **/
    public Boolean isCarrierCompanyIdToNull() {
        return carrierCompanyIdToNull;
    }

    /** 设置快递公司ID是否更新为空值 **/
    public void setCarrierCompanyIdToNull(Boolean carrierCompanyIdToNull) {
        this.carrierCompanyIdToNull = carrierCompanyIdToNull;
    }

    /** 判断收件人姓名是否更新为空值 **/
    public Boolean isReceiverNameToNull() {
        return receiverNameToNull;
    }

    /** 设置收件人姓名是否更新为空值 **/
    public void setReceiverNameToNull(Boolean receiverNameToNull) {
        this.receiverNameToNull = receiverNameToNull;
    }

    /** 判断收件人性别(0.未知|1.男|2.女)是否更新为空值 **/
    public Boolean isReceiverGenderToNull() {
        return receiverGenderToNull;
    }

    /** 设置收件人性别(0.未知|1.男|2.女)是否更新为空值 **/
    public void setReceiverGenderToNull(Boolean receiverGenderToNull) {
        this.receiverGenderToNull = receiverGenderToNull;
    }

    /** 判断收件人手机号码是否更新为空值 **/
    public Boolean isReceiverPhoneToNull() {
        return receiverPhoneToNull;
    }

    /** 设置收件人手机号码是否更新为空值 **/
    public void setReceiverPhoneToNull(Boolean receiverPhoneToNull) {
        this.receiverPhoneToNull = receiverPhoneToNull;
    }

    /** 判断收件人城市(关联行政区划表)是否更新为空值 **/
    public Boolean isReceiverAreaCodeToNull() {
        return receiverAreaCodeToNull;
    }

    /** 设置收件人城市(关联行政区划表)是否更新为空值 **/
    public void setReceiverAreaCodeToNull(Boolean receiverAreaCodeToNull) {
        this.receiverAreaCodeToNull = receiverAreaCodeToNull;
    }

    /** 判断收件人地址是否更新为空值 **/
    public Boolean isReceiverAddressToNull() {
        return receiverAddressToNull;
    }

    /** 设置收件人地址是否更新为空值 **/
    public void setReceiverAddressToNull(Boolean receiverAddressToNull) {
        this.receiverAddressToNull = receiverAddressToNull;
    }

    /** 判断排序号(0为当前有效记录)是否更新为空值 **/
    public Boolean isSortIndexToNull() {
        return sortIndexToNull;
    }

    /**
     * 设置排序号(0为当前有效记录)是否更新为空值
     *
     * @param sortIndexToNull 排序号(0为当前有效记录)是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSortIndexToNull(Boolean sortIndexToNull) {
        this.sortIndexToNull = sortIndexToNull;
    }

    /** 获取排序号(0为当前有效记录)的增加值 **/
    public Integer getSortIndexAdd() {
        return sortIndexAdd;
    }

    /** 设置排序号(0为当前有效记录)的增加值 **/
    public void setSortIndexAdd(Integer sortIndexAdd) {
        this.sortIndexAdd = sortIndexAdd;
    }

    /** 判断选项是否更新为空值 **/
    public Boolean isOptionsToNull() {
        return optionsToNull;
    }

    /** 设置选项是否更新为空值 **/
    public void setOptionsToNull(Boolean optionsToNull) {
        this.optionsToNull = optionsToNull;
    }

    /** 获取更新操作的条件 **/
    public OrderReceiverWhere getWhere() {
        return where;
    }

    /** 获取更新操作的条件, force=是否强制返回非空条件 **/
    public OrderReceiverWhere getWhere(boolean force) {
        if (where == null && force) {
            where = new OrderReceiverWhere();
        }
        return where;
    }

    /** 设置更新操作的条件 **/
    public void setWhere(OrderReceiverWhere where) {
        this.where = where;
    }
}