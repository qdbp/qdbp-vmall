package com.gitee.qdbp.vmall.sales.api.order.manage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.PartList;
import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;
import com.gitee.qdbp.base.enums.DataState;
import com.gitee.qdbp.base.enums.Gender;

/**
 * 商品订单发货单实体类
 *
 * @author zhh
 * @version 180626
 */
@OperateTraces(id = "id")
@DataIsolation("tenantCode")
public class OrderDeliveryBean implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 表名 **/
    public static final String TABLE = "vml_order_delivery";

    /** 主键 **/
    private String id;

    /** 租户编号 **/
    private String tenantCode;

    /** 订单ID **/
    private String orderId;

    /** 发货人ID(内部员工) **/
    private String delivererId;

    /** 收件人姓名 **/
    private String receiverName;

    /** 收件人性别(0.未知|1.男|2.女) **/
    private Gender receiverGender;

    /** 收件人手机号码 **/
    private String receiverPhone;

    /** 收件人城市(关联行政区划表) **/
    private String receiverAreaCode;

    /** 收件人地址 **/
    private String receiverAddress;

    /** 快递公司ID **/
    private String carrierCompanyId;

    /** 快递单号 **/
    private String carrierVoucherCode;

    /** 取件快递员姓名 **/
    private String carrierClerkName;

    /** 取件快递员电话 **/
    private String carrierClerkPhone;

    /** 选项 **/
    private OrderDeliveryOptions options;

    /** 创建时间 **/
    private Date createTime;

    /** 数据状态:0为正常|其他为删除 **/
    private DataState dataState;

    /** 获取主键 **/
    public String getId() {
        return id;
    }

    /** 设置主键 **/
    public void setId(String id) {
        this.id = id;
    }

    /** 获取租户编号 **/
    public String getTenantCode() {
        return tenantCode;
    }

    /** 设置租户编号 **/
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /** 获取订单ID **/
    public String getOrderId() {
        return orderId;
    }

    /** 设置订单ID **/
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /** 获取发货人ID(内部员工) **/
    public String getDelivererId() {
        return delivererId;
    }

    /** 设置发货人ID(内部员工) **/
    public void setDelivererId(String delivererId) {
        this.delivererId = delivererId;
    }

    /** 获取收件人姓名 **/
    public String getReceiverName() {
        return receiverName;
    }

    /** 设置收件人姓名 **/
    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    /** 获取收件人性别(0.未知|1.男|2.女) **/
    public Gender getReceiverGender() {
        return receiverGender;
    }

    /** 设置收件人性别(0.未知|1.男|2.女) **/
    public void setReceiverGender(Gender receiverGender) {
        this.receiverGender = receiverGender;
    }

    /** 获取收件人手机号码 **/
    public String getReceiverPhone() {
        return receiverPhone;
    }

    /** 设置收件人手机号码 **/
    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    /** 获取收件人城市(关联行政区划表) **/
    public String getReceiverAreaCode() {
        return receiverAreaCode;
    }

    /** 设置收件人城市(关联行政区划表) **/
    public void setReceiverAreaCode(String receiverAreaCode) {
        this.receiverAreaCode = receiverAreaCode;
    }

    /** 获取收件人地址 **/
    public String getReceiverAddress() {
        return receiverAddress;
    }

    /** 设置收件人地址 **/
    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    /** 获取快递公司ID **/
    public String getCarrierCompanyId() {
        return carrierCompanyId;
    }

    /** 设置快递公司ID **/
    public void setCarrierCompanyId(String carrierCompanyId) {
        this.carrierCompanyId = carrierCompanyId;
    }

    /** 获取快递单号 **/
    public String getCarrierVoucherCode() {
        return carrierVoucherCode;
    }

    /** 设置快递单号 **/
    public void setCarrierVoucherCode(String carrierVoucherCode) {
        this.carrierVoucherCode = carrierVoucherCode;
    }

    /** 获取取件快递员姓名 **/
    public String getCarrierClerkName() {
        return carrierClerkName;
    }

    /** 设置取件快递员姓名 **/
    public void setCarrierClerkName(String carrierClerkName) {
        this.carrierClerkName = carrierClerkName;
    }

    /** 获取取件快递员电话 **/
    public String getCarrierClerkPhone() {
        return carrierClerkPhone;
    }

    /** 设置取件快递员电话 **/
    public void setCarrierClerkPhone(String carrierClerkPhone) {
        this.carrierClerkPhone = carrierClerkPhone;
    }

    /** 获取选项 **/
    public OrderDeliveryOptions getOptions() {
        return options;
    }

    /** 获取选项, force=是否强制返回非空对象 **/
    public OrderDeliveryOptions getOptions(boolean force) {
        if (options == null && force) {
            options = new OrderDeliveryOptions();
        }
        return options;
    }

    /** 设置选项 **/
    public void setOptions(OrderDeliveryOptions options) {
        this.options = options;
    }

    /** 获取创建时间 **/
    public Date getCreateTime() {
        return createTime;
    }

    /** 设置创建时间 **/
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /** 获取数据状态:0为正常|其他为删除 **/
    public DataState getDataState() {
        return dataState;
    }

    /** 设置数据状态:0为正常|其他为删除 **/
    public void setDataState(DataState dataState) {
        this.dataState = dataState;
    }

    /**
     * 将当前对象转换为子类对象
     *
     * @param clazz 目标类型
     * @return 目标对象
     */
    public <T extends OrderDeliveryBean> T to(Class<T> clazz) {
        T instance;
        try {
            instance = clazz.newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to create " + clazz.getSimpleName() + " instance.", e);
        }

        instance.setId(this.getId()); // 主键
        instance.setTenantCode(this.getTenantCode()); // 租户编号
        instance.setOrderId(this.getOrderId()); // 订单ID
        instance.setDelivererId(this.getDelivererId()); // 发货人ID(内部员工)
        instance.setReceiverName(this.getReceiverName()); // 收件人姓名
        instance.setReceiverGender(this.getReceiverGender()); // 收件人性别(0.未知|1.男|2.女)
        instance.setReceiverPhone(this.getReceiverPhone()); // 收件人手机号码
        instance.setReceiverAreaCode(this.getReceiverAreaCode()); // 收件人城市(关联行政区划表)
        instance.setReceiverAddress(this.getReceiverAddress()); // 收件人地址
        instance.setCarrierCompanyId(this.getCarrierCompanyId()); // 快递公司ID
        instance.setCarrierVoucherCode(this.getCarrierVoucherCode()); // 快递单号
        instance.setCarrierClerkName(this.getCarrierClerkName()); // 取件快递员姓名
        instance.setCarrierClerkPhone(this.getCarrierClerkPhone()); // 取件快递员电话
        instance.setOptions(this.getOptions()); // 选项
        instance.setCreateTime(this.getCreateTime()); // 创建时间
        instance.setDataState(this.getDataState()); // 数据状态:0为正常|其他为删除
        return instance;
    }

    /**
     * 将OrderDeliveryBean转换为子类对象
     *
     * @param beans 待转换的对象列表
     * @param clazz 目标类型
     * @param <T> OrderDeliveryBean或子类
     * @param <C> T的子类
     * @return 目标对象列表
     */
    public static <T extends OrderDeliveryBean, C extends T> List<C> to(List<T> beans, Class<C> clazz) {
        if (beans == null) {
            return null;
        }
        List<C> list;
        if (beans instanceof PartList) {
            PartList<C> partlist = new PartList<>();
            partlist.setTotal(((PartList<?>) beans).getTotal());
            list = partlist;
        } else {
            list = new ArrayList<>();
        }
        for (OrderDeliveryBean bean : beans) {
            list.add(bean.to(clazz));
        }
        return list;
    }

}