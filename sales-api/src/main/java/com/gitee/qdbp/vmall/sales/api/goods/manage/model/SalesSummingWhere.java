package com.gitee.qdbp.vmall.sales.api.goods.manage.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.gitee.qdbp.vmall.sales.enums.SumType;

/**
 * 销量汇总查询类
 *
 * @author zhh
 * @version 180626
 */
public class SalesSummingWhere extends SalesSummingBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 主键列表 **/
    private List<String> ids;

    /** 主键前模匹配条件 **/
    private String idStarts;

    /** 主键后模匹配条件 **/
    private String idEnds;

    /** 主键模糊查询条件 **/
    private String idLike;

    /** 租户编号空值/非空值查询 **/
    private Boolean tenantCodeIsNull;

    /** 租户编号前模匹配条件 **/
    private String tenantCodeStarts;

    /** 租户编号后模匹配条件 **/
    private String tenantCodeEnds;

    /** 租户编号模糊查询条件 **/
    private String tenantCodeLike;

    /** 商品唯一ID空值/非空值查询 **/
    private Boolean goodsUidIsNull;

    /** 商品唯一ID列表 **/
    private List<String> goodsUids;

    /** 商品唯一ID前模匹配条件 **/
    private String goodsUidStarts;

    /** 商品唯一ID后模匹配条件 **/
    private String goodsUidEnds;

    /** 商品唯一ID模糊查询条件 **/
    private String goodsUidLike;

    /** 单品ID空值/非空值查询 **/
    private Boolean skuIdIsNull;

    /** 单品ID列表 **/
    private List<String> skuIds;

    /** 单品ID前模匹配条件 **/
    private String skuIdStarts;

    /** 单品ID后模匹配条件 **/
    private String skuIdEnds;

    /** 单品ID模糊查询条件 **/
    private String skuIdLike;

    /** 汇总类型(0.总|1.年|2.月|3.周|4.日)空值/非空值查询 **/
    private Boolean sumTypeIsNull;

    /** 汇总类型(0.总|1.年|2.月|3.周|4.日)列表 **/
    private List<SumType> sumTypes;

    /** 时间范围空值/非空值查询 **/
    private Boolean timeRangeIsNull;

    /** 最小时间范围 **/
    private Integer timeRangeMin;

    /** 最大时间范围 **/
    private Integer timeRangeMax;

    /** 销量空值/非空值查询 **/
    private Boolean salesQuantityIsNull;

    /** 最小销量 **/
    private Integer salesQuantityMin;

    /** 最大销量 **/
    private Integer salesQuantityMax;

    /** 获取主键列表 **/
    public List<String> getIds() {
        return ids;
    }

    /** 设置主键列表 **/
    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    /** 增加主键 **/
    public void addId(String... ids) {
        if (this.ids == null) {
            this.ids = new ArrayList<>();
        }
        this.ids.addAll(Arrays.asList(ids));
    }

    /** 获取主键前模匹配条件 **/
    public String getIdStarts() {
        return idStarts;
    }

    /** 设置主键前模匹配条件 **/
    public void setIdStarts(String idStarts) {
        this.idStarts = idStarts;
    }

    /** 获取主键后模匹配条件 **/
    public String getIdEnds() {
        return idEnds;
    }

    /** 设置主键后模匹配条件 **/
    public void setIdEnds(String idEnds) {
        this.idEnds = idEnds;
    }

    /** 获取主键模糊查询条件 **/
    public String getIdLike() {
        return idLike;
    }

    /** 设置主键模糊查询条件 **/
    public void setIdLike(String idLike) {
        this.idLike = idLike;
    }

    /** 判断租户编号是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getTenantCodeIsNull() {
        return tenantCodeIsNull;
    }

    /**
     * 设置租户编号空值查询(true:空值查询|false:非空值查询)
     *
     * @param tenantCodeIsNull 租户编号空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTenantCodeIsNull(Boolean tenantCodeIsNull) {
        this.tenantCodeIsNull = tenantCodeIsNull;
    }

    /** 获取租户编号前模匹配条件 **/
    public String getTenantCodeStarts() {
        return tenantCodeStarts;
    }

    /** 设置租户编号前模匹配条件 **/
    public void setTenantCodeStarts(String tenantCodeStarts) {
        this.tenantCodeStarts = tenantCodeStarts;
    }

    /** 获取租户编号后模匹配条件 **/
    public String getTenantCodeEnds() {
        return tenantCodeEnds;
    }

    /** 设置租户编号后模匹配条件 **/
    public void setTenantCodeEnds(String tenantCodeEnds) {
        this.tenantCodeEnds = tenantCodeEnds;
    }

    /** 获取租户编号模糊查询条件 **/
    public String getTenantCodeLike() {
        return tenantCodeLike;
    }

    /** 设置租户编号模糊查询条件 **/
    public void setTenantCodeLike(String tenantCodeLike) {
        this.tenantCodeLike = tenantCodeLike;
    }

    /** 判断商品唯一ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getGoodsUidIsNull() {
        return goodsUidIsNull;
    }

    /**
     * 设置商品唯一ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param goodsUidIsNull 商品唯一ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setGoodsUidIsNull(Boolean goodsUidIsNull) {
        this.goodsUidIsNull = goodsUidIsNull;
    }

    /** 获取商品唯一ID列表 **/
    public List<String> getGoodsUids() {
        return goodsUids;
    }

    /** 设置商品唯一ID列表 **/
    public void setGoodsUids(List<String> goodsUids) {
        this.goodsUids = goodsUids;
    }

    /** 增加商品唯一ID **/
    public void addGoodsUid(String... goodsUids) {
        if (this.goodsUids == null) {
            this.goodsUids = new ArrayList<>();
        }
        this.goodsUids.addAll(Arrays.asList(goodsUids));
    }

    /** 获取商品唯一ID前模匹配条件 **/
    public String getGoodsUidStarts() {
        return goodsUidStarts;
    }

    /** 设置商品唯一ID前模匹配条件 **/
    public void setGoodsUidStarts(String goodsUidStarts) {
        this.goodsUidStarts = goodsUidStarts;
    }

    /** 获取商品唯一ID后模匹配条件 **/
    public String getGoodsUidEnds() {
        return goodsUidEnds;
    }

    /** 设置商品唯一ID后模匹配条件 **/
    public void setGoodsUidEnds(String goodsUidEnds) {
        this.goodsUidEnds = goodsUidEnds;
    }

    /** 获取商品唯一ID模糊查询条件 **/
    public String getGoodsUidLike() {
        return goodsUidLike;
    }

    /** 设置商品唯一ID模糊查询条件 **/
    public void setGoodsUidLike(String goodsUidLike) {
        this.goodsUidLike = goodsUidLike;
    }

    /** 判断单品ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSkuIdIsNull() {
        return skuIdIsNull;
    }

    /**
     * 设置单品ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param skuIdIsNull 单品ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSkuIdIsNull(Boolean skuIdIsNull) {
        this.skuIdIsNull = skuIdIsNull;
    }

    /** 获取单品ID列表 **/
    public List<String> getSkuIds() {
        return skuIds;
    }

    /** 设置单品ID列表 **/
    public void setSkuIds(List<String> skuIds) {
        this.skuIds = skuIds;
    }

    /** 增加单品ID **/
    public void addSkuId(String... skuIds) {
        if (this.skuIds == null) {
            this.skuIds = new ArrayList<>();
        }
        this.skuIds.addAll(Arrays.asList(skuIds));
    }

    /** 获取单品ID前模匹配条件 **/
    public String getSkuIdStarts() {
        return skuIdStarts;
    }

    /** 设置单品ID前模匹配条件 **/
    public void setSkuIdStarts(String skuIdStarts) {
        this.skuIdStarts = skuIdStarts;
    }

    /** 获取单品ID后模匹配条件 **/
    public String getSkuIdEnds() {
        return skuIdEnds;
    }

    /** 设置单品ID后模匹配条件 **/
    public void setSkuIdEnds(String skuIdEnds) {
        this.skuIdEnds = skuIdEnds;
    }

    /** 获取单品ID模糊查询条件 **/
    public String getSkuIdLike() {
        return skuIdLike;
    }

    /** 设置单品ID模糊查询条件 **/
    public void setSkuIdLike(String skuIdLike) {
        this.skuIdLike = skuIdLike;
    }

    /** 判断汇总类型(0.总|1.年|2.月|3.周|4.日)是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSumTypeIsNull() {
        return sumTypeIsNull;
    }

    /**
     * 设置汇总类型(0.总|1.年|2.月|3.周|4.日)空值查询(true:空值查询|false:非空值查询)
     *
     * @param sumTypeIsNull 汇总类型(0.总|1.年|2.月|3.周|4.日)空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSumTypeIsNull(Boolean sumTypeIsNull) {
        this.sumTypeIsNull = sumTypeIsNull;
    }

    /** 获取汇总类型(0.总|1.年|2.月|3.周|4.日)列表 **/
    public List<SumType> getSumTypes() {
        return sumTypes;
    }

    /** 设置汇总类型(0.总|1.年|2.月|3.周|4.日)列表 **/
    public void setSumTypes(List<SumType> sumTypes) {
        this.sumTypes = sumTypes;
    }

    /** 增加汇总类型(0.总|1.年|2.月|3.周|4.日) **/
    public void addSumType(SumType... sumTypes) {
        if (this.sumTypes == null) {
            this.sumTypes = new ArrayList<>();
        }
        this.sumTypes.addAll(Arrays.asList(sumTypes));
    }

    /** 判断时间范围是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getTimeRangeIsNull() {
        return timeRangeIsNull;
    }

    /**
     * 设置时间范围空值查询(true:空值查询|false:非空值查询)
     *
     * @param timeRangeIsNull 时间范围空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTimeRangeIsNull(Boolean timeRangeIsNull) {
        this.timeRangeIsNull = timeRangeIsNull;
    }

    /** 获取最小时间范围 **/
    public Integer getTimeRangeMin() {
        return timeRangeMin;
    }

    /** 设置最小时间范围 **/
    public void setTimeRangeMin(Integer timeRangeMin) {
        this.timeRangeMin = timeRangeMin;
    }

    /** 获取最大时间范围 **/
    public Integer getTimeRangeMax() {
        return timeRangeMax;
    }

    /** 设置最大时间范围 **/
    public void setTimeRangeMax(Integer timeRangeMax) {
        this.timeRangeMax = timeRangeMax;
    }

    /** 判断销量是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSalesQuantityIsNull() {
        return salesQuantityIsNull;
    }

    /**
     * 设置销量空值查询(true:空值查询|false:非空值查询)
     *
     * @param salesQuantityIsNull 销量空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSalesQuantityIsNull(Boolean salesQuantityIsNull) {
        this.salesQuantityIsNull = salesQuantityIsNull;
    }

    /** 获取最小销量 **/
    public Integer getSalesQuantityMin() {
        return salesQuantityMin;
    }

    /** 设置最小销量 **/
    public void setSalesQuantityMin(Integer salesQuantityMin) {
        this.salesQuantityMin = salesQuantityMin;
    }

    /** 获取最大销量 **/
    public Integer getSalesQuantityMax() {
        return salesQuantityMax;
    }

    /** 设置最大销量 **/
    public void setSalesQuantityMax(Integer salesQuantityMax) {
        this.salesQuantityMax = salesQuantityMax;
    }

}