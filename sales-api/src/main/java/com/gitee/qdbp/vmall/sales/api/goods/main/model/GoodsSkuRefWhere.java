package com.gitee.qdbp.vmall.sales.api.goods.main.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 商品单品关联查询类
 *
 * @author zhh
 * @version 180627
 */
public class GoodsSkuRefWhere extends GoodsSkuRefBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 主键列表 **/
    private List<String> ids;

    /** 主键前模匹配条件 **/
    private String idStarts;

    /** 主键后模匹配条件 **/
    private String idEnds;

    /** 主键模糊查询条件 **/
    private String idLike;

    /** 商品唯一ID空值/非空值查询 **/
    private Boolean goodsUidIsNull;

    /** 商品唯一ID前模匹配条件 **/
    private String goodsUidStarts;

    /** 商品唯一ID后模匹配条件 **/
    private String goodsUidEnds;

    /** 商品唯一ID模糊查询条件 **/
    private String goodsUidLike;

    /** 单品ID空值/非空值查询 **/
    private Boolean skuIdIsNull;

    /** 单品ID前模匹配条件 **/
    private String skuIdStarts;

    /** 单品ID后模匹配条件 **/
    private String skuIdEnds;

    /** 单品ID模糊查询条件 **/
    private String skuIdLike;

    /** 获取主键列表 **/
    public List<String> getIds() {
        return ids;
    }

    /** 设置主键列表 **/
    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    /** 增加主键 **/
    public void addId(String... ids) {
        if (this.ids == null) {
            this.ids = new ArrayList<>();
        }
        this.ids.addAll(Arrays.asList(ids));
    }

    /** 获取主键前模匹配条件 **/
    public String getIdStarts() {
        return idStarts;
    }

    /** 设置主键前模匹配条件 **/
    public void setIdStarts(String idStarts) {
        this.idStarts = idStarts;
    }

    /** 获取主键后模匹配条件 **/
    public String getIdEnds() {
        return idEnds;
    }

    /** 设置主键后模匹配条件 **/
    public void setIdEnds(String idEnds) {
        this.idEnds = idEnds;
    }

    /** 获取主键模糊查询条件 **/
    public String getIdLike() {
        return idLike;
    }

    /** 设置主键模糊查询条件 **/
    public void setIdLike(String idLike) {
        this.idLike = idLike;
    }

    /** 判断商品唯一ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getGoodsUidIsNull() {
        return goodsUidIsNull;
    }

    /**
     * 设置商品唯一ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param goodsUidIsNull 商品唯一ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setGoodsUidIsNull(Boolean goodsUidIsNull) {
        this.goodsUidIsNull = goodsUidIsNull;
    }

    /** 获取商品唯一ID前模匹配条件 **/
    public String getGoodsUidStarts() {
        return goodsUidStarts;
    }

    /** 设置商品唯一ID前模匹配条件 **/
    public void setGoodsUidStarts(String goodsUidStarts) {
        this.goodsUidStarts = goodsUidStarts;
    }

    /** 获取商品唯一ID后模匹配条件 **/
    public String getGoodsUidEnds() {
        return goodsUidEnds;
    }

    /** 设置商品唯一ID后模匹配条件 **/
    public void setGoodsUidEnds(String goodsUidEnds) {
        this.goodsUidEnds = goodsUidEnds;
    }

    /** 获取商品唯一ID模糊查询条件 **/
    public String getGoodsUidLike() {
        return goodsUidLike;
    }

    /** 设置商品唯一ID模糊查询条件 **/
    public void setGoodsUidLike(String goodsUidLike) {
        this.goodsUidLike = goodsUidLike;
    }

    /** 判断单品ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSkuIdIsNull() {
        return skuIdIsNull;
    }

    /**
     * 设置单品ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param skuIdIsNull 单品ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSkuIdIsNull(Boolean skuIdIsNull) {
        this.skuIdIsNull = skuIdIsNull;
    }

    /** 获取单品ID前模匹配条件 **/
    public String getSkuIdStarts() {
        return skuIdStarts;
    }

    /** 设置单品ID前模匹配条件 **/
    public void setSkuIdStarts(String skuIdStarts) {
        this.skuIdStarts = skuIdStarts;
    }

    /** 获取单品ID后模匹配条件 **/
    public String getSkuIdEnds() {
        return skuIdEnds;
    }

    /** 设置单品ID后模匹配条件 **/
    public void setSkuIdEnds(String skuIdEnds) {
        this.skuIdEnds = skuIdEnds;
    }

    /** 获取单品ID模糊查询条件 **/
    public String getSkuIdLike() {
        return skuIdLike;
    }

    /** 设置单品ID模糊查询条件 **/
    public void setSkuIdLike(String skuIdLike) {
        this.skuIdLike = skuIdLike;
    }

}