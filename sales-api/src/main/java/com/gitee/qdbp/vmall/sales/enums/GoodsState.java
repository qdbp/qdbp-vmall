package com.gitee.qdbp.vmall.sales.enums;

/**
 * GoodsState枚举类<br>
 * 商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)
 *
 * @author zhh
 * @version 170803
 */
public enum GoodsState {

    /** 0.未提交 **/
    PENDING,

    /** 1.已提交 **/
    SUBMITTED,

    /** 2.已审核 **/
    ARGEED,

    /** 3.已驳回 **/
    REJECTED,

    /** 4.已上架 **/
    SELLING,

    /** 5.已下架 **/
    PAUSING;
}