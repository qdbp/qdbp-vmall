package com.gitee.qdbp.vmall.sales.api.goods.manage.model;

import java.io.Serializable;

public class ShoppingGoodsItem implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;


    /** 租户编号 **/
    private String tenantCode;
    /** 商品版本ID(必填) **/
    private String goodsVid;
    /** 单品ID(复合商品必填) **/
    private String skuId;
    /** 购买数量 **/
    private Integer quantity;

    /** 获取租户编号 **/
    public String getTenantCode() {
        return tenantCode;
    }

    /** 设置租户编号 **/
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /** 商品版本ID(必填) **/
    public String getGoodsVid() {
        return goodsVid;
    }

    /** 商品版本ID(必填) **/
    public void setGoodsVid(String goodsVid) {
        this.goodsVid = goodsVid;
    }

    /** 单品ID(复合商品必填) **/
    public String getSkuId() {
        return skuId;
    }

    /** 单品ID(复合商品必填) **/
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    /** 购买数量 **/
    public Integer getQuantity() {
        return quantity;
    }

    /** 购买数量 **/
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
