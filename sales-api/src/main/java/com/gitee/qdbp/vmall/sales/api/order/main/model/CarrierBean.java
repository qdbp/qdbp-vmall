package com.gitee.qdbp.vmall.sales.api.order.main.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.PartList;

/**
 * 快递公司信息
 *
 * @author zhh
 * @version 180624
 */
public class CarrierBean implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 快递公司ID **/
    private String companyId;

    /** 快递单号 **/
    private String voucherCode;

    /** 取件快递员姓名 **/
    private String clerkName;

    /** 取件快递员电话 **/
    private String clerkPhone;

    /** 获取快递公司ID **/
    public String getCompanyId() {
        return companyId;
    }

    /** 设置快递公司ID **/
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    /** 获取快递单号 **/
    public String getVoucherCode() {
        return voucherCode;
    }

    /** 设置快递单号 **/
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    /** 获取取件快递员姓名 **/
    public String getClerkName() {
        return clerkName;
    }

    /** 设置取件快递员姓名 **/
    public void setClerkName(String clerkName) {
        this.clerkName = clerkName;
    }

    /** 获取取件快递员电话 **/
    public String getClerkPhone() {
        return clerkPhone;
    }

    /** 设置取件快递员电话 **/
    public void setClerkPhone(String clerkPhone) {
        this.clerkPhone = clerkPhone;
    }

    /**
     * 将当前对象转换为子类对象
     *
     * @param clazz 目标类型
     * @return 目标对象
     */
    public <T extends CarrierBean> T to(Class<T> clazz) {
        T instance;
        try {
            instance = clazz.newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to create " + clazz.getSimpleName() + " instance.", e);
        }

        instance.setCompanyId(this.getCompanyId()); // 快递公司ID
        instance.setVoucherCode(this.getVoucherCode()); // 快递单号
        instance.setClerkName(this.getClerkName()); // 取件快递员姓名
        instance.setClerkPhone(this.getClerkPhone()); // 取件快递员电话
        return instance;
    }

    /**
     * 将OrderDeliveryBean转换为子类对象
     *
     * @param beans 待转换的对象列表
     * @param clazz 目标类型
     * @param <T> OrderDeliveryBean或子类
     * @param <C> T的子类
     * @return 目标对象列表
     */
    public static <T extends CarrierBean, C extends T> List<C> to(List<T> beans, Class<C> clazz) {
        if (beans == null) {
            return null;
        }
        List<C> list;
        if (beans instanceof PartList) {
            PartList<C> partlist = new PartList<>();
            partlist.setTotal(((PartList<?>) beans).getTotal());
            list = partlist;
        } else {
            list = new ArrayList<>();
        }
        for (CarrierBean bean : beans) {
            list.add(bean.to(clazz));
        }
        return list;
    }

}
