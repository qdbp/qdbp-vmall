package com.gitee.qdbp.vmall.sales.api.goods.main.model;

import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;

/**
 * 商品版本信息更新类
 *
 * @author zhh
 * @version 180627
 */
@OperateTraces(target = "where")
@DataIsolation(target = "where")
public class GoodsVersionUpdate extends GoodsVersionBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 商品唯一ID是否更新为空值 **/
    private Boolean uidToNull;

    /** 租户编号是否更新为空值 **/
    private Boolean tenantCodeToNull;

    /** 部门编号是否更新为空值 **/
    private Boolean deptCodeToNull;

    /** 分类编号是否更新为空值 **/
    private Boolean categoryCodeToNull;

    /** 品牌编号是否更新为空值 **/
    private Boolean brandCodeToNull;

    /** 型号是否更新为空值 **/
    private Boolean modelNumberToNull;

    /** 商品名称是否更新为空值 **/
    private Boolean nameToNull;

    /** 标题是否更新为空值 **/
    private Boolean titleToNull;

    /** 单位(个/件/台/袋等)是否更新为空值 **/
    private Boolean unitToNull;

    /** 图片信息是否更新为空值 **/
    private Boolean imageInfoToNull;

    /** 市场价格描述是否更新为空值 **/
    private Boolean marketPriceTextToNull;

    /** 销售价格描述(用于主商品展示的价格描述, 如80~99,199起)是否更新为空值 **/
    private Boolean salesPriceTextToNull;

    /** 价格值(实际售价)是否更新为空值 **/
    private Boolean priceValueToNull;
    /** 价格值(实际售价)的增加值 **/
    private Double priceValueAdd;

    /** 总销量是否更新为空值 **/
    private Boolean salesQuantityTotalToNull;
    /** 总销量的增加值 **/
    private Integer salesQuantityTotalAdd;

    /** 库存量是否更新为空值 **/
    private Boolean stockQuantityToNull;
    /** 库存量的增加值 **/
    private Integer stockQuantityAdd;

    /** 摘要是否更新为空值 **/
    private Boolean introTextToNull;

    /** 详细描述(富文本)是否更新为空值 **/
    private Boolean descDetailsToNull;

    /** 排序号是否更新为空值 **/
    private Boolean sortIndexToNull;
    /** 排序号的增加值 **/
    private Long sortIndexAdd;

    /** 规格分类ID是否更新为空值 **/
    private Boolean specificKindIdToNull;

    /** 规格数据列表是否更新为空值 **/
    private Boolean specificKindDataToNull;

    /** 单品实例列表是否更新为空值 **/
    private Boolean skuInstanceDataToNull;

    /** 创建人ID是否更新为空值 **/
    private Boolean creatorIdToNull;

    /** 发布时间是否更新为空值 **/
    private Boolean publishTimeToNull;
    /** 发布时间是否更新为数据库当前时间 **/
    private Boolean publishTimeToCurrent;
    /** 发布时间的增加值(单位:秒) **/
    private Long publishTimeAdd;

    /** 过期时间是否更新为空值 **/
    private Boolean expireTimeToNull;
    /** 过期时间是否更新为数据库当前时间 **/
    private Boolean expireTimeToCurrent;
    /** 过期时间的增加值(单位:秒) **/
    private Long expireTimeAdd;

    /** 选项是否更新为空值 **/
    private Boolean optionsToNull;

    /** 提交说明是否更新为空值 **/
    private Boolean submitDescToNull;

    /** 编辑序号(每编辑一次递增1)是否更新为空值 **/
    private Boolean editIndexToNull;
    /** 编辑序号(每编辑一次递增1)的增加值 **/
    private Integer editIndexAdd;

    /** 商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)是否更新为空值 **/
    private Boolean goodsStateToNull;

    /** 版本状态(0.正本|1.副本|2.归档)是否更新为空值 **/
    private Boolean versionStateToNull;

    /** 查询关键字是否更新为空值 **/
    private Boolean queryKeywordsToNull;

    /** 更新操作的条件 **/
    private GoodsVersionWhere where;

    /** 判断商品唯一ID是否更新为空值 **/
    public Boolean isUidToNull() {
        return uidToNull;
    }

    /**
     * 设置商品唯一ID是否更新为空值
     *
     * @param uidToNull 商品唯一ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setUidToNull(Boolean uidToNull) {
        this.uidToNull = uidToNull;
    }

    /** 判断租户编号是否更新为空值 **/
    public Boolean isTenantCodeToNull() {
        return tenantCodeToNull;
    }

    /**
     * 设置租户编号是否更新为空值
     *
     * @param tenantCodeToNull 租户编号是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTenantCodeToNull(Boolean tenantCodeToNull) {
        this.tenantCodeToNull = tenantCodeToNull;
    }

    /** 判断部门编号是否更新为空值 **/
    public Boolean isDeptCodeToNull() {
        return deptCodeToNull;
    }

    /** 设置部门编号是否更新为空值 **/
    public void setDeptCodeToNull(Boolean deptCodeToNull) {
        this.deptCodeToNull = deptCodeToNull;
    }

    /** 判断分类编号是否更新为空值 **/
    public Boolean isCategoryCodeToNull() {
        return categoryCodeToNull;
    }

    /**
     * 设置分类编号是否更新为空值
     *
     * @param categoryCodeToNull 分类编号是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setCategoryCodeToNull(Boolean categoryCodeToNull) {
        this.categoryCodeToNull = categoryCodeToNull;
    }

    /** 判断品牌编号是否更新为空值 **/
    public Boolean isBrandCodeToNull() {
        return brandCodeToNull;
    }

    /** 设置品牌编号是否更新为空值 **/
    public void setBrandCodeToNull(Boolean brandCodeToNull) {
        this.brandCodeToNull = brandCodeToNull;
    }

    /** 判断型号是否更新为空值 **/
    public Boolean isModelNumberToNull() {
        return modelNumberToNull;
    }

    /** 设置型号是否更新为空值 **/
    public void setModelNumberToNull(Boolean modelNumberToNull) {
        this.modelNumberToNull = modelNumberToNull;
    }

    /** 判断商品名称是否更新为空值 **/
    public Boolean isNameToNull() {
        return nameToNull;
    }

    /** 设置商品名称是否更新为空值 **/
    public void setNameToNull(Boolean nameToNull) {
        this.nameToNull = nameToNull;
    }

    /** 判断标题是否更新为空值 **/
    public Boolean isTitleToNull() {
        return titleToNull;
    }

    /** 设置标题是否更新为空值 **/
    public void setTitleToNull(Boolean titleToNull) {
        this.titleToNull = titleToNull;
    }

    /** 判断单位(个/件/台/袋等)是否更新为空值 **/
    public Boolean isUnitToNull() {
        return unitToNull;
    }

    /** 设置单位(个/件/台/袋等)是否更新为空值 **/
    public void setUnitToNull(Boolean unitToNull) {
        this.unitToNull = unitToNull;
    }

    /** 判断图片信息是否更新为空值 **/
    public Boolean isImageInfoToNull() {
        return imageInfoToNull;
    }

    /** 设置图片信息是否更新为空值 **/
    public void setImageInfoToNull(Boolean imageInfoToNull) {
        this.imageInfoToNull = imageInfoToNull;
    }

    /** 判断市场价格描述是否更新为空值 **/
    public Boolean isMarketPriceTextToNull() {
        return marketPriceTextToNull;
    }

    /** 设置市场价格描述是否更新为空值 **/
    public void setMarketPriceTextToNull(Boolean marketPriceTextToNull) {
        this.marketPriceTextToNull = marketPriceTextToNull;
    }

    /** 判断销售价格描述(用于主商品展示的价格描述, 如80~99,199起)是否更新为空值 **/
    public Boolean isSalesPriceTextToNull() {
        return salesPriceTextToNull;
    }

    /** 设置销售价格描述(用于主商品展示的价格描述, 如80~99,199起)是否更新为空值 **/
    public void setSalesPriceTextToNull(Boolean salesPriceTextToNull) {
        this.salesPriceTextToNull = salesPriceTextToNull;
    }

    /** 判断价格值(实际售价)是否更新为空值 **/
    public Boolean isPriceValueToNull() {
        return priceValueToNull;
    }

    /** 设置价格值(实际售价)是否更新为空值 **/
    public void setPriceValueToNull(Boolean priceValueToNull) {
        this.priceValueToNull = priceValueToNull;
    }

    /** 获取价格值(实际售价)的增加值 **/
    public Double getPriceValueAdd() {
        return priceValueAdd;
    }

    /** 设置价格值(实际售价)的增加值 **/
    public void setPriceValueAdd(Double priceValueAdd) {
        this.priceValueAdd = priceValueAdd;
    }

    /** 判断总销量是否更新为空值 **/
    public Boolean isSalesQuantityTotalToNull() {
        return salesQuantityTotalToNull;
    }

    /** 设置总销量是否更新为空值 **/
    public void setSalesQuantityTotalToNull(Boolean salesQuantityTotalToNull) {
        this.salesQuantityTotalToNull = salesQuantityTotalToNull;
    }

    /** 获取总销量的增加值 **/
    public Integer getSalesQuantityTotalAdd() {
        return salesQuantityTotalAdd;
    }

    /** 设置总销量的增加值 **/
    public void setSalesQuantityTotalAdd(Integer salesQuantityTotalAdd) {
        this.salesQuantityTotalAdd = salesQuantityTotalAdd;
    }

    /** 判断库存量是否更新为空值 **/
    public Boolean isStockQuantityToNull() {
        return stockQuantityToNull;
    }

    /** 设置库存量是否更新为空值 **/
    public void setStockQuantityToNull(Boolean stockQuantityToNull) {
        this.stockQuantityToNull = stockQuantityToNull;
    }

    /** 获取库存量的增加值 **/
    public Integer getStockQuantityAdd() {
        return stockQuantityAdd;
    }

    /** 设置库存量的增加值 **/
    public void setStockQuantityAdd(Integer stockQuantityAdd) {
        this.stockQuantityAdd = stockQuantityAdd;
    }

    /** 判断摘要是否更新为空值 **/
    public Boolean isIntroTextToNull() {
        return introTextToNull;
    }

    /** 设置摘要是否更新为空值 **/
    public void setIntroTextToNull(Boolean introTextToNull) {
        this.introTextToNull = introTextToNull;
    }

    /** 判断详细描述(富文本)是否更新为空值 **/
    public Boolean isDescDetailsToNull() {
        return descDetailsToNull;
    }

    /** 设置详细描述(富文本)是否更新为空值 **/
    public void setDescDetailsToNull(Boolean descDetailsToNull) {
        this.descDetailsToNull = descDetailsToNull;
    }

    /** 判断排序号是否更新为空值 **/
    public Boolean isSortIndexToNull() {
        return sortIndexToNull;
    }

    /** 设置排序号是否更新为空值 **/
    public void setSortIndexToNull(Boolean sortIndexToNull) {
        this.sortIndexToNull = sortIndexToNull;
    }

    /** 获取排序号的增加值 **/
    public Long getSortIndexAdd() {
        return sortIndexAdd;
    }

    /** 设置排序号的增加值 **/
    public void setSortIndexAdd(Long sortIndexAdd) {
        this.sortIndexAdd = sortIndexAdd;
    }

    /** 判断规格分类ID是否更新为空值 **/
    public Boolean isSpecificKindIdToNull() {
        return specificKindIdToNull;
    }

    /** 设置规格分类ID是否更新为空值 **/
    public void setSpecificKindIdToNull(Boolean specificKindIdToNull) {
        this.specificKindIdToNull = specificKindIdToNull;
    }

    /** 判断规格数据列表是否更新为空值 **/
    public Boolean isSpecificKindDataToNull() {
        return specificKindDataToNull;
    }

    /** 设置规格数据列表是否更新为空值 **/
    public void setSpecificKindDataToNull(Boolean specificKindDataToNull) {
        this.specificKindDataToNull = specificKindDataToNull;
    }

    /** 判断单品实例列表是否更新为空值 **/
    public Boolean isSkuInstanceDataToNull() {
        return skuInstanceDataToNull;
    }

    /** 设置单品实例列表是否更新为空值 **/
    public void setSkuInstanceDataToNull(Boolean skuInstanceDataToNull) {
        this.skuInstanceDataToNull = skuInstanceDataToNull;
    }

    /** 判断创建人ID是否更新为空值 **/
    public Boolean isCreatorIdToNull() {
        return creatorIdToNull;
    }

    /**
     * 设置创建人ID是否更新为空值
     *
     * @param creatorIdToNull 创建人ID是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setCreatorIdToNull(Boolean creatorIdToNull) {
        this.creatorIdToNull = creatorIdToNull;
    }

    /** 判断发布时间是否更新为空值 **/
    public Boolean isPublishTimeToNull() {
        return publishTimeToNull;
    }

    /** 设置发布时间是否更新为空值 **/
    public void setPublishTimeToNull(Boolean publishTimeToNull) {
        this.publishTimeToNull = publishTimeToNull;
    }

    /** 判断发布时间是否更新为数据库当前时间 **/
    public Boolean isPublishTimeToCurrent() {
        return publishTimeToCurrent;
    }

    /** 设置发布时间是否更新为数据库当前时间 **/
    public void setPublishTimeToCurrent(Boolean publishTimeToCurrent) {
        this.publishTimeToCurrent = publishTimeToCurrent;
    }

    /** 获取发布时间的增加值(单位:秒) **/
    public Long getPublishTimeAdd() {
        return publishTimeAdd;
    }

    /** 设置发布时间的增加值(单位:秒) **/
    public void setPublishTimeAdd(Long publishTimeAdd) {
        this.publishTimeAdd = publishTimeAdd;
    }

    /** 判断过期时间是否更新为空值 **/
    public Boolean isExpireTimeToNull() {
        return expireTimeToNull;
    }

    /** 设置过期时间是否更新为空值 **/
    public void setExpireTimeToNull(Boolean expireTimeToNull) {
        this.expireTimeToNull = expireTimeToNull;
    }

    /** 判断过期时间是否更新为数据库当前时间 **/
    public Boolean isExpireTimeToCurrent() {
        return expireTimeToCurrent;
    }

    /** 设置过期时间是否更新为数据库当前时间 **/
    public void setExpireTimeToCurrent(Boolean expireTimeToCurrent) {
        this.expireTimeToCurrent = expireTimeToCurrent;
    }

    /** 获取过期时间的增加值(单位:秒) **/
    public Long getExpireTimeAdd() {
        return expireTimeAdd;
    }

    /** 设置过期时间的增加值(单位:秒) **/
    public void setExpireTimeAdd(Long expireTimeAdd) {
        this.expireTimeAdd = expireTimeAdd;
    }

    /** 判断选项是否更新为空值 **/
    public Boolean isOptionsToNull() {
        return optionsToNull;
    }

    /** 设置选项是否更新为空值 **/
    public void setOptionsToNull(Boolean optionsToNull) {
        this.optionsToNull = optionsToNull;
    }

    /** 判断提交说明是否更新为空值 **/
    public Boolean isSubmitDescToNull() {
        return submitDescToNull;
    }

    /** 设置提交说明是否更新为空值 **/
    public void setSubmitDescToNull(Boolean submitDescToNull) {
        this.submitDescToNull = submitDescToNull;
    }

    /** 判断编辑序号(每编辑一次递增1)是否更新为空值 **/
    public Boolean isEditIndexToNull() {
        return editIndexToNull;
    }

    /**
     * 设置编辑序号(每编辑一次递增1)是否更新为空值
     *
     * @param editIndexToNull 编辑序号(每编辑一次递增1)是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setEditIndexToNull(Boolean editIndexToNull) {
        this.editIndexToNull = editIndexToNull;
    }

    /** 获取编辑序号(每编辑一次递增1)的增加值 **/
    public Integer getEditIndexAdd() {
        return editIndexAdd;
    }

    /** 设置编辑序号(每编辑一次递增1)的增加值 **/
    public void setEditIndexAdd(Integer editIndexAdd) {
        this.editIndexAdd = editIndexAdd;
    }

    /** 判断商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)是否更新为空值 **/
    public Boolean isGoodsStateToNull() {
        return goodsStateToNull;
    }

    /**
     * 设置商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)是否更新为空值
     *
     * @param goodsStateToNull 商品状态(0.未提交|1.已提交|2.已审核|3.已驳回|4.已上架|5.已下架)是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setGoodsStateToNull(Boolean goodsStateToNull) {
        this.goodsStateToNull = goodsStateToNull;
    }

    /** 判断版本状态(0.正本|1.副本|2.归档)是否更新为空值 **/
    public Boolean isVersionStateToNull() {
        return versionStateToNull;
    }

    /**
     * 设置版本状态(0.正本|1.副本|2.归档)是否更新为空值
     *
     * @param versionStateToNull 版本状态(0.正本|1.副本|2.归档)是否更新为空值
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setVersionStateToNull(Boolean versionStateToNull) {
        this.versionStateToNull = versionStateToNull;
    }

    /** 判断查询关键字是否更新为空值 **/
    public Boolean isQueryKeywordsToNull() {
        return queryKeywordsToNull;
    }

    /** 设置查询关键字是否更新为空值 **/
    public void setQueryKeywordsToNull(Boolean queryKeywordsToNull) {
        this.queryKeywordsToNull = queryKeywordsToNull;
    }

    /** 获取更新操作的条件 **/
    public GoodsVersionWhere getWhere() {
        return where;
    }

    /** 获取更新操作的条件, force=是否强制返回非空条件 **/
    public GoodsVersionWhere getWhere(boolean force) {
        if (where == null && force) {
            where = new GoodsVersionWhere();
        }
        return where;
    }

    /** 设置更新操作的条件 **/
    public void setWhere(GoodsVersionWhere where) {
        this.where = where;
    }
}