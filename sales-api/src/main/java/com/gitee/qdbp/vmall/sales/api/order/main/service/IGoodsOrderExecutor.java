package com.gitee.qdbp.vmall.sales.api.order.main.service;

import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.base.system.model.IAccount;
import com.gitee.qdbp.vmall.sales.api.order.main.model.CarrierBean;
import com.gitee.qdbp.vmall.sales.api.order.main.model.CreateGoodsOrderParams;
import com.gitee.qdbp.vmall.sales.api.order.main.model.OrderReceiverBase;

/**
 * 商品订单业务接口
 *
 * @author zhh
 * @version 170701
 */
public interface IGoodsOrderExecutor {

    /**
     * 创建内部订单流程:<br>
     *
     * <pre>
     * 1.校验
     * -- 查用户
     * -- 查商品
     * -- 查库存(只检查不锁定, 到支付跳转时才锁库存)
     *
     * 2.OrderMainExecutor.create()
     * -- 创建订单主信息
     * -- 创建订单明细信息
     *
     * 返回订单ID
     * </pre>
     *
     * @throws ServiceException
     */
    String create(String userId, CreateGoodsOrderParams params, OrderReceiverBase receiver, IAccount me) throws ServiceException;

    /** 修改订单收货人信息 **/
    void updateReceiver(String orderId, OrderReceiverBase model, IAccount me) throws ServiceException;

    /** 订单发货(发货给快递公司) **/
    void deliverByCarrier(String orderId, CarrierBean carrier, IAccount me) throws ServiceException;

}
