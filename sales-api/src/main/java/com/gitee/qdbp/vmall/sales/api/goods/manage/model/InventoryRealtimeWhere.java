package com.gitee.qdbp.vmall.sales.api.goods.manage.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 实时库存查询类
 *
 * @author zhh
 * @version 180626
 */
public class InventoryRealtimeWhere extends InventoryRealtimeBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 主键列表 **/
    private List<String> ids;

    /** 主键前模匹配条件 **/
    private String idStarts;

    /** 主键后模匹配条件 **/
    private String idEnds;

    /** 主键模糊查询条件 **/
    private String idLike;

    /** 租户编号空值/非空值查询 **/
    private Boolean tenantCodeIsNull;

    /** 租户编号前模匹配条件 **/
    private String tenantCodeStarts;

    /** 租户编号后模匹配条件 **/
    private String tenantCodeEnds;

    /** 租户编号模糊查询条件 **/
    private String tenantCodeLike;

    /** 商品唯一ID空值/非空值查询 **/
    private Boolean goodsUidIsNull;

    /** 商品唯一ID列表 **/
    private List<String> goodsUids;

    /** 商品唯一ID前模匹配条件 **/
    private String goodsUidStarts;

    /** 商品唯一ID后模匹配条件 **/
    private String goodsUidEnds;

    /** 商品唯一ID模糊查询条件 **/
    private String goodsUidLike;

    /** 单品ID空值/非空值查询 **/
    private Boolean skuIdIsNull;

    /** 单品ID列表 **/
    private List<String> skuIds;

    /** 单品ID前模匹配条件 **/
    private String skuIdStarts;

    /** 单品ID后模匹配条件 **/
    private String skuIdEnds;

    /** 单品ID模糊查询条件 **/
    private String skuIdLike;

    /** 可售库存量空值/非空值查询 **/
    private Boolean stockQuantityIsNull;

    /** 最小可售库存量 **/
    private Integer stockQuantityMin;

    /** 最大可售库存量 **/
    private Integer stockQuantityMax;

    /** 已售库存量(待发货)空值/非空值查询 **/
    private Boolean soldQuantityIsNull;

    /** 最小已售库存量(待发货) **/
    private Integer soldQuantityMin;

    /** 最大已售库存量(待发货) **/
    private Integer soldQuantityMax;

    /** 获取主键列表 **/
    public List<String> getIds() {
        return ids;
    }

    /** 设置主键列表 **/
    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    /** 增加主键 **/
    public void addId(String... ids) {
        if (this.ids == null) {
            this.ids = new ArrayList<>();
        }
        this.ids.addAll(Arrays.asList(ids));
    }

    /** 获取主键前模匹配条件 **/
    public String getIdStarts() {
        return idStarts;
    }

    /** 设置主键前模匹配条件 **/
    public void setIdStarts(String idStarts) {
        this.idStarts = idStarts;
    }

    /** 获取主键后模匹配条件 **/
    public String getIdEnds() {
        return idEnds;
    }

    /** 设置主键后模匹配条件 **/
    public void setIdEnds(String idEnds) {
        this.idEnds = idEnds;
    }

    /** 获取主键模糊查询条件 **/
    public String getIdLike() {
        return idLike;
    }

    /** 设置主键模糊查询条件 **/
    public void setIdLike(String idLike) {
        this.idLike = idLike;
    }

    /** 判断租户编号是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getTenantCodeIsNull() {
        return tenantCodeIsNull;
    }

    /**
     * 设置租户编号空值查询(true:空值查询|false:非空值查询)
     *
     * @param tenantCodeIsNull 租户编号空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setTenantCodeIsNull(Boolean tenantCodeIsNull) {
        this.tenantCodeIsNull = tenantCodeIsNull;
    }

    /** 获取租户编号前模匹配条件 **/
    public String getTenantCodeStarts() {
        return tenantCodeStarts;
    }

    /** 设置租户编号前模匹配条件 **/
    public void setTenantCodeStarts(String tenantCodeStarts) {
        this.tenantCodeStarts = tenantCodeStarts;
    }

    /** 获取租户编号后模匹配条件 **/
    public String getTenantCodeEnds() {
        return tenantCodeEnds;
    }

    /** 设置租户编号后模匹配条件 **/
    public void setTenantCodeEnds(String tenantCodeEnds) {
        this.tenantCodeEnds = tenantCodeEnds;
    }

    /** 获取租户编号模糊查询条件 **/
    public String getTenantCodeLike() {
        return tenantCodeLike;
    }

    /** 设置租户编号模糊查询条件 **/
    public void setTenantCodeLike(String tenantCodeLike) {
        this.tenantCodeLike = tenantCodeLike;
    }

    /** 判断商品唯一ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getGoodsUidIsNull() {
        return goodsUidIsNull;
    }

    /**
     * 设置商品唯一ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param goodsUidIsNull 商品唯一ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setGoodsUidIsNull(Boolean goodsUidIsNull) {
        this.goodsUidIsNull = goodsUidIsNull;
    }

    /** 获取商品唯一ID列表 **/
    public List<String> getGoodsUids() {
        return goodsUids;
    }

    /** 设置商品唯一ID列表 **/
    public void setGoodsUids(List<String> goodsUids) {
        this.goodsUids = goodsUids;
    }

    /** 增加商品唯一ID **/
    public void addGoodsUid(String... goodsUids) {
        if (this.goodsUids == null) {
            this.goodsUids = new ArrayList<>();
        }
        this.goodsUids.addAll(Arrays.asList(goodsUids));
    }

    /** 获取商品唯一ID前模匹配条件 **/
    public String getGoodsUidStarts() {
        return goodsUidStarts;
    }

    /** 设置商品唯一ID前模匹配条件 **/
    public void setGoodsUidStarts(String goodsUidStarts) {
        this.goodsUidStarts = goodsUidStarts;
    }

    /** 获取商品唯一ID后模匹配条件 **/
    public String getGoodsUidEnds() {
        return goodsUidEnds;
    }

    /** 设置商品唯一ID后模匹配条件 **/
    public void setGoodsUidEnds(String goodsUidEnds) {
        this.goodsUidEnds = goodsUidEnds;
    }

    /** 获取商品唯一ID模糊查询条件 **/
    public String getGoodsUidLike() {
        return goodsUidLike;
    }

    /** 设置商品唯一ID模糊查询条件 **/
    public void setGoodsUidLike(String goodsUidLike) {
        this.goodsUidLike = goodsUidLike;
    }

    /** 判断单品ID是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSkuIdIsNull() {
        return skuIdIsNull;
    }

    /**
     * 设置单品ID空值查询(true:空值查询|false:非空值查询)
     *
     * @param skuIdIsNull 单品ID空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSkuIdIsNull(Boolean skuIdIsNull) {
        this.skuIdIsNull = skuIdIsNull;
    }

    /** 获取单品ID列表 **/
    public List<String> getSkuIds() {
        return skuIds;
    }

    /** 设置单品ID列表 **/
    public void setSkuIds(List<String> skuIds) {
        this.skuIds = skuIds;
    }

    /** 增加单品ID **/
    public void addSkuId(String... skuIds) {
        if (this.skuIds == null) {
            this.skuIds = new ArrayList<>();
        }
        this.skuIds.addAll(Arrays.asList(skuIds));
    }

    /** 获取单品ID前模匹配条件 **/
    public String getSkuIdStarts() {
        return skuIdStarts;
    }

    /** 设置单品ID前模匹配条件 **/
    public void setSkuIdStarts(String skuIdStarts) {
        this.skuIdStarts = skuIdStarts;
    }

    /** 获取单品ID后模匹配条件 **/
    public String getSkuIdEnds() {
        return skuIdEnds;
    }

    /** 设置单品ID后模匹配条件 **/
    public void setSkuIdEnds(String skuIdEnds) {
        this.skuIdEnds = skuIdEnds;
    }

    /** 获取单品ID模糊查询条件 **/
    public String getSkuIdLike() {
        return skuIdLike;
    }

    /** 设置单品ID模糊查询条件 **/
    public void setSkuIdLike(String skuIdLike) {
        this.skuIdLike = skuIdLike;
    }

    /** 判断可售库存量是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getStockQuantityIsNull() {
        return stockQuantityIsNull;
    }

    /**
     * 设置可售库存量空值查询(true:空值查询|false:非空值查询)
     *
     * @param stockQuantityIsNull 可售库存量空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setStockQuantityIsNull(Boolean stockQuantityIsNull) {
        this.stockQuantityIsNull = stockQuantityIsNull;
    }

    /** 获取最小可售库存量 **/
    public Integer getStockQuantityMin() {
        return stockQuantityMin;
    }

    /** 设置最小可售库存量 **/
    public void setStockQuantityMin(Integer stockQuantityMin) {
        this.stockQuantityMin = stockQuantityMin;
    }

    /** 获取最大可售库存量 **/
    public Integer getStockQuantityMax() {
        return stockQuantityMax;
    }

    /** 设置最大可售库存量 **/
    public void setStockQuantityMax(Integer stockQuantityMax) {
        this.stockQuantityMax = stockQuantityMax;
    }

    /** 判断已售库存量(待发货)是否为空值查询(true:空值查询|false:非空值查询) **/
    public Boolean getSoldQuantityIsNull() {
        return soldQuantityIsNull;
    }

    /**
     * 设置已售库存量(待发货)空值查询(true:空值查询|false:非空值查询)
     *
     * @param soldQuantityIsNull 已售库存量(待发货)空值查询
     * @deprecated 该字段为非空字段
     */
    @Deprecated
    public void setSoldQuantityIsNull(Boolean soldQuantityIsNull) {
        this.soldQuantityIsNull = soldQuantityIsNull;
    }

    /** 获取最小已售库存量(待发货) **/
    public Integer getSoldQuantityMin() {
        return soldQuantityMin;
    }

    /** 设置最小已售库存量(待发货) **/
    public void setSoldQuantityMin(Integer soldQuantityMin) {
        this.soldQuantityMin = soldQuantityMin;
    }

    /** 获取最大已售库存量(待发货) **/
    public Integer getSoldQuantityMax() {
        return soldQuantityMax;
    }

    /** 设置最大已售库存量(待发货) **/
    public void setSoldQuantityMax(Integer soldQuantityMax) {
        this.soldQuantityMax = soldQuantityMax;
    }

}