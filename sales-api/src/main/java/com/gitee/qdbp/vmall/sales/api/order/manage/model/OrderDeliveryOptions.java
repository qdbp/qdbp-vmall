package com.gitee.qdbp.vmall.sales.api.order.manage.model;

import com.gitee.qdbp.able.model.reusable.ExtraData;

/**
 * 选项
 *
 * @author zhh
 * @version 180624
 */
public class OrderDeliveryOptions extends ExtraData {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

}