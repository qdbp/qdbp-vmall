package com.gitee.qdbp.vmall.sales.api.goods.manage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.gitee.qdbp.able.jdbc.paging.PartList;
import com.gitee.qdbp.base.annotation.DataIsolation;
import com.gitee.qdbp.base.annotation.OperateTraces;
import com.gitee.qdbp.base.enums.DataState;

/**
 * 用户购物车实体类
 *
 * @author zhh
 * @version 180703
 */
@OperateTraces(id = "id")
@DataIsolation("tenantCode")
public class ShoppingCartBean implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 表名 **/
    public static final String TABLE = "vml_shopping_cart";

    /** 主键 **/
    private String id;

    /** 商家租户编号 **/
    private String tenantCode;

    /** 消费者用户ID **/
    private String userId;

    /** 商品唯一ID **/
    private String goodsUid;

    /** 商品版本ID **/
    private String goodsVid;

    /** 单品ID **/
    private String skuId;

    /** 购买数量 **/
    private Integer quantity;

    /** 是否选中 **/
    private Boolean checked;

    /** 创建时间 **/
    private Date createTime;

    /** 查询关键字 **/
    private String queryKeywords;

    /** 数据状态:0为正常|其他为删除 **/
    private DataState dataState;

    /** 获取主键 **/
    public String getId() {
        return id;
    }

    /** 设置主键 **/
    public void setId(String id) {
        this.id = id;
    }

    /** 获取商家租户编号 **/
    public String getTenantCode() {
        return tenantCode;
    }

    /** 设置商家租户编号 **/
    public void setTenantCode(String tenantCode) {
        this.tenantCode = tenantCode;
    }

    /** 获取消费者用户ID **/
    public String getUserId() {
        return userId;
    }

    /** 设置消费者用户ID **/
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 获取商品唯一ID **/
    public String getGoodsUid() {
        return goodsUid;
    }

    /** 设置商品唯一ID **/
    public void setGoodsUid(String goodsUid) {
        this.goodsUid = goodsUid;
    }

    /** 获取商品版本ID **/
    public String getGoodsVid() {
        return goodsVid;
    }

    /** 设置商品版本ID **/
    public void setGoodsVid(String goodsVid) {
        this.goodsVid = goodsVid;
    }

    /** 获取单品ID **/
    public String getSkuId() {
        return skuId;
    }

    /** 设置单品ID **/
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    /** 获取购买数量 **/
    public Integer getQuantity() {
        return quantity;
    }

    /** 设置购买数量 **/
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /** 获取是否选中 **/
    public Boolean getChecked() {
        return checked;
    }

    /** 设置是否选中 **/
    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    /** 获取创建时间 **/
    public Date getCreateTime() {
        return createTime;
    }

    /** 设置创建时间 **/
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /** 获取查询关键字 **/
    public String getQueryKeywords() {
        return queryKeywords;
    }

    /** 设置查询关键字 **/
    public void setQueryKeywords(String queryKeywords) {
        this.queryKeywords = queryKeywords;
    }

    /** 获取数据状态:0为正常|其他为删除 **/
    public DataState getDataState() {
        return dataState;
    }

    /** 设置数据状态:0为正常|其他为删除 **/
    public void setDataState(DataState dataState) {
        this.dataState = dataState;
    }

    /**
     * 将当前对象转换为子类对象
     *
     * @param clazz 目标类型
     * @return 目标对象
     */
    public <T extends ShoppingCartBean> T to(Class<T> clazz) {
        T instance;
        try {
            instance = clazz.newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to create " + clazz.getSimpleName() + " instance.", e);
        }

        instance.setId(this.getId()); // 主键
        instance.setTenantCode(this.getTenantCode()); // 商家租户编号
        instance.setUserId(this.getUserId()); // 消费者用户ID
        instance.setGoodsUid(this.getGoodsUid()); // 商品唯一ID
        instance.setGoodsVid(this.getGoodsVid()); // 商品版本ID
        instance.setSkuId(this.getSkuId()); // 单品ID
        instance.setQuantity(this.getQuantity()); // 购买数量
        instance.setChecked(this.getChecked()); // 是否选中
        instance.setCreateTime(this.getCreateTime()); // 创建时间
        instance.setQueryKeywords(this.getQueryKeywords()); // 查询关键字
        instance.setDataState(this.getDataState()); // 数据状态:0为正常|其他为删除
        return instance;
    }

    /**
     * 将ShoppingCartBean转换为子类对象
     *
     * @param beans 待转换的对象列表
     * @param clazz 目标类型
     * @param <T> ShoppingCartBean或子类
     * @param <C> T的子类
     * @return 目标对象列表
     */
    public static <T extends ShoppingCartBean, C extends T> List<C> to(List<T> beans, Class<C> clazz) {
        if (beans == null) {
            return null;
        }
        List<C> list;
        if (beans instanceof PartList) {
            PartList<C> partlist = new PartList<>();
            partlist.setTotal(((PartList<?>) beans).getTotal());
            list = partlist;
        } else {
            list = new ArrayList<>();
        }
        for (ShoppingCartBean bean : beans) {
            list.add(bean.to(clazz));
        }
        return list;
    }

}