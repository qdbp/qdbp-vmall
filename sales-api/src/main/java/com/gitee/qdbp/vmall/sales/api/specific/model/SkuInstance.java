package com.gitee.qdbp.vmall.sales.api.specific.model;

import java.io.Serializable;
import java.util.List;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsImages;

/**
 * 单品实例
 *
 * @author zhaohuihua
 * @version 180625
 */
public class SkuInstance implements Serializable {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 单品ID **/
    private String id;
    /** 单品规格描述: 如颜色:红/尺码:M; 颜色:红/尺码:L; ... **/
    private String specificText;
    /** 单品规格项: 如颜色红/尺码M **/
    private List<SkuItem> items;

    /** 市场价格描述 **/
    private String marketPriceText;
    /** 价格值(实际售价) **/
    private Double priceValue;
    /** 单品图片信息 **/
    private GoodsImages imageInfo;

    /** 单品ID **/
    public String getId() {
        return id;
    }

    /** 单品ID **/
    public void setId(String id) {
        this.id = id;
    }

    /** 单品规格描述: 如颜色:红/尺码:M; 颜色:红/尺码:L; ... **/
    public String getSpecificText() {
        return specificText;
    }

    /** 单品规格描述: 如颜色:红/尺码:M; 颜色:红/尺码:L; ... **/
    public void setSpecificText(String specificText) {
        this.specificText = specificText;
    }

    /** 单品规格项: 如颜色红/尺码M **/
    public List<SkuItem> getItems() {
        return items;
    }

    /** 单品规格项: 如颜色红/尺码M **/
    public void setItems(List<SkuItem> items) {
        this.items = items;
    }

    /** 市场价格描述 **/
    public String getMarketPriceText() {
        return marketPriceText;
    }

    /** 市场价格描述 **/
    public void setMarketPriceText(String marketPriceText) {
        this.marketPriceText = marketPriceText;
    }

    /** 价格值(实际售价) **/
    public Double getPriceValue() {
        return priceValue;
    }

    /** 价格值(实际售价) **/
    public void setPriceValue(Double priceValue) {
        this.priceValue = priceValue;
    }

    /** 单品图片信息 **/
    public GoodsImages getImageInfo() {
        return imageInfo;
    }

    /** 获取图片信息, force=是否强制返回非空对象 **/
    public GoodsImages getImageInfo(boolean force) {
        if (imageInfo == null && force) {
            imageInfo = new GoodsImages();
        }
        return imageInfo;
    }

    /** 单品图片信息 **/
    public void setImageInfo(GoodsImages imageInfo) {
        this.imageInfo = imageInfo;
    }

}
