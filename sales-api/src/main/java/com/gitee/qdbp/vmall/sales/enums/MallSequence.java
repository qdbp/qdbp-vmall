package com.gitee.qdbp.vmall.sales.enums;

public enum MallSequence {
    /** 订单 **/
    ORDER_FORM,
    /** 商品 **/
    GOODS_INFO;
}
