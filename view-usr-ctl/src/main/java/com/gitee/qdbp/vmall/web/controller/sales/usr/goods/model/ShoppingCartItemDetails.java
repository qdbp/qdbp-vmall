package com.gitee.qdbp.vmall.web.controller.sales.usr.goods.model;

import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionBean;
import com.gitee.qdbp.vmall.sales.api.goods.manage.model.ShoppingCartBean;
import com.gitee.qdbp.vmall.sales.api.specific.model.SkuInstance;

/**
 * 商品订单项详情
 *
 * @author zhaohuihua
 * @version 180624
 */
public class ShoppingCartItemDetails extends ShoppingCartBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 可售库存量 **/
    private Integer stockQuantity;
    /** 已售库存量(待发货) **/
    private Integer soldQuantity;
    /** 商品信息(最新版本的商品信息) **/
    private GoodsVersionBean goods;
    /** 单品信息 **/
    private SkuInstance sku;
    /** 价格变动值(正数为涨价,负数为降价; 版本未变更或价格未变动时为0) **/
    private Double priceChange;

    /** 获取可售库存量 **/
    public Integer getStockQuantity() {
        return stockQuantity;
    }

    /** 设置可售库存量 **/
    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    /** 获取已售库存量(待发货) **/
    public Integer getSoldQuantity() {
        return soldQuantity;
    }

    /** 设置已售库存量(待发货) **/
    public void setSoldQuantity(Integer soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    /** 获取商品信息(最新版本的商品信息) **/
    public GoodsVersionBean getGoods() {
        return goods;
    }

    /** 设置商品信息(最新版本的商品信息) **/
    public void setGoods(GoodsVersionBean goods) {
        this.goods = goods;
    }

    /** 获取单品信息(最新版本的单品信息) **/
    public SkuInstance getSku() {
        return sku;
    }

    /** 设置单品信息(最新版本的单品信息) **/
    public void setSku(SkuInstance sku) {
        this.sku = sku;
    }

    /** 获取价格变动值(正数为涨价,负数为降价; 版本未变更或价格未变动时为0) **/
    public Double getPriceChange() {
        return priceChange;
    }

    /** 设置价格变动值(正数为涨价,负数为降价; 版本未变更或价格未变动时为0) **/
    public void setPriceChange(Double priceChange) {
        this.priceChange = priceChange;
    }

}
