package com.gitee.qdbp.vmall.web.controller.sales.usr.home;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.gitee.qdbp.able.beans.DepthMap;
import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.able.jdbc.paging.Paging;
import com.gitee.qdbp.base.annotation.OperateRecord;
import com.gitee.qdbp.general.common.enums.VersionState;
import com.gitee.qdbp.general.system.web.views.SimpleViewsTools;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionBean;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionWhere;
import com.gitee.qdbp.vmall.sales.api.goods.main.service.IGoodsVersionQueryer;
import com.gitee.qdbp.vmall.sales.enums.GoodsState;

@Controller
@RequestMapping()
@OperateRecord(enable = false)
public class HomePageController {

    protected SimpleViewsTools viewTools;
    @Autowired
    protected Properties setting;
    @Autowired
    private IGoodsVersionQueryer goodsVersionQueryer;

    @PostConstruct
    protected void init() {
        viewTools = new SimpleViewsTools(setting);
    }

    /** 首页 **/
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView toDefault() throws ServiceException {
        return toIndex();
    }

    /** 首页 **/
    @RequestMapping(value = "index", method = RequestMethod.GET)
    public ModelAndView toIndex() throws ServiceException {
        Map<String, Object> selling = new HashMap<>();
        { // 查询在售的商品
            OrderPaging paging = OrderPaging.of(new Paging(1, 20));
            GoodsVersionWhere where = new GoodsVersionWhere();
            where.setGoodsState(GoodsState.SELLING);
            where.setVersionState(VersionState.ACTIVATED);
            PageList<GoodsVersionBean> temp = goodsVersionQueryer.list(where, paging);
            selling.put("rows", paging.getRows());
            selling.put("total", temp.getTotal());
            selling.put("list", temp.getList());
        }

        String dt = viewTools.getDeviceType();
        String view = dt + "/site/home/index";
        DepthMap dpm = viewTools.getPageVariables();
        dpm.put("goods.selling", selling);
        return new ModelAndView(view, "pv", dpm.map());
    }

}
