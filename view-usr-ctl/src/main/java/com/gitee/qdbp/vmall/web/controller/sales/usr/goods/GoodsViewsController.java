package com.gitee.qdbp.vmall.web.controller.sales.usr.goods;

import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.annotation.PostConstruct;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.gitee.qdbp.able.beans.DepthMap;
import com.gitee.qdbp.base.annotation.OperateRecord;
import com.gitee.qdbp.general.system.web.views.SimpleViewsTools;
import com.gitee.qdbp.tools.utils.DateTools;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionBean;
import com.gitee.qdbp.vmall.sales.api.goods.main.service.IGoodsVersionQueryer;
import com.gitee.qdbp.vmall.sales.api.specific.model.SkuInstance;

/**
 * 页面跳转控制器
 *
 * @author zhaohuihua
 * @version 180706
 */
@Controller
@RequestMapping()
@OperateRecord(enable = false)
public class GoodsViewsController {

    private SimpleViewsTools viewTools;
    @Autowired
    private Properties setting;
    @Autowired
    private IGoodsVersionQueryer goodsVersionQueryer;

    @PostConstruct
    protected void init() {
        viewTools = new SimpleViewsTools(setting);
    }

    /** 商品详情页 **/
    @RequestMapping(value = "/goods/{uid:\\d+}/", method = RequestMethod.GET)
    public ModelAndView toGoodsDetails(@PathVariable("uid") String uid) {
        SecurityUtils.getSubject().isPermitted("*");

        GoodsVersionBean goods = goodsVersionQueryer.findByUid(uid);
        if (goods == null) {
            // return "404"; // TODO
        }

        String dt = "pc"; // viewTools.getDeviceType();
        String view = dt + "/site/goods/details";
        DepthMap dpm = viewTools.getPageVariables();
        dpm.put("goods", goods);
        dpm.put("now", DateTools.toNormativeString(new Date()));
        return new ModelAndView(view, "pv", dpm.map());
    }

    /** 商品快照页 **/
    @RequestMapping(value = "/goods/sku/{vid}/{skuId}/", method = RequestMethod.GET)
    public ModelAndView toGoodsVidDetails(@PathVariable("vid") String vid, @PathVariable("skuId") String skuId) {
        SecurityUtils.getSubject().isPermitted("*");

        GoodsVersionBean goods = goodsVersionQueryer.findByVid(vid);
        if (goods == null) {
            // return "404"; // TODO
        }

        SkuInstance sku = null;
        List<SkuInstance> skus = goods.getSkuInstanceData();
        for (SkuInstance i : skus) {
            if (i.getId().equals(skuId)) {
                sku = i;
                break;
            }
        }
        if (sku == null) {
            // return "404"; // TODO
        }

        String dt = "pc"; // viewTools.getDeviceType();
        String view = dt + "/site/goods/details";
        DepthMap dpm = viewTools.getPageVariables();
        dpm.put("goods", goods);
        dpm.put("sku", sku);
        dpm.put("snapshot", true);
        dpm.put("now", DateTools.toNormativeString(new Date()));
        return new ModelAndView(view, "pv", dpm.map());
    }

    /** 商品预览页 **/
    @RequestMapping(value = "/goods/preview/{vid}/", method = RequestMethod.GET)
    public ModelAndView toGoodsPerview(@PathVariable("vid") String vid) {
        SecurityUtils.getSubject().isPermitted("*");

        GoodsVersionBean goods = goodsVersionQueryer.findByVid(vid);
        if (goods == null) {
            // return "404"; // TODO
        }

        String dt = "pc"; // viewTools.getDeviceType();
        String view = dt + "/site/goods/details";
        DepthMap dpm = viewTools.getPageVariables();
        dpm.put("goods", goods);
        dpm.put("preview", true);
        dpm.put("now", DateTools.toNormativeString(new Date()));
        return new ModelAndView(view, "pv", dpm.map());
    }


    /** 购物页面 **/
    @RequestMapping(value = "/goods/shopping-{page}/", method = RequestMethod.GET)
    public ModelAndView toShoppingPage(@PathVariable("page") String page) {
        SecurityUtils.getSubject().isPermitted("*");

        String dt = "pc"; // viewTools.getDeviceType();
        String view = dt + "/site/goods/shopping-" + page;
        DepthMap dpm = viewTools.getPageVariables();
        return new ModelAndView(view, "pv", dpm.map());
    }
}
