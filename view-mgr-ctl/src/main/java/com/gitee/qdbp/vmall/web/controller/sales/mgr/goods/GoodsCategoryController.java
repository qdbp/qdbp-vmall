package com.gitee.qdbp.vmall.web.controller.sales.mgr.goods;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.gitee.qdbp.able.exception.ServiceException;
import com.gitee.qdbp.able.jdbc.ordering.OrderPaging;
import com.gitee.qdbp.able.jdbc.paging.PageList;
import com.gitee.qdbp.able.result.ResponseMessage;
import com.gitee.qdbp.base.system.model.IAccount;
import com.gitee.qdbp.base.utils.SessionTools;
import com.gitee.qdbp.general.common.api.reusable.model.SimpleTreeBean;
import com.gitee.qdbp.general.common.api.reusable.model.SimpleTreeUpdate;
import com.gitee.qdbp.general.common.api.reusable.model.SimpleTreeWhere;
import com.gitee.qdbp.general.common.api.reusable.service.ISimpleTreeExecutor;
import com.gitee.qdbp.general.common.api.reusable.service.ISimpleTreeQueryer;
import com.gitee.qdbp.vmall.sales.enums.MallScene;

/**
 * 商品分类控制器
 *
 * @author zhaohuihua
 * @version 170401
 */
@Controller
@RequestMapping("actions/goods/category")
public class GoodsCategoryController {

    private MallScene scene = MallScene.GOODS_CATEGORY;
    @Autowired
    private ISimpleTreeQueryer simpleTreeQueryer;
    @Autowired
    private ISimpleTreeExecutor simpleTreeExecutor;

    /**
     * 查询商品分类列表
     *
     * @param paging 分页信息
     * @param model 查询条件
     * @return 商品分类列表
     */
    @ResponseBody
    @RequestMapping("list")
    @RequiresPermissions("goods:category:query")
    public ResponseMessage list(SimpleTreeWhere model, OrderPaging paging, Boolean extra) {
        try {
            PageList<SimpleTreeBean> list = simpleTreeQueryer.list(scene.key(), model, paging);
            ResponseMessage result = new ResponseMessage(list.toList());
            return result;
        } catch (ServiceException e) {
            return new ResponseMessage(e);
        }
    }

    /** 创建商品分类 **/
    @ResponseBody
    @RequestMapping("create")
    @RequiresPermissions("goods:category:create")
    public ResponseMessage create(SimpleTreeBean model) {

        try {
            IAccount operator = SessionTools.getLoginUser();
            simpleTreeExecutor.create(scene.key(), model, operator);
            return new ResponseMessage(model);
        } catch (ServiceException e) {
            return new ResponseMessage(e);
        }
    }

    /** 修改商品分类 **/
    @ResponseBody
    @RequestMapping("update")
    @RequiresPermissions("goods:category:update")
    public ResponseMessage update(SimpleTreeUpdate model) {

        try {
            IAccount operator = SessionTools.getLoginUser();
            simpleTreeExecutor.update(scene.key(), model, operator);
            return new ResponseMessage(model);
        } catch (ServiceException e) {
            return new ResponseMessage(e);
        }
    }

    /** 删除商品分类 **/
    @ResponseBody
    @RequestMapping("delete")
    @RequiresPermissions("goods:category:delete") // 删除
    public ResponseMessage delete(String code) {

        try {
            IAccount operator = SessionTools.getLoginUser();
            simpleTreeExecutor.deleteByCode(scene.key(), code, operator);
            return new ResponseMessage();
        } catch (ServiceException e) {
            return new ResponseMessage(e);
        }
    }
}
