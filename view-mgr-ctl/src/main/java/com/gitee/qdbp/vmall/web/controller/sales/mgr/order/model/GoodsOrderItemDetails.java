package com.gitee.qdbp.vmall.web.controller.sales.mgr.order.model;

import com.gitee.qdbp.general.trade.api.order.main.model.OrderItemBean;
import com.gitee.qdbp.vmall.sales.api.goods.main.model.GoodsVersionBean;

/**
 * 商品订单项详情
 *
 * @author zhaohuihua
 * @version 180624
 */
public class GoodsOrderItemDetails extends OrderItemBean {

    /** 版本序列号 **/
    private static final long serialVersionUID = 1L;

    /** 可售库存量 **/
    private Integer stockQuantity;

    /** 已售库存量(待发货) **/
    private Integer soldQuantity;

    private GoodsVersionBean goods;

    public GoodsVersionBean getGoods() {
        return goods;
    }

    public void setGoods(GoodsVersionBean goods) {
        this.goods = goods;
    }

    /** 获取可售库存量 **/
    public Integer getStockQuantity() {
        return stockQuantity;
    }

    /** 设置可售库存量 **/
    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    /** 获取已售库存量(待发货) **/
    public Integer getSoldQuantity() {
        return soldQuantity;
    }

    /** 设置已售库存量(待发货) **/
    public void setSoldQuantity(Integer soldQuantity) {
        this.soldQuantity = soldQuantity;
    }
}
